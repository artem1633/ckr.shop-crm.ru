<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/site-custom.css',
        'css/act.css',
        'https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css',
        'libs/intro/css/introjs.css',
    ];
    public $js = [
        'libs/intro/js/intro.js',
        'js/main.js',
        'https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
