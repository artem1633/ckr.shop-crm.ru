<?php

namespace app\controllers;

use app\models\Documents;
use Yii;
use app\models\Creditor;
use app\models\CreditorSearch;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

use app\models\Contract;
use app\models\DocumentsCreditors;
use app\models\DocumentsCreditorsSearch;

use app\services\YandexDisk;


/**
 * CreditorController implements the CRUD actions for Creditor model.
 */
class CreditorController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => \app\filters\BannedFilter::className(),
            ],
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'rules' => [
				   [
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Creditor models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CreditorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Creditor model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Creditor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($client_id)
    {
        $model = new Creditor();
        $contracts = ArrayHelper::map(Contract::find()->all(), 'id', 'number');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'client_id' => $client_id,
                'contracts' => $contracts,
            ]);
        }
    }

    /**
     * Updates an existing Creditor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $contracts = ArrayHelper::map(Contract::find()->all(), 'id', 'number');

        $documentsSearchModel = new DocumentsCreditorsSearch();
        $documentsDataProvider = $documentsSearchModel->searchByCreditorId(Yii::$app->request->queryParams, $id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['client/view', 'id' => $model->client]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'contracts' => $contracts,
                'documentsSearchModel' => $documentsSearchModel,
                'documentsDataProvider' => $documentsDataProvider,
            ]);
        }
    }

    // Start Creditors Documents ---
    public function actionCreatedocument($creditor_id)
    {
        $model = new DocumentsCreditors();
        $contracts = ArrayHelper::map(Contract::find()->all(), 'id', 'number');
//        return dd ($creditor_id);
        $creditor = Creditor::find()->where(['id' => $creditor_id])->one();
        $contract = Contract::find()->where(['id' => $creditor->contract_id])->one();

        $model->date_created = date('d.m.Y');

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $file = UploadedFile::getInstance($model, 'file');
            $model->file = $file;
            if($model->file != null)
            {
                $path = 'img/uploaded/img_'.date('d.m.Y.H.i.s').'.'.$model->file->extension;
                $model->file->saveAs($path);

                $diskPath = "%2FКлиенты%2F".$model->creditor->client->name."%2F".$model->creditor->name."%2Fскан_".$model->name."__".date('d.m.Y.H.i.s').'.'.$model->file->extension;
                YandexDisk::mkdir("%2FКлиенты%2F".$model->creditor->client->name);
                YandexDisk::mkdir("%2FКлиенты%2F".$model->creditor->client->name."%2F".$model->creditor->name);
                YandexDisk::uploadFile($path, $diskPath);
                $model->scan = $diskPath;
                unlink($path);
            }

            $model->save();

            return $this->redirect(['client/view', 'id' => $model->creditor->client->id]);
        } else {
            return $this->renderAjax('createdocument', [
                'model' => $model,/**/
                'contracts' => $contracts,
                'creditor_id' => $creditor_id,
                'contract' => $contract,
            ]);
        }
    }

    public function actionUpdatedocument($id)
    {
        $model = DocumentsCreditors::findOne($id);
        $contracts = ArrayHelper::map(Contract::find()->all(), 'id', 'number');
        $document = DocumentsCreditors::find()->where(['id' => $id])->one();
        $contract = Contract::find()->where(['id' => $document->contract_id])->one();


        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $file = UploadedFile::getInstance($model, 'file');
            $model->file = $file;
            if($model->file != null)
            {
                $path = 'img/uploaded/img_'.date('d.m.Y.H.i.s').'.'.$model->file->extension;
                $model->file->saveAs($path);

                $diskPath = "%2FКлиенты%2F".$model->creditor->client->name."%2F".$model->creditor->name."%2Fскан_".$model->name."__".date('d.m.Y.H.i.s').'.'.$model->file->extension;
                YandexDisk::mkdir("%2FКлиенты%2F".$model->creditor->client->name);
                YandexDisk::mkdir("%2FКлиенты%2F".$model->creditor->client->name."%2F".$model->creditor->name);
                YandexDisk::uploadFile($path, $diskPath);
                $model->scan = $diskPath;
                unlink($path);
            }

            $model->save();

            return $this->redirect(['client/view', 'id' => $model->creditor->client->id]);
        } else {
            return $this->renderAjax('updatedocument', [
                'model' => $model,
                'contracts' => $contracts,
                'contract' => $contract,
            ]);
        }
    }

    public function actionDeletedocument($id)
    {
        $document = DocumentsCreditors::findOne($id);
        $document->delete();

        YandexDisk::rmdir($document->scan);

        return $this->redirect(['client/view', 'id' => $document->creditor->client->id]);
    }

    /**
     * Deletes an existing Creditor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Creditor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Creditor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Creditor::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	 public function print_arr($array) {
        echo '<pre>'.print_r($array, true).'</pre>';
    }
}
