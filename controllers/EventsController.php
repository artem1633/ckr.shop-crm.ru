<?php

namespace app\controllers;

use app\models\Calendar;
use app\models\Notifications;
use app\models\User;
use Faker\Provider\DateTime;
use Yii;
use app\models\Events;
use app\models\EventsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EventsController implements the CRUD actions for Events model.
 */
class EventsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => \app\filters\BannedFilter::className(),
            ],
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'rules' => [
				   [
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Events models.
     * @return mixed
     */
    public function actionIndex($date = null)
    {
        if($date == null)
        {
            $calendar = Calendar::findByCurrentDate();
        } else {
            $calendar = Calendar::find()->where(['date' => $date])->one();
        }

        if($calendar == null)
        {
            $this->updateCalendar(date('Y', strtotime($date)));
            $calendar = Calendar::findByCurrentDate();
        }

        $searchModel = new EventsSearch();
        $dataProvider = $searchModel->searchByCalendar(Yii::$app->request->queryParams, $calendar->id);


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'calendarId' => $calendar->id,
        ]);
    }

    /**
     * Displays a single Events model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Events model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($calendarId, $calendarDate = null)
    {
        $model = new Events();
        $model->calendar_id = $calendarId;

        if($calendarDate == null)
            $calendarDate = date('Y-m-d');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'date' => $calendarDate]);
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Events model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $calendarDate = null)
    {
        $model = $this->findModel($id);

        if($calendarDate == null)
            $calendarDate = date('Y-m-d');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'date' => $calendarDate]);
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Events model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();
        $calendar = Calendar::find()->where(['id' => $model->calendar_id])->one();

        return $this->redirect(['index', 'date' => $calendar->date]);
    }

    /**
     * Оповещает всех пользователей о событии
     * @param $id
     * @return mixed
     */
    public function actionNotify($id)
    {
        $event = $this->findModel($id);
        $calendar = $event->calendar;
        $userId = Yii::$app->user->identity->id;
        $result = Notifications::globalNotify($userId, 'Оповещение', "
            $event->content<br><i>Дата: $calendar->date $event->time</i>
        ");

        return $this->renderAjax('_sender-message', [
            'result' => $result,
        ]);
    }

    /**
     * Finds the Events model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Events the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Events::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	 public function print_arr($array) {
        echo '<pre>'.print_r($array, true).'</pre>';
    }

    /**
     * Записывает в календарь все даты на год
     * @param $year
     * @return mixed
     */
    public function updateCalendar($year = null)
    {
        if($year == null)
        {
            $year = date('Y');
        }

        $fromDate = new \DateTime(date('Y-m-d', strtotime(date($year.'-01-01'))));
        $toDate = new \DateTime(date('Y-m-t' , strtotime(date($year.'-12-t'))));

        $period = new \DatePeriod($fromDate, new \DateInterval('P1D'), $toDate);

        $arrayOfDates = array_map(
            function($item){return $item->format('Y-m-d');},
            iterator_to_array($period)
        );

        foreach ($arrayOfDates as $date)
        {
            $calendar = new Calendar($date);
            $calendar->save();
        }
    }
}
