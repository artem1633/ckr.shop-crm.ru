<?php

namespace app\controllers;

use Yii;
use app\models\ReportMoneyOperations;
use app\models\ReportMoneyOperationsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;

/**
 * ReportMoneyOperationsController implements the CRUD actions for ReportMoneyOperations model.
 */
class ReportMoneyOperationsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => \app\filters\BannedFilter::className(),
            ],
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'rules' => [
				   [
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ReportMoneyOperations models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->user->identity->category_id != 1)
            throw new ForbiddenHttpException('У вас недостаточно прав');

        $searchModel = new ReportMoneyOperationsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all ReportMoneyOperations models if it is debit operation.
     * @return mixed
     */
    public function actionDebits()
    {
        if(Yii::$app->user->identity->category_id != 1)
            throw new ForbiddenHttpException('У вас недостаточно прав');

        $searchModel = new ReportMoneyOperationsSearch();
        $dataProvider = $searchModel->searchDebits(Yii::$app->request->queryParams);

        return $this->render('index-debits', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all ReportMoneyOperations models if it is credit operation.
     * @return mixed
     */
    public function actionCredits()
    {
        if(Yii::$app->user->identity->category_id != 1)
            throw new ForbiddenHttpException('У вас недостаточно прав');

        $searchModel = new ReportMoneyOperationsSearch();
        $dataProvider = $searchModel->searchCredits(Yii::$app->request->queryParams);

        return $this->render('index-credits', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ReportMoneyOperations model.
     * @param integer $id
     * @return mixed
     */
//    public function actionView($id)
//    {
//        return $this->render('view', [
//            'model' => $this->findModel($id),
//        ]);
//    }

    /**
     * Creates a new ReportMoneyOperations model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
//    public function actionCreate()
//    {
//        $model = new ReportMoneyOperations();
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['index']);
//        } else {
//            return $this->render('create', [
//                'model' => $model,
//            ]);
//        }
//    }

    public function actionCreatecredit()
    {
        if(Yii::$app->user->identity->category_id != 1)
            throw new ForbiddenHttpException('У вас недостаточно прав');

        $model = new ReportMoneyOperations();
        $model->type = 0; // расход

        $model->date = date('d.m.Y');

        $dataLoaded = $model->load(Yii::$app->request->post());

        if ($dataLoaded) {
            $model->date = date('Y-m-d', strtotime($model->date));
//            return dd(date('Y-m-d', strtotime($model->date_confirmed)));
        }

        if ($dataLoaded && $model->save()) {
            return $this->redirect(['credits']);
        } else {
            return $this->render('create-credit', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ReportMoneyOperations model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
//    public function actionUpdate($id)
//    {
//        $model = $this->findModel($id);
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['index']);
//        } else {
//            return $this->render('update', [
//                'model' => $model,
//            ]);
//        }
//    }

    public function actionUpdatecredit($id)
    {
        if(Yii::$app->user->identity->category_id != 1)
            throw new ForbiddenHttpException('У вас недостаточно прав');

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['credits']);
        } else {
            return $this->render('update-credit', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ReportMoneyOperations model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if(Yii::$app->user->identity->category_id != 1)
            throw new ForbiddenHttpException('У вас недостаточно прав');

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ReportMoneyOperations model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ReportMoneyOperations the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ReportMoneyOperations::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	 public function print_arr($array) {
        echo '<pre>'.print_r($array, true).'</pre>';
    }
}
