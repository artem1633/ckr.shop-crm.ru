<?php

namespace app\filters;

use Yii;
use yii\base\ActionFilter;
use yii\web\ForbiddenHttpException;

class BannedFilter extends ActionFilter
{

    public function beforeAction($action)
    {
        $user = Yii::$app->user->identity;

        if($user != null)
        {
            if($user->isBanned() == 1)
            {
                throw new ForbiddenHttpException('Ваш аккаунт заблокирован');
            }
        }

        return parent::beforeAction($action);
    }

//    public function afterAction($action, $result)
//    {
//
//    }


}


?>