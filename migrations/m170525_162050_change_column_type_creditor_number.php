<?php

use yii\db\Migration;

class m170525_162050_change_column_type_creditor_number extends Migration
{
    public function up(){
        $this->alterColumn('creditor', 'number', 'text');//timestamp new_data_type
    }

    public function down() {
        $this->alterColumn('creditor','number', 'INT' );//int is old_data_type
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
