<?php

use yii\db\Migration;

class m170525_170256_alter_column_date_type extends Migration
{
    public function up(){
        $this->alterColumn('creditor', 'date_confirmed', 'date');//timestamp new_data_type
    }

    public function down() {
        $this->alterColumn('creditor','date_confirmed', 'text' );//int is old_data_type
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
