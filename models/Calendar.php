<?php

namespace app\models;

use Faker\Provider\DateTime;
use Yii;

/**
 * This is the model class for table "calendar".
 *
 * @property integer $id
 * @property string $date
 * @property integer $day
 * @property integer $month
 * @property integer $year
 */
class Calendar extends \yii\db\ActiveRecord
{

    public function __construct($date = null, array $config = [])
    {
        $this->date = $date;
        $dateTime = new \DateTime($date);
        $this->day = $dateTime->format('d');
        $this->month = $dateTime->format('m');
        $this->year = $dateTime->format('Y');

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'calendar';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'day', 'month', 'year'], 'required'],
            [['date'], 'safe'],
            [['day', 'month', 'year'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'day' => 'Day',
            'month' => 'Month',
            'year' => 'Year',
        ];
    }

    /**
     * Возвращает запись с сегодняшней датой
     * @return mixed
     */
    public static function findByCurrentDate()
    {
        $calendar = static::find();
        /** @var \app\models\Calendar $calendar */
        return $calendar->where(['date' => date('Y-m-d')])->one();
    }
}
