<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "category_rule".
 *
 * @property integer $id
 * @property string $rule
 */
class CategoryRule extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category_rule';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rule'], 'required'],
            [['rule'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'rule' => 'Rule',
        ];
    }
}
