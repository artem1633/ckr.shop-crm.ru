<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Client;

/**
 * ClientSearch represents the model behind the search form about `app\models\Client`.
 */
class ClientSearch extends Client
{

    // public $contract_number;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'date_birth', 'place_birth', 'passport', 'registration_address', 'current_address', 'phone', 'alt_phone', 'WorkPlace'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Client::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);


        // $query->joinWith(['contract'])->andFilterWhere(['like', Contract::tableName().'.number', $this->contract_number]);


        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }



        $query->andFilterWhere([
            'id' => $this->id,
            'date_birth' => $this->date_birth,
        ]);

        $query->andFilterWhere(['like', 'place_birth', $this->place_birth])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'passport', $this->passport])
            ->andFilterWhere(['like', 'registration_address', $this->registration_address])
            ->andFilterWhere(['like', 'current_address', $this->current_address])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'alt_phone', $this->alt_phone])
            // ->andFilterWhere(['like', 'contract.number', $this->contract])
            // ->andFilterWhere(['like', 'contract.date_created', $this->contract])
            ->andFilterWhere(['like', 'office.name', $this->office])
            ->andFilterWhere(['like', 'WorkPlace', $this->WorkPlace]);



        return $dataProvider;
    }
}
