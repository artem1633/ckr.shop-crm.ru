<?php

namespace app\models;

use Yii;
use Carbon\Carbon;

/**
 * This is the model class for table "contract".
 *
 * @property integer $id
 * @property integer $client_id
 * @property string $conctract_n
 * @property string $date_created
 * @property double $price
 * @property double $rate
 *
 * @property Client $client
 * @property Creditor[] $creditors
 * @property Documents[] $documents
 */
class Contract extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contract';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // [['price', 'rate'], 'required'],
            [['client_id'], 'integer'],
            [['date_created', 'number'], 'safe'],
            [['status', 'rate'], 'string'],
            [['price'], 'number'],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::className(), 'targetAttribute' => ['client_id' => 'id']],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => 'Клиент',
            'number' => 'Номер договора',
            'date_created' => 'Дата создания',
            'price' => 'Сумма по договору',
            'rate' => 'Тариф',
            'status' => 'Статус',
        ];
    }

    public function afterFind()
    {
        $this->date_created = date('Y-m-d', strtotime($this->date_created));
    }

    public function beforeSave($insert)
    {
        if(parent::beforeSave($insert))
        {
//            if($this->isNewRecord && $this->number == null)
//            {
//                $this->number = $this->generateNumber();
//            }

            $currentDate = Carbon::now();
            $this->date_created = $currentDate->toDateString(); //date('d/m/Y',);//$date;//->format('Y-m-d');//format('d-m-Y');

            return true;
        } else {
            return false;
        }
    }

    public function getDateCreated ( ){


            return DateTime::createFromFormat('Y-m-d H:i:s', $this->date_created)->format('d-m-Y');

    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreditors()
    {
        return $this->hasMany(Creditor::className(), ['contract_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocuments()
    {
        return $this->hasMany(Documents::className(), ['contract_id' => 'id']);
    }

    /**
     * Генерирует номер договора
     * @return int $number
     */
    public function generateNumber()
    {
        $number = "ЦКР-";
        $monthId = self::find()->where(['like', 'date_created', date('Y')])->count()+1;// порнядковый номер

        $zeroCount = 4-strlen($monthId); // Кол-во нулей в номере
        for ($i=0; $i < $zeroCount; $i++) { 
            $number .= '0';
        }

        $number .= $monthId."/".date('y');

        return $number;
    }
}
