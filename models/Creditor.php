<?php

namespace app\models;

use Yii;
use app\services\YandexDisk;

/**
 * This is the model class for table "creditor".
 *
 * @property integer $id
 * @property integer $contract_id
 * @property integer $number
 * @property string $name
 * @property string $requisites
 * @property string $comment
 *
 * @property Contract $contract
 */
class Creditor extends \yii\db\ActiveRecord
{

    public $file;
    private $_old_name;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'creditor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // [['number', 'name', 'requisites'], 'required'],
            [['contract_id'], 'integer'],
            ['number', 'string'],
            [['file'], 'file'],
            [['name', 'requisites', 'comment', 'date_confirmed'], 'string'],
            [['contract_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contract::className(), 'targetAttribute' => ['contract_id' => 'id']],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::className(), 'targetAttribute' => ['client_id' => 'id']],
//            [['creditor_id'], 'exist', 'skipOnError' => true, 'targetClass' => DocumentsCreditors::className(), 'targetAttribute' => ['creditor_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'contract_id' => 'Контракт',
            'client_id' => 'Client ID',
            'number' => 'Номер договора',
            'name' => 'Наименование кредитора',
            'requisites' => 'Реквизиты',
            'date_confirmed' => 'от',
            'comment' => 'Комментарии',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContract()
    {
        return $this->hasOne(Contract::className(), ['id' => 'contract_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocuments()
    {
        return $this->hasMany(DocumentsCreditors::className(), ['creditor_id' => 'id']);
    }
    public  function afterFind() // при чтении из базы
    {
        $this->_old_name = $this->name;
        parent::afterFind();
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if($insert)
        {
            // новые записи
            YandexDisk::mkdir("%2FКлиенты%2F". $this->client->name . "%2F" . $this->name);
        }
        else
        {
            // редактирование записи
            // имя поменялось
            if ($this->_old_name != $this.name) {
                // создаем новую директорию
                YandexDisk::mkdir("%2FКлиенты%2F".$this->client->name."%2F" . $this->name);
                // переносим туда документы из старой директории
                YandexDisk::copy("%2FКлиенты%2F".$this->client->name."%2F" . $this->_old_name, "%2FКлиенты%2F".$this->client->name."%2F" . $this->name);
                // удаляем старую директорию
                YandexDisk::rmdir("%2FКлиенты%2F".$this->client->name."%2F" . $this->_old_name);
            }
        }
    }
}
