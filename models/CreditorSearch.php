<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Creditor;
use Carbon\Carbon;

/**
 * CreditorSearch represents the model behind the search form about `app\models\Creditor`.
 */
class CreditorSearch extends Creditor
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'contract_id', 'client_id'], 'integer'],
            [['number'], 'string'],
            [['name', 'requisites', 'comment', 'date_confirmed'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Creditor::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'contract_id' => $this->contract_id,
            'number' => $this->number,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'requisites', $this->requisites])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchByClientId($params, $id)
    {
        $query = Creditor::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'contract_id' => $this->contract_id,
            'client_id' => $id, // Ищем по id клиента
            'number' => $this->number,
        ]);

        $formattedDate = null;

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'requisites', $this->requisites])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        if ($this->date_confirmed!=null) {
            $query->andFilterWhere(['like', 'date_confirmed', date('Y-m-d', strtotime($this->date_confirmed))]);
        }

        return $dataProvider;
    }
}
