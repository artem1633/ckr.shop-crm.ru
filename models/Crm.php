<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "client".
 *
 * @property integer $id
 * @property integer $name
 * @property string $date_birth
 * @property string $place_birth
 * @property string $passport
 * @property string $registration_address
 * @property string $current_address
 * @property string $phone
 * @property string $alt_phone
 * @property string $WorkPlace
 *
 * @property Contract[] $contracts
 * @property Office $office
 */
class Crm extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'crm';
    }

    public function beforeSave($insert)
    {

        return true;

    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'description', 'title'], 'required'],
            [['id'], 'integer'],
            [['description', 'title'], 'string'],
//            [['date'], 'date', 'format' => 'php:d.m.Y'],
            [['date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'description' => 'Описание',
            'title' => 'Заголовок',
            'created_at' => 'Дата',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }


}
