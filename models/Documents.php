<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "documents".
 *
 * @property integer $id
 * @property integer $contract_id
 * @property string $name
 * @property string $scan
 * @property string $date_created
 * @property string $comment
 *
 * @property Contract $contract
 */
class Documents extends \yii\db\ActiveRecord
{

    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'documents';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // [['contract_id', 'name'], 'required'],
            [['contract_id'], 'integer'],
            [['name', 'scan', 'comment'], 'string'],
            [['file'], 'file'],
            [['date_created'], 'safe'],
            [['contract_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contract::className(), 'targetAttribute' => ['contract_id' => 'id']],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::className(), 'targetAttribute' => ['client_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'contract_id' => 'Контракт',
            'client_id' => 'Клиент',
            'file' => 'Скан копия',
            'name' => 'Название',
            'scan' => 'Копия',
            'date_created' => 'Дата загрузки',
            'comment' => 'Комментарии',
        ];
    }

//    public function beforeSave($insert)
//    {
//        if(parent::beforeSave($insert))
//        {
//            if($this->isNewRecord)
//            {
//                $this->date_created = date('d.m.Y');
//            }
//
//            return true;
//        } else {
//            return false;
//        }
//    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContract()
    {
        return $this->hasOne(Contract::className(), ['id' => 'contract_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['id' => 'client_id']);
    }
}
