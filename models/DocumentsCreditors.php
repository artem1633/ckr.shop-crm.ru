<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "documents_creditors".
 *
 * @property integer $id
 * @property integer $contract_id
 * @property integer $creditor_id
 * @property string $name
 * @property string $scan
 * @property string $date_created
 * @property string $comment
 *
 * @property Contract $contract
 * @property Creditor $creditor
 */
class DocumentsCreditors extends \yii\db\ActiveRecord
{
    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'documents_creditors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // [['contract_id', 'creditor_id', 'name'], 'required'],
            [['contract_id', 'creditor_id'], 'integer'],
            [['date_created'], 'safe'],
            [['file'], 'file'],
            [['comment'], 'string'],
            [['name', 'scan'], 'string', 'max' => 255],
            [['contract_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contract::className(), 'targetAttribute' => ['contract_id' => 'id']],
            [['creditor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Creditor::className(), 'targetAttribute' => ['creditor_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'contract_id' => 'Контракт',
            'creditor_id' => 'Кредитор',
            'file' => 'Скан копия',
            'name' => 'Наименование документа',
            'scan' => 'Копия',
            'date_created' => 'Дата загрузки',
            'comment' => 'Комментарии',
        ];
    }

    public function beforeSave($insert)
    {
        if(parent::beforeSave($insert))
        {
            if($this->isNewRecord)
            {
                $this->date_created = date('d.m.Y');
            }

            return true;
        } else {
            return false;
        }
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContract()
    {
        return $this->hasOne(Contract::className(), ['id' => 'contract_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreditor()
    {
        return $this->hasOne(Creditor::className(), ['id' => 'creditor_id']);
    }
}
