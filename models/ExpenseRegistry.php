<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "expense_registry".
 *
 * @property integer $id
 * @property string $date
 * @property double $price
 * @property string $comment
 * @property integer $office_id
 */
class ExpenseRegistry extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'expense_registry';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'price', 'comment', 'office_id'], 'required'],
            [['id', 'office_id'], 'integer'],
            [['date'], 'safe'],
            [['price'], 'number'],
            [['comment'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'price' => 'Price',
            'comment' => 'Comment',
            'office_id' => 'Office ID',
        ];
    }
}
