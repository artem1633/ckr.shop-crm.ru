<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "html_documents".
 *
 * @property integer $id
 * @property string $name
 * @property string $name_ru
 * @property string $content
 */
class HtmlDocuments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'html_documents';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['name_ru'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'name_ru' => 'Наименование',
            'content' => 'Содержимое',
        ];
    }
}
