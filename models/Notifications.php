<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "notifications".
 *
 * @property integer $id
 * @property integer $recipient_id
 * @property integer $sender_id
 * @property string $title
 * @property string $content
 * @property string $datetime
 * @property integer $is_readable
 *
 * @property User $recipient
 */
class Notifications extends \yii\db\ActiveRecord
{
    /**
     * Notifications constructor.
     * @param int $sender_id
     * @param int $recipient_id
     * @param String $title
     * @param String $content
     * @param String $class
     * @param array $config
     */
    public function __construct($sender_id = null, $recipient_id = null, $title = null, $content = null, $class = null, array $config = [])
    {
        $this->sender_id = $sender_id;
        $this->recipient_id = $recipient_id;
        $this->title = $title;
        $this->content = $content;
        if($class == null)
        {
            $class = 'info';
        }
        $this->class = $class;
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notifications';
    }

    /**
     * Создает уведомления для всех пользователей
     * @param int $sender_id
     * @param String $title
     * @param String $content
     * @return bool
     */
    public static function globalNotify($sender_id, $title, $content)
    {
        $users = User::find()->where(['!=', 'id', $sender_id])->all();
        $result = true;

        foreach ($users as $user)
        {
            $notification = new self($sender_id, $user->id, $title, $content);
            if($notification->save() == false)
                $result = false;
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['recipient_id', 'title', 'content'], 'required'],
            [['recipient_id', 'is_readable'], 'integer'],
            [['datetime'], 'safe'],
            [['title', 'content'], 'string', 'max' => 255],
            [['recipient_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['recipient_id' => 'id']],
            [['sender_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['sender_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'recipient_id' => 'Recipient ID',
            'title' => 'Title',
            'content' => 'Content',
            'datetime' => 'Datetime',
            'is_readable' => 'Is Readable',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecipient()
    {
        return $this->hasOne(User::className(), ['id' => 'recipient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSender()
    {
        return $this->hasOne(User::className(), ['id' => 'sender_id']);
    }

    /**
     * Помечает уведомление как прочитанное
     * @return bool
     */
    public function read()
    {
        $this->is_readable = 1;
        return $this->save();
    }
}
