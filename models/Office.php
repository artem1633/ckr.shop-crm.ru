<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "office".
 *
 * @property integer $id
 * @property integer $client_id
 * @property string $name
 *
 * @property ExpenseRegistry[] $expenseRegistries
 * @property Client $client
 * @property User[] $users
 * @property mixed clients
 */
class Office extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'office';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id'], 'integer'],
            [['name'], 'required'],
            [['name'], 'string'],
            [['client_id'], 'unique'],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::className(), 'targetAttribute' => ['client_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => 'Client ID',
            'name' => 'Наименование',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpenseRegistries()
    {
        return $this->hasMany(ExpenseRegistry::className(), ['office_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['office_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClients()
    {
        return $this->hasMany(Client::className(), ['office_id' => 'id']);
    }

    public function getReports()
    {
        return $this->hasMany(ReportMoneyOperations::className(), ['office_id' => 'id']);
    }

    public function getReportscredit()
    {
        return $this->hasMany(ReportMoneyOperations::className(), ['office_id' => 'id', 'type' => 0]); // type 0 - расход
    }

    /**
     * Возвращает доход офиса за период времени
     * @param $timestamp
     * @return int $debit
     */
    public function getDebitByTime($timestamp)
    {
        $debit = 0;
        $clients = $this->clients;

        foreach($clients as $client)
        {
            /** @var \app\models\Payment $payments */
            $payments = Payment::find()->where(['client_id' => $client->id])->andFilterWhere(['between', 'date', $timestamp, date('Y-m-d')])->all();

            foreach ($payments as $payment)
            {

                $debit = $debit + $payment->price;
            }
        }
        return $debit;
    }

    /**
     * Возвращает расход офиса за период времени
     * @param $timestamp
     * @return int
     */
    public function getCreditByTime($timestamp)
    {
        $credit = 0;
        /** @var \app\models\ReportMoneyOperations $reports */
        $reports = ReportMoneyOperations::find()
            ->where(['office_id' => $this->id, 'type' => 0]) // type 0 - расход
            ->andFilterWhere(['between', 'date', $timestamp, date('Y-m-d')])->all();

        foreach($reports as $report)
        {
            $credit = $credit + $report->summ;
        }
        return $credit;
    }

    /**
     * Возвращает доход офиса
     * @param $timestamp
     * @return int $debit
     */
    public function getDebit()
    {
        $debit = 0;
        $clients = $this->clients;

        foreach($clients as $client)
        {
            /** @var \app\models\Payment $payments */
            $payments = Payment::find()->where(['client_id' => $client->id])->all();

            foreach ($payments as $payment)
            {

                $debit = $debit + $payment->price;
            }
        }
        return $debit;
    }

    /**
     * Возвращает расход офиса
     * @param $timestamp
     * @return int
     */
    public function getCredit()
    {
        $credit = 0;
        /** @var \app\models\ReportMoneyOperations $reports */
        $reports = ReportMoneyOperations::find()
            ->where(['office_id' => $this->id, 'type' => 0]) // type 0 - расход
            ->all();

        foreach($reports as $report)
        {
            $credit = $credit + $report->summ;
        }
        return $credit;
    }

    public function getBalance()
    {
        return $this->getDebit()-$this->getCredit();
    }

    public function getBalanceByTime($timestamp)
    {
        return $this->getDebitByTime($timestamp)-$this->getCreditByTime($timestamp);
    }
}
