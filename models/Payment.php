<?php

namespace app\models;

use Carbon\Carbon;
use function GuzzleHttp\Psr7\str;
use Yii;

/**
 * This is the model class for table "payment".
 *
 * @property integer $id
 * @property integer $payment_type
 * @property integer $payment_type_id
 * @property string $date
 * @property double $price
 * @property integer $receipt_number
 * @property string $receipt
 *
 * @property PaymentType $paymentType
 * @property \app\models\Client $client
 */
class Payment extends \yii\db\ActiveRecord
{

	public $nameList = [
	    'Банкротство' => 'Банкротство',
		'Возражение на суд приказ' => 'Возражение на суд приказ',
		'Оплата по договору' => 'Оплата по договору',
		'Юридическая консультация' => 'Письменный комментарий документов',
		'Отзыв согласия на обработку персональных данных' => 'Отзыв согласия на обработку персональных данных',
		'Заявление (досудебное)' => 'Заявление (досудебное)',
		'Жалоба (досудебная)' => 'Жалоба (досудебная)',
		'Претензия (досудебная)' => 'Претензия (досудебная)',
		'Исковое заявление в суд' => 'Исковое заявление в суд',
		'Отзыв на исковое заявление' => 'Отзыв на исковое заявление',
		'Возражение на исковые требования' => 'Возражение на исковые требования',
		'Заявление на отмену заочного решения суда' => 'Заявление на отмену заочного решения суда',
		'Жалоба на определение суда' => 'Жалоба на определение суда',
		'Ходатайство об истребовании документов' => 'Ходатайство об истребовании документов',
		'Представительство в суде (за 1 день судебного заседания)' => 'Представительство в суде (за 1 день судебного заседания)',
		'Заявление на снижение суммы взыскания' => 'Заявление на снижение суммы взыскания',

	];

	const TAGS_TO_ATTRIBUTES = [
	    '%payment.number%' => 'paymentNumber',
	    '%name%' => 'clientName',
	    '%payment.name%' => 'name',
	    '%payment.price%' => 'price',
    ];

    const TAGS_TO_FUNCS = [
        '*date*' => 'dateFormated',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // [['price', 'receipt'], 'required'],
            [['payment_type_id', 'receipt_number', 'client_id'], 'integer'],
            [['date'], 'safe'],
            [['date'], 'string', 'min' => '10'],
            [['price'], 'number'],
            [['name', 'receipt'], 'string'],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::className(), 'targetAttribute' => ['client_id' => 'id']],
            // [['payment_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentType::className(), 'targetAttribute' => ['payment_type_id' => 'id']],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

                $currentDate = Carbon::now();
                $this->date = $currentDate->toDateString(); //date('d/m/Y',);//$date;//->format('Y-m-d');

            return true;
        }
        return false;
    }

    public function afterFind()
    {
        $this->date = Carbon::createFromFormat('Y-m-d H:i:s', $this->date)->format('d.m.Y');
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Номер',
            'name' => 'Наименование платежа',
            'payment_type_id' => 'Способ платежа',
            'date' => 'Дата платежа',
            'price' => 'Сумма платежа',
            'receipt_number' => 'Номер квитанции',
            'receipt' => 'Квитанция',
        ];
    }



    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentType()
    {
        return $this->hasOne(PaymentType::className(), ['id' => 'payment_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['id' => 'client_id']);
    }

    public function getPaymentNumber()
    {
        $zeroNumberCount = 5 - strlen($this->id);

        $number = '';

        for ($i = 0; $i < $zeroNumberCount; $i++)
        {
            $number = $number . '0';
        }

        $number = $number . $this->id;

        return $number;
    }

    public function getClientName()
    {
        return $this->client->name;
    }

    public function getDateFormated()
    {
        return date('d.m.Y', strtotime($this->date));
    }

    public function applyTags($htmlDocument)
    {
        foreach (self::TAGS_TO_ATTRIBUTES as $tag => $attribute) {
            $htmlDocument = str_replace($tag, $this->$attribute, $htmlDocument);
        }

        foreach (self::TAGS_TO_FUNCS as $tag => $attribute){
            $htmlDocument = str_replace($tag, $this->$attribute, $htmlDocument);
        }

        return $htmlDocument;
    }
}
