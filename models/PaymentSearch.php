<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Payment;

/**
 * PaymentSearch represents the model behind the search form about `app\models\Payment`.
 */
class PaymentSearch extends Payment
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'payment_type_id', 'receipt_number', 'client_id'], 'integer'],
            [['name', 'date', 'receipt'], 'safe'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Payment::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'client_id' => $this->client_id,
            'payment_type_id' => $this->payment_type_id,
            'date' => $this->date,
            'price' => $this->price,
            'receipt_number' => $this->receipt_number,
        ]);

        $query->andFilterWhere(['like', 'receipt', $this->receipt]);
        $query->andFilterWhere(['like', 'client.name', $this->client]);
        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchByClientId($params, $id)
    {
        $query = Payment::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'client_id' => $id,
            'payment_type_id' => $this->payment_type_id,
            'date' => $this->date,
            'price' => $this->price,
            'receipt_number' => $this->receipt_number,
        ]);

        $query->andFilterWhere(['like', 'receipt', $this->receipt]);
        $query->andFilterWhere(['like', 'client.name', $this->client]);
        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
