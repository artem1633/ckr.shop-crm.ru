<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "report_money_operations".
 *
 * @property integer $id
 * @property string $date
 * @property string $event
 * @property integer $summ
 * @property integer $person_id
 * @property string $person_snp
 * @property string $comment
 * @property integer $office_id
 *
 * @property Client $person
 * @property Office $office
 */
class ReportMoneyOperations extends \yii\db\ActiveRecord
{
    public $creditArticles = [
        'рееcтр WL' => 'рееcтр WL',
        'задаток WL' => 'задаток WL',
        'аренда' => 'аренда',
        'зарплата' => 'зарплата',
        'налог' => 'налог',
        'прочий расход' => 'прочий расход',
        'канцтовары' => 'канцтовары',
        'почтовые расходы' => 'почтовые расходы',
        'оргтехника' => 'оргтехника',
        'реклама' => 'реклама',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report_money_operations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'event', 'summ'], 'required'],
            [['date'], 'safe'],
            [['summ', 'person_id', 'office_id', 'type'], 'integer'], // type 0 - расход, 1 - доход
            [['comment'], 'string'],
            [['event', 'person_snp'], 'string', 'max' => 255],
            [['person_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::className(), 'targetAttribute' => ['person_id' => 'id']],
            [['office_id'], 'exist', 'skipOnError' => true, 'targetClass' => Office::className(), 'targetAttribute' => ['office_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Дата',
            'event' => 'Статья',
            'summ' => 'Сумма',
            'person_id' => 'Person ID',
            'person_snp' => 'ФИО',
            'comment' => 'Коментарий',
            'office_id' => 'Офис',
            'type' => 'Тип',
        ];
    }

    public function afterFind()
    {
//        $this->type = 0 ? 'Расход' : 'Доход';

        $this->date = date('Y-m-d', strtotime($this->date));
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerson()
    {
        return $this->hasOne(Client::className(), ['id' => 'person_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffice()
    {
        return $this->hasOne(Office::className(), ['id' => 'office_id']);
    }

    public function getOfficeList()
    {
        return ArrayHelper::map(Office::find()->all(), 'id', 'name');
    }
}
