<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ReportMoneyOperations;

/**
 * ReportMoneyOperationsSearch represents the model behind the search form about `app\models\ReportMoneyOperations`.
 */
class ReportMoneyOperationsSearch extends ReportMoneyOperations
{
    public $date_to;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'summ', 'person_id', 'office_id', 'type'], 'integer'], // type  0 - расход, 1 - доход
            [['date', 'event', 'person_snp', 'comment', 'date_to', 'office_id', 'clientid'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ReportMoneyOperations::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'date' => SORT_DESC
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'summ' => $this->summ,
            'person_id' => $this->person_id,
            'office_id' => $this->office_id,
        ]);

        $query->andFilterWhere(['like', 'event', $this->event])
            ->andFilterWhere(['like', 'person_snp', $this->person_snp])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        if ($this->date!==null && $this->date_to!=null) {
            $query->andFilterWhere(['>=', 'date', date('Y-m-d', strtotime($this->date))])->andFilterWhere(['<=', 'date', date('Y-m-d', strtotime($this->date_to))]);
        }

        return $dataProvider;
    }

    public function searchDebits($params)
    {
        $query = ReportMoneyOperations::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'date' => SORT_DESC
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'type' => 1, // доход
            'summ' => $this->summ,
            'person_id' => $this->person_id,
            'office_id' => $this->office_id,
        ]);

        if ($this->date!==null && $this->date_to!=null) {
            $query->andFilterWhere(['>=', 'date', date('Y-m-d', strtotime($this->date))])->andFilterWhere(['<=', 'date', date('Y-m-d', strtotime($this->date_to))]);
        }

        $query->andFilterWhere(['like', 'event', $this->event])
            ->andFilterWhere(['like', 'person_snp', $this->person_snp])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }

    public function searchCredits($params)
    {
        $query = ReportMoneyOperations::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'date' => SORT_DESC
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'type' => 0, // расход
            'summ' => $this->summ,
            'person_id' => $this->person_id,
            'office_id' => $this->office_id,
        ]);

        if ($this->date!==null && $this->date_to!=null) {
            $query->andFilterWhere(['>=', 'date', date('Y-m-d', strtotime($this->date))])->andFilterWhere(['<=', 'date', date('Y-m-d', strtotime($this->date_to))]);
        }

        $query->andFilterWhere(['like', 'event', $this->event])
            ->andFilterWhere(['like', 'person_snp', $this->person_snp])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
