<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Tasks;

/**
 * TasksSearch represents the model behind the search form about `app\models\Tasks`.
 */
class TasksSearch extends Tasks
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'performer_id', 'exec_time', 'owner_id'], 'integer'],
            [['date_created', 'name', 'comment', 'date_close', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $user = Yii::$app->user->identity;
        $query = Tasks::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'performer_id' => $this->performer_id,
            'exec_time' => $this->exec_time,
            // $this->date_close,
            'owner_id' => $this->owner_id,
        ]);

        if ( $this->date_created != null ) {
            $query->andFilterWhere(['like', 'date_created', date('Y-m-d', strtotime($this->date_created))]);
        }

        if ( $this->date_close != null ) {
            $query->andFilterWhere(['like', 'date_close', date('Y-m-d', strtotime($this->date_close))]);
        }

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'status', $this->status]);

        if($user->isAdmin() == false)
        {
            $query->andFilterWhere(['performer_id' => $user->id]);
        }

        return $dataProvider;
    }
}
