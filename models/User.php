<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\web\IdentityInterface;


use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $middle_name
 * @property string $email
 * @property integer $category_id
 * @property integer $office_id
 * @property string $phone
 * @property string $date
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;

    const CATEGORY_ADMIN = 1;
    const CATEGORY_USER = 2;

    public $authKey = 'a'; // ДЛЯ ВРЕМЕНЕГО РЕШЕНИЯ ПРОБЛЕМЫ С АВТОРИЗАЦИЕЙ


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'middle_name', 'email', 'category_id', 'office_id', 'phone', 'password', 'is_banned'], 'required'],
            [['first_name', 'last_name', 'middle_name', 'phone'], 'string'],
            [['password'], 'string', 'min' => 6],
            [['category_id', 'office_id'], 'integer'],
            [['is_banned'], 'integer'],
            [['date', 'password_hash'], 'safe'],
            [['email'], 'string', 'max' => 255],
            ['email', 'unique'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            //['id', 'compare', 'compareValue' => 17, 'operator' => '!='],
            ['is_banned', 'validatorIsSelf'],
        ];
    }

    public function validatorIsSelf($attribute, $params)
    {
        if(!empty($this->id)){
            if($this->id == Yii::$app->user->id && $this->is_banned == 1){
                $this->addError($attribute,'Нельзя заблокировать себя.');
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'middle_name' => 'Отчество',
            'email' => 'Email',
            'password' => 'Пароль',
            'category_id' => 'Категория',
            'office_id' => 'Офис',
            'phone' => 'Телефон',
            'date' => 'Дата регистрации',
            'is_banned' => 'Заблокирован',
        ];
    }

    public function beforeSave($insert)
    {
        if(parent::beforeSave($insert))
        {
            $passwordHash = Yii::$app->getSecurity()->generatePasswordHash($this->password);

            $this->password_hash = $passwordHash;

            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by email
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
        return false;
    }

    /**
     * @return bool
     */
    public function isAdmin()
    {
        return $this->category_id == self::CATEGORY_ADMIN;
    }

    public function isBanned()
    {
        return $this->is_banned;
    }

//    public function setIsBanned($value)
//    {
////        $this->is_banned = 0;
//        $this->attributes['is_banned'] = 0;
//    }

//    public function setPassword($value)
//    {
//        $this->attributes['is_banned'] = SecurityHelper::generatePassword($value);
//    }
}
