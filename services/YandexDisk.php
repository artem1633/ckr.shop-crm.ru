<?php 

namespace app\services;

use Yii;
use yii\base\Exception;


class YandexDisk {

	const AUTHORIZATION_TOKEN = 'AQAAAAAe7ezWAARqK-A0FJwrSkt0l4YP17b_3Ac';
    // токен из приложения Полигон
	//const AUTHORIZATION_TOKEN = 'AQAAAAAHqIWYAADLWzMfPusfZkKMtcOKTGydPyY';
	const SERVER_URL = 'http://185.22.63.24/';
    // адрес машины разработчика
	//const SERVER_URL = 'http://ckr.local/';

	/**
	 * Создает дерикторию на Яндекс Диске
	 * @param String $path. Путь до создаваемой папки (кодируемый в url формате)
	 * @return String $out. Ответ от сервера
	 */
	public static function mkdir($path)
	{
		$ch = curl_init('https://cloud-api.yandex.net/v1/disk/resources/?path='.$path);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_PUT, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
				'Authorization: '.self::AUTHORIZATION_TOKEN,
			]);

		$out = curl_exec($ch);
		curl_close($ch);

		return $out;

	}

	/**
	 * Создает дерикторию на Яндекс Диске
	 * @param String $path. Путь до файла, который хотим скачать
	 * @return String $out. Ответ от сервера.
	 */
	public static function download($path)
	{
		$ch = curl_init('https://cloud-api.yandex.net/v1/disk/resources/download?path='.$path);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
				'Authorization: '.self::AUTHORIZATION_TOKEN,
			]);

		$out = json_decode(curl_exec($ch));
		curl_close($ch);

		return $out;

	}

	/**
	 * Удаляет дерикторию с Яндекс Диска
	 * @param String $path. Путь до удаляемой папки (кодируемый в url формате)
	 * @return String $out. Ответ от сервера
	 */
	public static function rmdir($path)
	{
		$ch = curl_init('https://cloud-api.yandex.net/v1/disk/resources/?path='.$path);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
				'Authorization: '.self::AUTHORIZATION_TOKEN,
			]);

		$out = curl_exec($ch);
		curl_close($ch);

		return $out;
	}

	/**
	 * Перемещает дерикторию или папку
	 * @param String $from. Путь до перемещаемой дериктории или файла (кодируемый в url формате)
	 * @param String $path. Путь назначения (кодируемый в url формате)
	 */
	public static function move($from, $path)
	{
		$ch = curl_init('https://cloud-api.yandex.net/v1/disk/resources/move');

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,
		            "from=$from&path=$path");
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
				'Authorization: '.self::AUTHORIZATION_TOKEN,
			]);

		$out = curl_exec($ch);
		curl_close($ch);

		return $out;
	}

	/**
	 * Копирует дерикторию или папку с Яндекс Диска
	 * @param String $from. Путь до копируемого дериктории или файла (кодируемый в url формате)
	 * @param String $path. Путь назначения (кодируемый в url формате)
	 */
	public static function copy($from, $path)
	{
		$ch = curl_init('https://cloud-api.yandex.net/v1/disk/resources/copy');

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,
		            "from=$from&path=$path");
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
				'Authorization: '.self::AUTHORIZATION_TOKEN,
			]);

		$out = curl_exec($ch);
		curl_close($ch);

		return $out;
	}

	/**
	 * Загружает файл из интернета на Яндекс Диск
	 * @param String $filePath. Путь до файла от папки web. Например img/photo.jpg
	 * @param String $uploadPath. Путь загрузки файла на Яндекс Диске (кодируемый в url формате) 
	 */
	public static function uploadFile($filePath, $uploadPath)
	{
		// Отправляем запрос на запрос url для загрузки
		$ch = curl_init('https://cloud-api.yandex.net/v1/disk/resources/upload?path='.$uploadPath);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
				'Authorization: '.self::AUTHORIZATION_TOKEN,
			]);
		$putUrl = json_decode(curl_exec($ch));
		curl_close($ch);

        try {
            // Отправляем PUT запрос для загрузки фала
            $image = fopen(self::SERVER_URL.$filePath, 'rb');
        } catch (Exception $e) {
            Yii::warning("Проблема при загрузке скана.");
        }

		if(!isset($putUrl->href)){
			throw new Exception($putUrl->message);
		}

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 2);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_BINARYTRANSFER, 1);
		curl_setopt($curl, CURLOPT_URL, $putUrl->href);

		curl_setopt($curl, CURLOPT_PUT, 1);
		curl_setopt($curl, CURLOPT_INFILE, $image);
		curl_setopt($curl, CURLOPT_INFILESIZE, filesize($filePath));

		$result = curl_exec($curl);

		curl_close($curl);


	}

}


 ?>