<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Calendar */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="calendar-form">
	<div class="box box-default">
		<div class="box-body">
			<?php $form = ActiveForm::begin(); ?>

		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'date')->textInput() ?>				
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'day')->textInput() ?>				
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'month')->textInput() ?>				
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'year')->textInput() ?>				
			</div>
		</div>
   	
<div style="display:none">
</div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

		</div>
	</div>
</div>
