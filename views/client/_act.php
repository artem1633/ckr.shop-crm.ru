<html>

<head>
    <meta http-equiv=Content-Type content="text/html; charset=utf-8">
    <meta name=Generator content="Microsoft Word 12 (filtered)">
<!--    <base href="http://quickdoc.ru">-->
    <title></title>
    <style>
        <!--
        /* Font Definitions */
        @font-face
        {font-family:"Arial Narrow";
            panose-1:2 11 6 6 2 2 2 3 2 4;}
        /* Style Definitions */
        p.MsoNormal, li.MsoNormal, div.MsoNormal
        {margin:0cm;
            margin-bottom:.0001pt;
            font-size:10.0pt;
            font-family:"Times New Roman","serif";}
        a:link, span.MsoHyperlink
        {color:blue;
            text-decoration:underline;}
        a:visited, span.MsoHyperlinkFollowed
        {color:purple;
            text-decoration:underline;}
        @page Section1
        {size:595.3pt 841.9pt;
            margin:1.0cm 1.0cm 21.3pt 42.55pt;}
        div.Section1
        {page:Section1;}
        -->
    </style>

</head>

<body>

<p class="application">Бланк акта о приемке выполненных работ (оказанных услуг)</p> <br>

<H5 align="center" style="color:Black">
    <b>
        Акт № %contract.number% от
        *date_full*<br>
        о приемке выполненных работ<br>
        (оказанных услуг)
        </br>
    </b>
</H5>

<br>

<p>Исполнитель ИП Прокопенко А.Н.</p>
<p>Заказчик %name% </p>
        *SERVICES_CYCLE*
<p> </p>
<p><i>Всего оказано услуг на сумму: _________________________________________________________________рублей <u>__</u>коп.,</i></p>
<p><i>Вышеперечисленные работы (услуги) выполнены полностью, в срок и с надлежащим качеством. Заказчик претензий по объему, качеству и срокам оказания услуг претензий не имеет.</i></p>
<p>Исполнитель <i><u> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </u></i> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Заказчик ______________________________________________</p>


</body>

</html>




