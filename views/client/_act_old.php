<?php
/**
 * Created by PhpStorm.
 * User: Ilusha
 * Date: 13.04.2017
 * Time: 23:13

 * @property app\models\Creditor $creditor
 */

$monthes = [
    '01' => 'Января',
    '02' => 'Февраля',
    '03' => 'Марта',
    '04' => 'Апреля',
    '05' => 'Мая',
    '06' => 'Июня',
    '07' => 'Июля',
    '08' => 'Августа',
    '09' => 'Сентября',
    '10' => 'Октября',
    '11' => 'Ноября',
    '12' => 'Декабря',
];

$sum = 0;

?>

<html>

<head>
    <meta http-equiv=Content-Type content="text/html; charset=utf-8">
    <meta name=Generator content="Microsoft Word 12 (filtered)">
<!--    <base href="http://quickdoc.ru">-->
    <title>Бланк акта о приемке выполненных работ (оказанных услуг)</title>
    <style>
        <!--
        /* Font Definitions */
        @font-face
        {font-family:"Arial Narrow";
            panose-1:2 11 6 6 2 2 2 3 2 4;}
        /* Style Definitions */
        p.MsoNormal, li.MsoNormal, div.MsoNormal
        {margin:0cm;
            margin-bottom:.0001pt;
            font-size:10.0pt;
            font-family:"Times New Roman","serif";}
        a:link, span.MsoHyperlink
        {color:blue;
            text-decoration:underline;}
        a:visited, span.MsoHyperlinkFollowed
        {color:purple;
            text-decoration:underline;}
        @page Section1
        {size:595.3pt 841.9pt;
            margin:1.0cm 1.0cm 21.3pt 42.55pt;}
        div.Section1
        {page:Section1;}
        -->
    </style>

</head>

<body lang=RU link=blue vlink=purple>

<div class=Section1 style="max-width: 630px; margin: auto;">

<!--    <p class=MsoNormal align=center style='text-align:center'><span-->
<!--            style='position:relative;z-index:1'><span style='left:0px;position:absolute;-->
<!--left:-17px;top:-17px;width:202px;height:60px'><img width=202 height=60-->
<!--                                                   src="img/act.png"></span></span><b><span-->
<!--                style='font-size:12.0pt;font-family:"Arial","sans-serif"'>                                                             -->
<!--</span></b></p>-->

    <p class=MsoNormal align=center style='text-align:center'><b><span
                style='font-size:12.0pt;font-family:"Arial","sans-serif"'>&nbsp;</span></b></p>

    <p class=MsoNormal align=center style='text-align:center'><b><span
                style='font-size:12.0pt;'>&nbsp;</span></b></p>

    <p class=MsoNormal align=center style='text-align:center'><b><span
                style='font-size:12.0pt;'>Акт № <u><?=$client->contract->number?> от
«<?=date('d', strtotime($client->contract->date_created))?>»_<?=$monthes[date('m', strtotime($client->contract->date_created))]?>_<?=date('Y', strtotime($client->contract->date_created))?>г.</u></span></b></p>

    <p class=MsoNormal align=center style='text-align:center'><b><span
                style='font-size:12.0pt;font-family:"Arial","sans-serif"'>о приемке выполненных
работ</span></b></p>

    <p class=MsoNormal align=center style='text-align:center'><b><span
                style='font-size:12.0pt;font-family:"Arial","sans-serif"'>(оказанных услуг)</span></b></p>

    <p class=MsoNormal>&nbsp;</p>

    <p class=MsoNormal style='line-height:150%;'>Исполнитель <u>ИП Прокопенко А.Н.</u></p>

    <p class=MsoNormal style='margin-bottom:2.0pt'>Заказчик  <u><?=$client->name?></u></p>

    <table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0
           style='margin-left:.25pt;border-collapse:collapse;border:none'>
        <tr style='height:19.85pt'>
            <td width=26>
                <p class=MsoNormal align=center style='text-align:center'><span
                        style='font-size:9.0pt'>№</span></p>
            </td>
            <td width=503 >
                <p class=MsoNormal align=center style='text-align:center'><span
                        style='font-size:9.0pt'>Наименование работы (услуги)</span></p>
            </td>
            <td width=97 >
                <p class=MsoNormal align=center style='text-align:center'><span
                        style='font-size:9.0pt'>Дата </span></p>
            </td>
        </tr>
        <?php $counter = 1;
        foreach($creditors as $creditor){
            foreach ($creditor->documents as $document){ ?>
            <tr style='height:17.0pt'>
                <td width=26>
                    <p class=MsoNormal align=center style='text-align:center'><span
                                style='font-size:9.0pt'><?=$counter?></span></p>
                </td>
                <td width=503>
                    <p class=MsoNormal style='margin-left:2.85pt'><span style='font-size:8.0pt;
  font-family:"Arial Narrow","sans-serif"'><?=$document->name?></span></p>
                </td>
                <td width=97>
                    <p class=MsoNormal align=center style='text-align:center'><?=date('Y-m-d', strtotime($document->date_created))?></p>
                </td>
            </tr>
        <?php $counter++; } } ?>
    </table>

    <p class=MsoNormal>&nbsp;</p>

    <p class=MsoNormal style='line-height:150%'><i>Всего оказано услуг на сумму: _________________________________________________________________рублей<u>__</u>коп.,</i></p>

    <p class=MsoNormal><i>Вышеперечисленные работы (услуги) выполнены полностью, в
            срок и с надлежащим качеством. Заказчик претензий по объему, качеству и срокам
            оказания услуг претензий не имеет.</i></p>

    <p class=MsoNormal>&nbsp;</p>

    <p class=MsoNormal>Исполнитель ____<i><u>ИП Прокопенко А.Н.</u></i>________               
         Заказчик ______________________________________________</p>

    <p class=MsoNormal>                                                  </p>

</div>

</body>

</html>




