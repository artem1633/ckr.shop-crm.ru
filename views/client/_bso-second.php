<?php

$zeroNumberCount = 5 - strlen($payment->receipt_number);

$number = '';

for ($i = 0; $i < $zeroNumberCount; $i++)
{
    $number .= '0';
}

$number .= $payment->receipt_number;

?>

<html>

<head>
    <meta http-equiv=Content-Type content="text/html; charset=utf-8">
    <meta name=Generator content="Microsoft Word 12 (filtered)">
    <style>
        <!--
        /* Font Definitions */

        @font-face {
            font-family: Arial;
            panose-1: 2 4 5 3 5 4 6 3 2 4;
        }
        @font-face {
            font-family: Arial;
            panose-1: 2 15 5 2 2 2 4 3 2 4;
        }
        @font-face {
            font-family: Arial;
            panose-1: 2 11 6 4 3 5 4 4 2 4;
        }
        @font-face {
            font-family: Arial;
            panose-1: 2 11 6 3 2 1 2 2 2 4;
        }
        @font-face {
            font-family: Arial;
            panose-1: 2 4 5 2 5 4 5 2 3 3;
        }
        @font-face {
            font-family: Arial;
            panose-1: 2 11 8 6 3 9 2 5 2 4;
        }
        /* Style Definitions */

        p.MsoNormal,
        li.MsoNormal,
        div.MsoNormal {
            margin-top: 0cm;
            margin-right: 0cm;
            margin-bottom: 10.0pt;
            margin-left: 0cm;
            line-height: 115%;
            font-size: 11.0pt;
            font-family: "Calibri", "sans-serif";
        }
        p.MsoAcetate,
        li.MsoAcetate,
        div.MsoAcetate {
            mso-style-link: "????? ??????? ????";
            margin: 0cm;
            margin-bottom: .0001pt;
            font-size: 8.0pt;
            font-family: "Tahoma", "sans-serif";
        }
        span.a {
            mso-style-name: "????? ??????? ????";
            mso-style-link: "????? ???????";
            font-family: "Tahoma", "sans-serif";
        }
        .MsoPapDefault {
            margin-bottom: 10.0pt;
            line-height: 115%;
        }
        @page Section1 {
            size: 595.3pt 841.9pt;
            margin: 21.3pt 36.0pt 36.0pt 4.0cm;
        }
        div.Section1 {
            page: Section1;
        }
        -->
    </style>

</head>

<body lang=RU>

<div class=Section1>

    <table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=483 style='border-collapse:collapse;border:none'>
        <tr style='height:10.5pt'>
            <td width=125 nowrap colspan=2 valign=bottom style='border: none;'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Franklin Gothic Medium","sans-serif";
  color:black'>ИП Прокопенко А.Н.</span>
                </p>
            </td>
            <td width=358 nowrap colspan=5 valign=bottom style='border: none;'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='position:relative;z-index:1'><span style='position:absolute;
  left:280px;top:-2px;width:73px;height:76px'></span></span><span style='font-size:6.0pt;
  color:black'>Разработана в соотв.с Постановлением Правительства РФ №359 от 06.05.2008г.</span>
                </p>
            </td>
        </tr>
        <tr style='height:10.5pt'>
            <td width=125 nowrap colspan=2 valign=bottom style='border: none;'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Franklin Gothic Medium","sans-serif";
  color:black'>ИНН360408101210</span>
                </p>
            </td>
            <td width=18 nowrap valign=bottom style='border: none;'></td>
            <td width=140 nowrap valign=bottom style='border: none;'></td>
            <td width=81 nowrap valign=bottom style='border: none;'></td>
            <td width=119 nowrap colspan=2 rowspan=4 valign=bottom style='border: none;'></td>
        </tr>
        <tr style='height:10.5pt'>
            <td width=125 nowrap colspan=2 valign=bottom style='border: none;'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Franklin Gothic Medium","sans-serif";
  color:black'>ОГРН 315366800050012</span>
                </p>
            </td>
            <td width=18 nowrap valign=bottom style='border: none;'></td>
            <td width=140 nowrap valign=bottom style='border: none;'></td>
            <td width=81 nowrap valign=bottom style='border: none;'></td>
        </tr>
        <tr style='height:10.5pt'>
            <td width=283 nowrap colspan=4 valign=bottom style='border: none;'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"times","sans-serif";
  color:black'>Воронежская обл., г.Борисоглебск, ул. Устиновская, д.36</span>
                </p>
            </td>
            <td width=81 nowrap valign=bottom style='border: none;'></td>
        </tr>
        <tr style='height:10.5pt'>
            <td width=125 nowrap colspan=2 valign=bottom style='border: none;'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Franklin Gothic Medium","sans-serif";
  color:black'>тел. 89521063637</span>
                </p>
            </td>
            <td width=18 nowrap valign=bottom style='border: none;'></td>
            <td width=140 nowrap valign=bottom style='border: none;'></td>
            <td width=81 nowrap valign=bottom style='border: none;'></td>
        </tr>
        <tr style='height:27.0pt'>
            <td width=143 nowrap colspan=3 valign=bottom style='border: none;'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><b><span style='font-size:14.0pt;font-family:"Georgia","serif";
  color:black'>КВИТАНЦИЯ </span></b>
                </p>
            </td>
            <td width=140 nowrap valign=bottom style='border: none;'>
                <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:right;line-height:normal'><b><span style='color:black'>серия</span></b><span style='font-size:20.0pt;font-family:"Impact","sans-serif";color:black'> ЦКР</span>
                </p>
            </td>
            <td width=114 nowrap colspan=2 valign=bottom style='border: none;'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:15.0pt;font-family:"Impact","sans-serif";
  color:black'>№</span><span style='font-size:20.0pt;font-family:"Impact","sans-serif";
  color:black'> <?=$number?></span>
                </p>
            </td>
            <td width=86 nowrap valign=bottom style='border: none;'></td>
        </tr>
        <tr style='height:10.5pt'>
            <td width=103 nowrap valign=bottom style='border: none;'></td>
            <td width=22 nowrap valign=bottom style='border: none;'></td>
            <td width=18 nowrap valign=bottom style='border: none;'></td>
            <td width=140 nowrap valign=bottom style='border: none;'></td>
            <td width=81 nowrap valign=bottom style='border: none;'></td>
            <td width=33 nowrap valign=bottom style='border: none;'></td>
            <td width=86 nowrap valign=bottom style='border: none;'></td>
        </tr>
        <tr style='height:15.75pt'>
            <td width=125 nowrap colspan=2 valign=bottom style='border: none; width: 250.0pt;'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:12.0pt;font-family:"Georgia","serif";
  color:black'>Заказчик:</span>
                </p>
                <p style="width: 1000px; margin-top: 0;"><?=$payment->client->name?></p>
            </td>
            <td width=18 nowrap valign=bottom style='border: none;'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='color:black'>&nbsp;</span>
                </p>
            </td>
            <td width=140 nowrap valign=bottom style='border: none;'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='color:black'>&nbsp;</span>
                </p>
            </td>
            <td width=81 nowrap valign=bottom style='border: none;'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:12.0pt;font-family:"Georgia","serif";
  color:black'>Дата:&nbsp; <span style="position: absolute;"><?=date('d.m.Y', strtotime($payment->date))?></span></span>
                </p>
            </td>
            <td width=33 nowrap valign=bottom style='border: none;'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='color:black'>&nbsp;</span>
                </p>
            </td>
            <td width=86 nowrap valign=bottom style='border: none;'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='color:black'>&nbsp;</span>
                </p>
            </td>
        </tr>
        <tr style='height:15.75pt'>
                <tr style='height:15.0pt'>
                    <td width=397 nowrap colspan=6 valign=bottom >
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span>Наименование работ(услуг), видов доплат</span></b>
                        </p>
                    </td>
                    <td width=86 nowrap valign=bottom >
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><b><span style='font-family:"Georgia","serif";
  color:black'>Сумма</span></b>
                        </p>
                    </td>
                </tr>
                <tr style='height:15.0pt;'>
                    <td width=397 nowrap colspan=6 valign=bottom >
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-family:"Georgia","serif";
  color:black'><?=$payment->name?></span>
                        </p>
                    </td>
                    <td width=86 nowrap valign=bottom >
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-family:"Georgia","serif";
  color:black'><?=$payment->price?></span>
                        </p>
                    </td>
                </tr>
                <tr style='height:15.0pt'>
                    <td width=397 nowrap colspan=6 valign=bottom >
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-family:"Georgia","serif";
  color:black'>&nbsp;</span>
                        </p>
                    </td>
                    <td width=86 nowrap valign=bottom >
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-family:"Georgia","serif";
  color:black'>&nbsp;</span>
                        </p>
                    </td>
                </tr>
                <tr style='height:15.0pt'>
                    <td width=397 nowrap colspan=6 valign=bottom >
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-family:"Georgia","serif";
  color:black'>&nbsp;</span>
                        </p>
                    </td>
                    <td width=86 nowrap valign=bottom >
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-family:"Georgia","serif";
  color:black'>&nbsp;</span>
                        </p>
                    </td>
                </tr>
                <tr style='height:15.0pt'>
                    <td width=397 nowrap colspan=6 valign=bottom >
                        <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:right;line-height:normal'><b><span style='font-family:"Georgia","serif";
  color:black'>Итого:</span></b>
                        </p>
                    </td>
                    <td width=86 nowrap valign=bottom >
                        <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-family:"Georgia","serif";
  color:black'><?=$payment->price?></span>
                        </p>
                    </td>
                </tr>
        </tr>
        <tr style='height:15.0pt'>
            <td width=483 nowrap colspan=7 valign=bottom style='border: none;'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.5pt;font-family:"Georgia","serif";
  color:black'>Деньги в сумме
  _<span style="font-size: 12.5pt;"><?=$payment->price?>р.</span>______________________________________________получил</span>
                </p>
            </td>
        </tr>
        <tr style='height:15.0pt'>
            <td width=103 nowrap valign=bottom style='border: none;'></td>
            <td width=22 nowrap valign=bottom style='border: none;'></td>
            <td width=18 nowrap valign=bottom style='border: none;'></td>
            <td width=140 nowrap valign=bottom style='border: none;'></td>
            <td width=81 nowrap valign=bottom style='border: none;'></td>
            <td width=33 nowrap valign=bottom style='border: none;'></td>
            <td width=86 nowrap valign=bottom style='border: none;'></td>
        </tr>
        <tr style='height:15.0pt'>
            <td width=103 nowrap valign=bottom style='border: none;'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='color:black'>М.П.</span>
                </p>
            </td>
            <td width=22 nowrap valign=bottom style='width:16.75pt;border:none;
  border-bottom:solid windowtext 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.0pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='color:black'>&nbsp;</span>
                </p>
            </td>
            <td width=18 nowrap valign=bottom style='width:13.6pt;border:none;border-bottom:
  solid windowtext 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.0pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='color:black'>&nbsp;</span>
                </p>
            </td>
            <td width=140 nowrap valign=bottom style='width:105.15pt;border:none;
  border-bottom:solid windowtext 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.0pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='color:black'>&nbsp;</span>
                </p>
            </td>
            <td width=81 nowrap valign=bottom style='width:60.9pt;border:none;border-bottom:
  solid windowtext 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.0pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='color:black'>&nbsp;</span>
                </p>
            </td>
            <td width=33 nowrap valign=bottom style='width:24.4pt;border:none;border-bottom:
  solid windowtext 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.0pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='color:black'>&nbsp;</span>
                </p>
            </td>
            <td width=86 nowrap valign=bottom style='width:64.5pt;border:none;border-bottom:
  solid windowtext 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.0pt'>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='color:black'>&nbsp;</span>
                </p>
            </td>
        </tr>
    </table>

    <p class=MsoNormal>&nbsp;</p>

</div>

</body>

</html>