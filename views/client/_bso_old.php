<?php
/**
 * Created by PhpStorm.
 * User: Ilusha
 * Date: 12.04.2017
 * Time: 21:23
 */

$zeroNumberCount = 5 - strlen($payment->receipt_number);

$number = '';

for ($i = 0; $i < $zeroNumberCount; $i++)
{
    $number .= '0';
}

$number .= $payment->receipt_number;

?>
<html>

<head><meta content="text/html; charset=UTF-8" http-equiv="content-type">
    <link rel="stylesheet" href="css/bso.css">
</head>

<body>

<table class="c35">
    <tbody>
    <tr class="c3">
        <td class="c25" colspan="2" rowspan="1">
            <p class="c2"><span class="c6">ИП Прокопенко А.Н.</span>
            </p>
        </td>
        <td class="c42" colspan="5" rowspan="1">
            <p class="c2"><span class="c29">Разработана в соотв.с Постановлением Правительства РФ №359 от 06.05.2008г.</span><span style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 73.00px; height: 76.00px; position: absolute;"><img alt="bso" src="/img/bso.jpg" style="width: 73.00px; height: 76.00px; margin-left: -0.00px; margin-top: -0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);" title=""></span>
            </p>
        </td>
    </tr>
    <tr class="c3">
        <td class="c25" colspan="2" rowspan="1">
            <p class="c2"><span class="c6">ИНН360408101210</span>
            </p>
        </td>
        <td class="c10" colspan="1" rowspan="1">
            <p class="c2 c5"><span class="c12"></span>
            </p>
        </td>
        <td class="c11" colspan="1" rowspan="1">
            <p class="c2 c5"><span class="c12"></span>
            </p>
        </td>
        <td class="c26" colspan="1" rowspan="1">
            <p class="c2 c5"><span class="c12"></span>
            </p>
        </td>
        <td class="c30" colspan="2" rowspan="4">
            <p class="c24 c5"><span class="c12"></span>
            </p>
        </td>
    </tr>
    <tr class="c3">
        <td class="c25" colspan="2" rowspan="1">
            <p class="c2"><span class="c6">ОГРН 315366800050012</span>
            </p>
        </td>
        <td class="c10" colspan="1" rowspan="1">
            <p class="c2 c5"><span class="c12"></span>
            </p>
        </td>
        <td class="c11" colspan="1" rowspan="1">
            <p class="c2 c5"><span class="c12"></span>
            </p>
        </td>
        <td class="c26" colspan="1" rowspan="1">
            <p class="c2 c5"><span class="c12"></span>
            </p>
        </td>
    </tr>
    <tr class="c3">
        <td class="c57" colspan="4" rowspan="1">
            <p class="c2"><span class="c6">Воронежская обл., г.Борисоглебск, ул. Устиновская, д.36</span>
            </p>
        </td>
        <td class="c26" colspan="1" rowspan="1">
            <p class="c2 c5"><span class="c12"></span>
            </p>
        </td>
    </tr>
    <tr class="c3">
        <td class="c25" colspan="2" rowspan="1">
            <p class="c2"><span class="c6">тел. 89521063637</span>
            </p>
        </td>
        <td class="c10" colspan="1" rowspan="1">
            <p class="c2 c5"><span class="c12"></span>
            </p>
        </td>
        <td class="c11" colspan="1" rowspan="1">
            <p class="c2 c5"><span class="c12"></span>
            </p>
        </td>
        <td class="c26" colspan="1" rowspan="1">
            <p class="c2 c5"><span class="c12"></span>
            </p>
        </td>
    </tr>
    <tr class="c47">
        <td class="c27" colspan="3" rowspan="1">
            <p class="c2"><span class="c14">КВИТАНЦИЯ </span>
            </p>
        </td>
        <td class="c11" colspan="1" rowspan="1">
            <p class="c50"><span class="c39">серия</span><span class="c46">&nbsp;ЦКР</span>
            </p>
        </td>
        <td class="c33" colspan="2" rowspan="1">
            <p class="c2"><span class="c40">№</span><span class="c46">&nbsp;<?=$number?></span>
            </p>
        </td>
        <td class="c20" colspan="1" rowspan="1">
            <p class="c2 c5"><span class="c12"></span>
            </p>
        </td>
    </tr>
    <tr class="c3">
        <td class="c43" colspan="1" rowspan="1">
            <p class="c2 c5"><span class="c12"></span>
            </p>
        </td>
        <td class="c9" colspan="1" rowspan="1">
            <p class="c2 c5"><span class="c12"></span>
            </p>
        </td>
        <td class="c10" colspan="1" rowspan="1">
            <p class="c2 c5"><span class="c12"></span>
            </p>
        </td>
        <td class="c11" colspan="1" rowspan="1">
            <p class="c2 c5"><span class="c12"></span>
            </p>
        </td>
        <td class="c26" colspan="1" rowspan="1">
            <p class="c2 c5"><span class="c12"></span>
            </p>
        </td>
        <td class="c19" colspan="1" rowspan="1">
            <p class="c2 c5"><span class="c12"></span>
            </p>
        </td>
        <td class="c20" colspan="1" rowspan="1">
            <p class="c2 c5"><span class="c12"></span>
            </p>
        </td>
    </tr>
    <tr class="c1">
        <td class="c54" colspan="2" rowspan="1">
            <p class="c2"><span class="c51">Заказчик: </span>
            </p>
            <p style="width: 182px;"><?=$payment->client->name?></p>
        </td>
        <td class="c45" colspan="1" rowspan="1">
            <p class="c2"><span class="c12">&nbsp;</span>
            </p>
        </td>
        <td class="c52" colspan="1" rowspan="1">
            <p class="c2"><span class="c12">&nbsp;</span>
            </p>
        </td>
        <td class="c38" colspan="1" rowspan="1">
            <p class="c2"><span class="c51">Дата: &nbsp;<span style="position: absolute;"><?=date('d.m.Y', strtotime($payment->date))?></span></span>
            </p>
        </td>
        <td class="c34" colspan="1" rowspan="1">
            <p class="c2"><span class="c12">&nbsp;</span>
            </p>
        </td>
        <td class="c37" colspan="1" rowspan="1">
            <p class="c2"><span class="c12">&nbsp;</span>
            </p>
        </td>
    </tr>
    <tr class="c1">
        <td class="c8" colspan="6" rowspan="1">
            <p class="c24"><span class="c28">Наименование работ(услуг), видов доплат</span>
            </p>
        </td>
        <td class="c31" colspan="1" rowspan="1">
            <p class="c24"><span class="c28">Сумма</span>
            </p>
        </td>
    </tr>
    <tr class="c1">
        <td class="c8" colspan="6" rowspan="1">
            <p class="c24"><span class="c0"><?=$payment->name?></span>
            </p>
        </td>
        <td class="c31" colspan="1" rowspan="1">
            <p class="c24"><span class="c0"><?=$payment->price?></span>
            </p>
        </td>
    </tr>
    <tr class="c1">
        <td class="c8" colspan="6" rowspan="1">
            <p class="c24"><span class="c0">&nbsp;</span>
            </p>
        </td>
        <td class="c31" colspan="1" rowspan="1">
            <p class="c24"><span class="c0">&nbsp;</span>
            </p>
        </td>
    </tr>
    <tr class="c1">
        <td class="c8" colspan="6" rowspan="1">
            <p class="c24"><span class="c0">&nbsp;</span>
            </p>
        </td>
        <td class="c31" colspan="1" rowspan="1">
            <p class="c24"><span class="c0">&nbsp;</span>
            </p>
        </td>
    </tr>
    <tr class="c1">
        <td class="c49" colspan="6" rowspan="1">
            <p class="c50"><span class="c28">ИТОГО: </span>
            </p>
        </td>
        <td class="c31" colspan="1" rowspan="1">
            <p class="c24"><span class="c0"><?=$payment->price?></span>
            </p>
        </td>
    </tr>
    <tr class="c1">
        <td class="c44" colspan="7" rowspan="1">
            <p class="c2"><span class="c16">Деньги в сумме  ___<span style="font-size: 12.5pt;"><?=$payment->price?>р.</span>_________________________________________получил</span>
            </p>
        </td>
    </tr>
    <tr class="c1">
        <td class="c43" colspan="1" rowspan="1">
            <p class="c2 c5"><span class="c12"></span>
            </p>
        </td>
        <td class="c9" colspan="1" rowspan="1">
            <p class="c2 c5"><span class="c12"></span>
            </p>
        </td>
        <td class="c10" colspan="1" rowspan="1">
            <p class="c2 c5"><span class="c12"></span>
            </p>
        </td>
        <td class="c11" colspan="1" rowspan="1">
            <p class="c2 c5"><span class="c12"></span>
            </p>
        </td>
        <td class="c26" colspan="1" rowspan="1">
            <p class="c2 c5"><span class="c12"></span>
            </p>
        </td>
        <td class="c19" colspan="1" rowspan="1">
            <p class="c2 c5"><span class="c12"></span>
            </p>
        </td>
        <td class="c20" colspan="1" rowspan="1">
            <p class="c2 c5"><span class="c12"></span>
            </p>
        </td>
    </tr>
    <tr class="c1">
        <td class="c43" colspan="1" rowspan="1">
            <p class="c2"><span class="c12">м.п.</span>
            </p>
        </td>
        <td class="c4" colspan="1" rowspan="1">
            <p class="c2"><span class="c12">&nbsp;</span>
            </p>
        </td>
        <td class="c45" colspan="1" rowspan="1">
            <p class="c2"><span class="c12">&nbsp;</span>
            </p>
        </td>
        <td class="c52" colspan="1" rowspan="1">
            <p class="c2"><span class="c12">&nbsp;</span>
            </p>
        </td>
        <td class="c38" colspan="1" rowspan="1">
            <p class="c2"><span class="c12">&nbsp;</span>
            </p>
        </td>
        <td class="c34" colspan="1" rowspan="1">
            <p class="c2"><span class="c12">&nbsp;</span>
            </p>
        </td>
        <td class="c37" colspan="1" rowspan="1">
            <p class="c2"><span class="c12">&nbsp;</span>
            </p>
        </td>
    </tr>
    </tbody>
</table>

</body>

</html>