<?php
/**
 * Created by PhpStorm.
 * User: Ilusha
 * Date: 26.04.2017
 * Time: 17:42
 */

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<p><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /></p>
<p><strong>Договор на оказание юридических услуг с физическим лицом № </strong><strong>ЦКР-0001/16</strong></p>
<p><span style="font-weight: 400;">г.Борисоглебск</span><span style="font-weight: 400;">&nbsp;&nbsp;&nbsp; </span><span style="font-weight: 400;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><strong>&laquo;01&raquo; марта 2016г.</strong></p>
<p>&nbsp;</p>
<p><span style="font-weight: 400;">Индивидуальный предприниматель Прокопенко Анастасия Николаевна, ИНН 360408101210, ОГРН 315366800050012, именуемое далее "Исполнитель", с одной стороны и </span><strong>Иванов Иван Иванович, паспор 1807 1023456 выдан Отделением УФМС России по Воронежской области в Грибановском районе, 12.03.2011г., 362-016, адрес регистрации: Воронежская область, г.Борисоглебск, ул.Советская д.17, кв.7,</strong><span style="font-weight: 400;"> &nbsp;</span><span style="font-weight: 400;">действующий (ая) от своего имени, именуемый (ая) далее "Заказчик", с другой стороны, вместе именуемые &laquo;Стороны&raquo;, а по отдельности &laquo;Сторона&raquo;, заключили настоящий договор о следующем:</span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<ul>
    <li><strong><strong>Предмет договора </strong></strong></li>
</ul>
<p>&nbsp;</p>
<p><br /><br /></p>
<ol>
    <li style="font-weight: 400;"><span style="font-weight: 400;">Исполнитель обязуется по поручению Заказчика оказать ему юридические услуги в объеме и порядке, оговоренном настоящим Договором на оказание юридических услуг с физическим лицом, далее по тексту &laquo;Договор&raquo;, а Заказчик обязуется обеспечить Исполнителя документами, сведениями и средствами, необходимыми для оказания юридических услуг и выплатить ему вознаграждение на условиях и в порядке, предусмотренных настоящим Договором. </span></li>
    <li style="font-weight: 400;"><span style="font-weight: 400;">Заказчик обязуется оформить и предоставить нотариально заверенную доверенность выданную на компанию Исполнителя, на право представлять интересы Заказчика, с правом передоверия. </span></li>
</ol>
<p>&nbsp;</p>
<ol>
    <li style="font-weight: 400;"><span style="font-weight: 400;">Поручение к Исполнителю Заказчик оформляет &laquo;Заявлением&raquo; (Приложение № 2), которое является неотъемлемой частью Договора, а также определяет сущность услуги и поручения. </span></li>
</ol>
<p>&nbsp;</p>
<ol>
    <li style="font-weight: 400;"><span style="font-weight: 400;">Объем юридических услуг оказываемых Исполнителем в рамках настоящего Договора определяется тарифами и услугами изложенными в Приложении № 1 Договора и являющейся его неотъемлемой частью, а также включает в себя: </span></li>
</ol>
<p>&nbsp;</p>
<p><span style="font-weight: 400;">- юридические консультации по вопросам действующего российского законодательства, в том числе подбор и предоставление необходимых для исполнения поручения Заказчика нормативных актов;</span></p>
<p><span style="font-weight: 400;">- выбор наиболее оптимального плана по урегулированию спорных правовых ситуаций между Заказчиком и третьими лицами указанными в &laquo;Заявлении&raquo;; </span></p>
<p><span style="font-weight: 400;">- разработка различных правовых позиций, связанной с защитой прав Заказчика в рассматриваемой ситуации и оказание юридических услуг по реализации выбранного направления защиты; </span></p>
<p><span style="font-weight: 400;">- обращение в соответствующие государственные учреждения и организации с необходимыми заявлениями, документами и т.п.; </span></p>
<p><span style="font-weight: 400;">- подготовка и направление необходимых документов в соответствующий суд и оказание в связи с этим юридических услуг по представительству Заказчика при ведении дел в суде;</span></p>
<p><span style="font-weight: 400;"> - иные юридические услуги, необходимые для выполнения настоящего Договора. </span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<ul>
    <li><strong><strong>Права и обязанности сторон </strong></strong></li>
</ul>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><span style="font-weight: 400;">2.1. В рамках настоящего договора Исполнитель обязуется:</span></p>
<p>&nbsp;</p>
<p><span style="font-weight: 400;">- изучить имеющиеся у Заказчика документы, относящиеся к предмету услуги или поручения, дать письменное или устное предварительное заключение о судебной перспективе дела, в том числе о юридической обоснованности обжалования состоявшихся судебных решений; </span></p>
<p><span style="font-weight: 400;">- при содействии Заказчика провести работу по подбору документов и других материалов, обосновывающих заявленные требования;</span></p>
<p><span style="font-weight: 400;">- консультировать Заказчика по всем возникающим в связи с данным Договором правовыми вопросами.</span></p>
<p>&nbsp;</p>
<p><span style="font-weight: 400;">2.2. При оказании Заказчику юридических услуг, обусловленных настоящим Договором, Исполнитель руководствуется принципами максимального учета интересов Заказчика и осуществляет действия в соответствии с конкретными, осуществимыми и правомерными поручениями Заказчика.</span></p>
<p>&nbsp;</p>
<p><span style="font-weight: 400;">2.3. Исполнитель не отвечает перед Заказчиком за обычный риск убытков, связанных с судебными процессами. Любые заключения специалистов Исполнителя либо привлеченных Исполнителем лиц по поводу возможного исхода той или иной стадии судебного процесса в силу объективных причин являются лишь обоснованными предположениями и не могут быть использованы для каких-либо претензий к Исполнителю со стороны Заказчика. Упущенная выгода возмещению не подлежит.</span></p>
<p>&nbsp;</p>
<p><span style="font-weight: 400;">2.4. Заказчик обязуется предоставлять Исполнителю любую запрошенную им информацию и/или документы, которые относятся к конкретному поручению Заказчика и требуются Исполнителю для наиболее полного и быстрого его выполнения. Исполнитель не несет ответственности за последствия, связанные с представлением Заказчиком документов, не соответствующих действительности. При этом Исполнитель как в период действия настоящего Договора об оказании юридических услуг, так и после его прекращения, гарантирует Заказчику неразглашение сведений третьи лицам, ставших известными ему в результате оказания Заказчику юридических услуг в рамках настоящего Договора на оказание юридических услуг.</span></p>
<p><span style="font-weight: 400;">2.5 В течении действия данного договора стороны имеют право расторгнуть договор досрочно направив письменное заявление без объяснения причин расторжения данного договора. В случае расторжения договора по инициативе Заказчика оплаченные ранее денежные средства Исполнителем не возвращаются.</span></p>
<p>&nbsp;</p>
<ol start="3">
    <li><strong> Порядок сдачи приема услуг.</strong></li>
</ol>
<p>&nbsp;</p>
<p><span style="font-weight: 400;">3.1. Исполнитель обязуется выполнять принятые на себя обязательства в надлежащие сроки.</span></p>
<p>&nbsp;</p>
<p><span style="font-weight: 400;">3.2. Сдача-приемка оказанных услуг производится путем подписания обеими Сторонами Акта сдачи-приемки услуг, далее по тексту &laquo;Акт&raquo;.</span></p>
<p>&nbsp;</p>
<p><span style="font-weight: 400;">3.3. В случае не подписания Заказчиком Акта услуг в течение трех рабочих дней, с момента его получения, и не предоставления его Исполнителю, обязательства по Договору считаются выполненными полностью.</span></p>
<p>&nbsp;</p>
<p><span style="font-weight: 400;">3.4. В случае отказа Заказчика от подписания Акта, Заказчик в течении трех рабочих дней с момента получения Акта, передает Исполнителю перечень необходимых доработок и согласовывает с Исполнителем сроки их устранения. Претензии Заказчика должны быть обоснованными и содержать конкретные ссылки на несоответствие оказанных услуг результатам.</span></p>
<p>&nbsp;</p>
<p><span style="font-weight: 400;">3.5. После устранения замечаний указанных в перечне Исполнитель вновь передает Акт, в порядке предусмотренном п. 3.2. настоящего Договора.</span></p>
<p><span style="font-weight: 400;">3.6. Стороны пришли к соглашению, что Акты и другие документы Исполнитель может направлять в бумажном виде на юридический адрес или электронным письмом с приложением копий Актов и других документов на электронную почту (eskalat36@mail.ru) Заказчика указанные им в Договоре.</span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<ul>
    <li><strong><strong>Порядок расчетов </strong></strong></li>
</ul>
<p>&nbsp;</p>
<p>&nbsp;</p>
<ol>
    <li style="font-weight: 400;"><span style="font-weight: 400;">Стоимость &nbsp;услуг &nbsp;по &nbsp;Договору &nbsp;определяется &nbsp;согласно &nbsp;тарифам &nbsp;указанным &nbsp;в &nbsp;Приложении &nbsp;№ &nbsp;1, &nbsp;со дня подписания Договора в течении 12 месяцев. Дата подписания Договора является отчетной датой для оплаты в следующем месяце. </span></li>
</ol>
<p>&nbsp;</p>
<p><span style="font-weight: 400;">Стоимость по представлению интересов Заказчика в последующих судебных заседаниях и инстанциях определяется дополнительным соглашением. </span></p>
<p>&nbsp;</p>
<ol>
    <li style="font-weight: 400;"><span style="font-weight: 400;">Оплата осуществляется ежемесячно в виде предоплаты в течении трех рабочих дней с наступления отчетной даты в размере 100% от стоимости услуг, определенных настоящим Договором. </span></li>
</ol>
<p>&nbsp;</p>
<ol>
    <li style="font-weight: 400;"><span style="font-weight: 400;">Расходы по оплате почтовых расходов, государственных пошлин и сборов в стоимость услуг Исполнителя не входят и оплачиваются Заказчиком отдельно. </span></li>
</ol>
<p>&nbsp;</p>
<ol>
    <li style="font-weight: 400;"><span style="font-weight: 400;">Заказчик обязуется оплатить услуги Исполнителя в размере и порядке, предусмотренных настоящим Договором, в случае просрочки полной суммы предоплаты Заказчиком по Договору более чем за 1(один) месяц, Исполнитель имеет право в одностороннем порядке расторгнуть Договор. </span></li>
</ol>
<p>&nbsp;</p>
<p>&nbsp;</p>
<ul>
    <li><strong><strong>Прочие условия. </strong></strong></li>
</ul>
<p>&nbsp;</p>
<p><br /><br /></p>
<ol>
    <li style="font-weight: 400;"><span style="font-weight: 400;">Стороны освобождаются от ответственности за неисполнение или ненадлежащее исполнение обязательств по договору при возникновении непреодолимых препятствий, под которыми понимаются: стихийные бедствия, массовые беспорядки, запретительные действия властей и иные форс-мажорные обстоятельства. Сумма, уплаченная за выполненную на этот момент работу, возврату не подлежит. </span></li>
</ol>
<p>&nbsp;</p>
<ol>
    <li style="font-weight: 400;"><span style="font-weight: 400;">Стороны обязуются все возникающие разногласия решать путем переговоров, при не урегулировании сторонами возникших разногласий, спор разрешается в судебном порядке. </span></li>
</ol>
<p>&nbsp;</p>
<ol>
    <li style="font-weight: 400;"><span style="font-weight: 400;">Исполнитель вправе по своему усмотрению, в рамках дополнительного соглашения к Договору и по согласованию с Заказчиком, для выполнения своих обязательств перед Заказчиком привлекать к оказанию услуг по настоящему договору третьих лиц, в том числе адвокатов, аудиторов, консультантов и т.д. </span></li>
</ol>
<p>&nbsp;</p>
<ol>
    <li style="font-weight: 400;"><span style="font-weight: 400;">В случае изменения адреса, контактных телефонов или банковских реквизитов Стороны обязаны уведомить об этом друг друга в течение 3-х рабочих дней с момента изменений. </span></li>
</ol>
<p>&nbsp;</p>
<ol>
    <li style="font-weight: 400;"><span style="font-weight: 400;">Настоящий договор составлен в двух экземплярах, для каждой из Сторон, вступает в силу с момента его подписания и действует до полного выполнения Исполнителем согласованного объема услуг и полного завершения расчетов Заказчиком. </span></li>
    <li style="font-weight: 400;"><span style="font-weight: 400;">Срок действия данного Договора &ndash; 12 месяцев с момента подписания Сторонами, если Стороны, за один месяц до окончания срока, не направили друг другу уведомления о его расторжении, то действие Договора считается пролонгированным еще на шесть месяцев. </span></li>
</ol>
<p>&nbsp;</p>
<p>&nbsp;</p>
<ul>
    <li><strong><strong>Адреса и подписи сторон.</strong></strong></li>
</ul>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><span style="font-weight: 400;">Исполнитель:</span><span style="font-weight: 400;">&nbsp;&nbsp;&nbsp; </span><span style="font-weight: 400;">Заказчик:</span></p>
<table style="height: 255px;" width="809">
    <tbody>
    <tr>
        <td>
            <p><span style="font-weight: 400;">ИП Прокопенко Анастасия Николаевна</span></p>
            <p><span style="font-weight: 400;">ИНН 360408101210</span></p>
            <p><span style="font-weight: 400;">ОГРН 315366800050012</span></p>
            <p><span style="font-weight: 400;">Юр.адрес: Воронежская обл., г.Борисоглебск, ул.Устиновская д.36/1</span></p>
            <p><span style="font-weight: 400;">Фактический адрес:</span></p>
            <p><span style="font-weight: 400;">397160, Воронежская обл., г.Борисоглебск,</span></p>
            <p><span style="font-weight: 400;">Ул.Карла Маркса, д. 104, оф. 7</span></p>
            <p><span style="font-weight: 400;">Тел.: 8-952-106-36-37</span></p>
            <p><span style="font-weight: 400;">e-mail: finuslugi36@mail.ru</span></p>
        </td>
        <td>
            <p><strong>Иванов Иван Иванович, </strong></p>
            <p><strong>паспорт 1807 1023456 выдан Отделением УФМС России по Воронежской области в Грибановском районе, 12.03.2011г., 362-016, </strong></p>
            <p><strong>адрес регистрации: Воронежская область, г.Борисоглебск, ул.Советская д.17, кв.7,</strong><span style="font-weight: 400;"> &nbsp;</span></p>
            <p><strong>адрес фактического проживания: Саратовская обл., </strong><strong>г.Балашов, ул.К.Маркса, д.3</strong></p>
            <p><strong>тел. 89803403477 </strong></p>
        </td>
    </tr>
    </tbody>
</table>
<p>&nbsp;</p>
<p><span style="font-weight: 400;">ИП &nbsp;&nbsp;_________________________/Прокопенко А.Н/ &nbsp;&nbsp;&nbsp;&nbsp;__________________/_______________________/</span></p>
<p>&nbsp;</p>
<p><span style="font-weight: 400;"></span><span style="font-weight: 400;"><br /></span></p>
<p><span style="font-weight: 400;">Приложение № 1 к Договору на оказание юридических услуг с физическим лицом №</span><strong> ЦКР-0001/16 </strong></p>
<p><strong>от 01.03.2016г.</strong></p>
<p><span style="font-weight: 400;">от </span><strong>&laquo;01&raquo; марта 2016г.</strong></p>
<p>&nbsp;</p>
<p><span style="font-weight: 400;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></p>
<p><br /><br /><br /></p>
<p><strong>Тарифы на оказание юридических услуг</strong></p>
<p>&nbsp;</p>
<table>
    <tbody>
    <tr>
        <td>
            <p><span style="font-weight: 400;">№ п/п</span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">Тарифы</span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">Стоимость</span></p>
        </td>
    </tr>
    <tr>
        <td>
            <p><span style="font-weight: 400;">1</span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">Тариф &laquo;Эконом&raquo; при сумме долга до 150&nbsp;000 руб.</span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">4000 руб.</span></p>
        </td>
    </tr>
    <tr>
        <td>
            <p><span style="font-weight: 400;">2</span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">Тариф &laquo;Базовый&raquo; при сумме долга от 150&nbsp;000 руб. </span></p>
            <p><span style="font-weight: 400;">до 300&nbsp;000 руб.</span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">6700 руб.</span></p>
        </td>
    </tr>
    <tr>
        <td>
            <p><span style="font-weight: 400;">3</span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">Тариф &laquo;Премиум&raquo; при сумме долга от 300&nbsp;000 руб. </span></p>
            <p><span style="font-weight: 400;">до 800&nbsp;000 руб.</span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">7700 руб.</span></p>
        </td>
    </tr>
    <tr>
        <td>
            <p><span style="font-weight: 400;">4</span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">Тариф &laquo;Максимальный&raquo; при сумме долга от 800&nbsp;000 руб. </span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">10500 руб.</span></p>
        </td>
    </tr>
    <tr>
        <td>
            <p><span style="font-weight: 400;">5</span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">Тариф &laquo;Индивидуальный&raquo;</span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">20% от суммы иска</span></p>
        </td>
    </tr>
    </tbody>
</table>
<p><br /><br /></p>
<p><strong>Тарифы на оказание дополнительных юридических услуг</strong></p>
<p>&nbsp;</p>
<table>
    <tbody>
    <tr>
        <td>
            <p><span style="font-weight: 400;">№ п/п</span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">Перечень письменных работ</span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">Стоимость</span></p>
        </td>
    </tr>
    <tr>
        <td>
            <p><span style="font-weight: 400;">1</span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">Юридическая консультация</span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">350 руб.</span></p>
        </td>
    </tr>
    <tr>
        <td>
            <p><span style="font-weight: 400;">2</span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">Письменный комментарий документов</span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">1500 руб.</span></p>
        </td>
    </tr>
    <tr>
        <td>
            <p><span style="font-weight: 400;">3</span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">Отзыв согласия на обработку персональных данных</span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">500 руб.</span></p>
        </td>
    </tr>
    <tr>
        <td>
            <p><span style="font-weight: 400;">4</span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">Заявление (досудебное)</span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">1500 руб.</span></p>
        </td>
    </tr>
    <tr>
        <td>
            <p><span style="font-weight: 400;">5</span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">Жалоба (досудебная)</span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">1500 руб.</span></p>
        </td>
    </tr>
    <tr>
        <td>
            <p><span style="font-weight: 400;">6</span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">Претензия (досудебная)</span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">1500 руб.</span></p>
        </td>
    </tr>
    <tr>
        <td>
            <p><span style="font-weight: 400;">7</span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">Исковое заявление в суд (в т.ч. встречное)</span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">4500 руб.</span></p>
        </td>
    </tr>
    <tr>
        <td>
            <p><span style="font-weight: 400;">8</span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">Отзыв на исковое заявление</span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">3000 руб.</span></p>
        </td>
    </tr>
    <tr>
        <td>
            <p><span style="font-weight: 400;">9</span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">Возражение на исковые требования</span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">4500 руб.</span></p>
        </td>
    </tr>
    <tr>
        <td>
            <p><span style="font-weight: 400;">10</span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">Заявление на отмену судебного приказа</span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">1000 руб.</span></p>
        </td>
    </tr>
    <tr>
        <td>
            <p><span style="font-weight: 400;">11</span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">Заявление на отмену заочного решения суда</span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">2000 руб.</span></p>
        </td>
    </tr>
    <tr>
        <td>
            <p><span style="font-weight: 400;">12</span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">Жалоба на определение суда</span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">2000 руб.</span></p>
        </td>
    </tr>
    <tr>
        <td>
            <p><span style="font-weight: 400;">13</span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">Ходатайство об истребовании документов</span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">1000 руб.</span></p>
        </td>
    </tr>
    <tr>
        <td>
            <p><span style="font-weight: 400;">14</span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">Представительство в суде (за 1 день судебного заседания)</span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">5000 руб.</span></p>
        </td>
    </tr>
    <tr>
        <td>
            <p><span style="font-weight: 400;">15</span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">Запрос в Банк на предоставление документы (договор/выписка/разъяснение)</span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">500 руб.</span></p>
        </td>
    </tr>
    </tbody>
</table>
<p>&nbsp;</p>
<p><span style="font-weight: 400;">______________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________</span></p>
<p><br /><br /><br /><br /><br /><br /></p>
<p><span style="font-weight: 400;">Приложение № 2 к Договору на оказание юридических услуг с физическим лицом № </span><strong>ЦКР-0001/16 </strong></p>
<p><strong>от 01.03.2016г.</strong></p>
<p>&nbsp;</p>
<p><span style="font-weight: 400;">ИП Прокопенко Анастасия Николаевна</span></p>
<p><span style="font-weight: 400;">ИНН 360408101210</span></p>
<p><span style="font-weight: 400;">ОГРН 315366800050012</span></p>
<p><span style="font-weight: 400;">Фактический адрес:</span></p>
<p><span style="font-weight: 400;">397160, Воронежская обл., г.Борисоглебск,</span></p>
<p><span style="font-weight: 400;">Ул.Карла Маркса, д. 104, оф. 7</span></p>
<p><span style="font-weight: 400;">Тел.: 8-952-106-36-37</span></p>
<p>&nbsp;</p>
<table>
    <tbody>
    <tr>
        <td>
            <p><strong>Иванов Иван Иванович, </strong></p>
            <p><strong>паспорт 1807 1023456 выдан Отделением УФМС России по Воронежской области в Грибановском районе, 12.03.2011г., 362-016, </strong></p>
            <p><strong>адрес регистрации: Воронежская область, г.Борисоглебск, ул.Советская д.17, кв.7,</strong><span style="font-weight: 400;"> &nbsp;</span></p>
            <p><strong>адрес фактического проживания: Саратовская обл., </strong><strong>г.Балашов, ул.К.Маркса, д.3</strong></p>
            <p><strong>тел. 89803403477</strong></p>
        </td>
        <td>&nbsp;</td>
    </tr>
    </tbody>
</table>
<p><br /><br /><br /><br /></p>
<p><span style="font-weight: 400;">ЗАЯВЛЕНИЕ</span></p>
<p><br /><br /></p>
<p><span style="font-weight: 400;">Прошу Вас оказать мне юридические услуги по представлению моих интересов по кредитным договорам для снижения</span> <span style="font-weight: 400;">и/или &nbsp;реструктуризации задолженности, а также представления интересов в суде и ФССП.</span></p>
<p><span style="font-weight: 400;">Я, до подписания, ознакомившись с условиями данного Договора на оказание юридических услуг, прошу заключить со мной Договор на оказание юридических услуг.</span></p>
<p>&nbsp;</p>
<p><span style="font-weight: 400;">Я ознакомлен с условиями Договора и Тарифами, осознаю их и понимаю, согласен с ними и обязуюсь неукоснительно соблюдать, принимая на себя все права и обязанности Клиента, указанные в них. Я заявляю, что данный Договор и Тарифы, с которыми я предварительно ознакомился, являются неотъемлемой частью настоящего Заявления.</span></p>
<p>&nbsp;</p>
<p><span style="font-weight: 400;">Об ответственности за достоверность представленных сведений предупрежден(а).</span></p>
<p><span style="font-weight: 400;">В соответствии с Федеральным законом от 27.07.2006г. №152-ФЗ &laquo;О персональных данных&raquo;</span></p>
<p><span style="font-weight: 400;">Я выражаю свое &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;согласие &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;несогласие с обработкой (а именно: сбор, систематизация, накопление, обновление, изменение, использование, распространение, обмен, обезличивание, блокирование, уничтожение) моих персональных данных, указанных выше. </span></p>
<p><span style="font-weight: 400;">Отзыв моего согласия на обработку персональных данных производится путем направления соответствующего письменного заявления в Компанию по почте или иным способом, позволяющим подтвердить факт направления. </span></p>
<p><span style="font-weight: 400;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></p>
<p><span style="font-weight: 400;">Мой платеж, составит: _____________________(________________________________________________)руб.</span></p>
<p><span style="font-weight: 400;"> за 1 (один) календарный месяц.</span></p>
<p><br /><br /><br /></p>
<p><strong>________________/______________________________</strong></p>
<p>&nbsp;</p>
<p><strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(подпись) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong><strong>(расшифровка)</strong></p>
<p><br /><br /><br /><br /><br /><br /><br /></p>
<p><span style="font-weight: 400;">Приложение № 3 к Договору на оказание юридических услуг с физическим лицом № </span><strong>ЦКР-0001/16 </strong></p>
<p><strong>от 01.03.2016г.</strong></p>
<p><span style="font-weight: 400;">от &laquo;___&raquo; ___________ 20__ г</span></p>
<p><br /><br /><br /><br /><br /><br /><br /><br /></p>
<p><strong>Перечень передаваемых в работу кредитных договоров:</strong></p>
<p>&nbsp;</p>
<table>
    <tbody>
    <tr>
        <td>
            <p><span style="font-weight: 400;">№ п/п</span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">Номер кредитного договора, наименование банка</span></p>
        </td>
    </tr>
    <tr>
        <td>
            <p><span style="font-weight: 400;">1</span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">ХКФ № 12563985 от 01.02.2015</span></p>
        </td>
    </tr>
    <tr>
        <td>
            <p><span style="font-weight: 400;">2</span></p>
        </td>
        <td>
            <p><span style="font-weight: 400;">Сбербанк №12563 от 12.03.2010</span></p>
        </td>
    </tr>
    <tr>
        <td>
            <p><span style="font-weight: 400;">3</span></p>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <p><span style="font-weight: 400;">4</span></p>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <p><span style="font-weight: 400;">5</span></p>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    </tbody>
</table>
<p><br /><br /></p>
<p><span style="font-weight: 400;">Итого: &nbsp;</span><span style="font-weight: 400;">2 </span><span style="font-weight: 400;">&nbsp;&nbsp;&nbsp;кредитных договора на сумму до &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;руб. &nbsp;(с учетом скидки) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;руб. </span></p>
<p><strong>________________/______________________________</strong></p>
<p>&nbsp;</p>
<p><strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(подпись) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong><strong>(расшифровка)</strong></p>
<p>&nbsp;</p>
</body>
</html>
