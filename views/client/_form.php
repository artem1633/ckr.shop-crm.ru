<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Contract;
use Carbon\Carbon;
/* @var $this yii\web\View */
/* @var $model app\models\Client */
/* @var $form yii\widgets\ActiveForm */

$isAdmin = Yii::$app->user->identity->category_id === 1;
$isReadOnly =  Yii::$app->user->identity->category_id != 1; //&& $model->isNewRecord == false;
$createdDate = Carbon::now();
$createdDate = $createdDate->format('d-m-Y');

$contractMaxId = Contract::find() // AQ instance
->select('max(id)') // we need only one column
->scalar();

$zeros = '';
for ($i=0; $i<(3-count($contractMaxId)); $i++) {
	$zeros .= '0';
}
$contractNumber = 'ЦКР-' . $zeros . $contractMaxId . '/17';

?>

<div class="client-form">
	<div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Информация о клиенте</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
		<div class="box-body">
			<?php $form = ActiveForm::begin(); ?>

   		<div class="row">
			<div class="col-md-4 vcenter">
				    <?php
						if ($model->isNewRecord) {
							echo $form->field($contract, 'number')->textInput(['readonly' => $isReadOnly, 'value' => $contractNumber,]);
						}
						else {
							echo $form->field($contract, 'number')->textInput(['readonly' => $isReadOnly]);
						}
					?>

			</div>
<!-- 			<div class="col-md-4 vcenter">
				    <?= $form->field($contract, 'date_created')->widget(\yii\widgets\MaskedInput::className(), [
    					'mask' => '99.99.9999',
					], ['readonly' => $isReadOnly]) ?>
			</div> -->
			<div class="col-md-4 vcenter">
				    <?= $form->field($contract, 'price')->textInput() ?>				
			</div>
			<div class="col-md-4 vcenter">
				    <?= $form->field($contract, 'rate')->textInput() ?>				
			</div>
		</div>
   		<div class="row">
		</div>

		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'name')->textInput() ?>				
			</div>
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
    					'mask' => '+7(999) 999-99-99',
					]) ?>				
			</div>
			<div class="col-md-4 vcenter">
				<?= $form->field($contract, 'status')->dropDownList([
					'Активный' => 'Активный',
					'Завершенный' => 'Завершенный',
					'Архив' => 'Архив',
				])->label('Статус договора') ?>
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'date_birth')->widget(\yii\widgets\MaskedInput::className(), [
    					'mask' => '99.99.9999',
					]) ?>				
			</div>
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'alt_phone')->widget(\yii\widgets\MaskedInput::className(), [
    					'mask' => '+7(999) 999-99-99',
					]) ?>				
			</div>
			<div class="col-md-4 vcenter">
				<?= $form->field($model, 'office_id')->dropDownList($model->getOfficeList()) ?>		
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'place_birth')->textarea(['rows' => 6]) ?>
			</div>
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'passport')->textarea(['rows' => 6]) ?>				
			</div>
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'registration_address')->textarea(['rows' => 6]) ?>				
			</div>
		</div>
   		<div class="row">
			<div class="col-md-6 vcenter">
				    <?= $form->field($model, 'current_address')->textarea(['rows' => 6]) ?>				
			</div>
			<div class="col-md-6 vcenter">
				    <?= $form->field($model, 'WorkPlace')->textarea(['rows' => 6]) ?>				
			</div>
		</div>
            <div class="row">
                <div class="col-md-12 vcenter">
                    <?= $form->field($model, 'comments')->textarea(['rows' => 6]) ?>
                </div>
            </div>
            <div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($contract, 'date_created')->textInput(['readonly' => $isReadOnly, 'value' => $createdDate]) ?>
			</div>
		</div>


<div style="display:none">
</div>

	<?php if($model->isNewRecord): ?>
	    <div class="form-group">
	        <?= Html::submitButton('Создать', ['class' => 'btn btn-success']) ?>
	    </div>
	<?php endif; ?>

	<?php if($model->isNewRecord == false): ?>
	    <div class="form-group">
	        <?= Html::submitButton('Данные', ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Сформировать договор', ['create-agreement', 'id' => $model->id], ['class' => 'btn btn-success', 'target' => '_blank']) ?>
	    </div>
	<?php endif; ?>

    <?php ActiveForm::end(); ?>

		</div>
	</div>
</div>
