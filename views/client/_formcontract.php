<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Contract */
/* @var $form yii\widgets\ActiveForm */

if(isset($client_id) == false)
	$client_id = $model->client_id;

?>

<div class="contract-form">
	<div class="box box-default">
		<div class="box-body">
			<?php $form = ActiveForm::begin(); ?>

		<?= $form->field($model, 'client_id')->hiddenInput(['value' => $client_id])->label(false) ?>
   		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'number')->textInput() ?>
			</div>
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'date_created')->widget(\yii\widgets\MaskedInput::className(), [
    					'mask' => '9999-99-99',
					]) ?>
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'price')->textInput(['required' => 'true', 'type' => 'number']) ?>
			</div>
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'rate')->textInput(['required' => 'true', 'type' => 'number']) ?>
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'status')->dropDownList([
				    	'Активный' => 'Активный',
				    	'Завершенный' => 'Завершенный',
				    	'Архив' => 'Архив',
				    ]) ?>
			</div>
		</div>
   	
<div style="display:none">
</div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Сформировать договор' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

		</div>
	</div>
</div>
