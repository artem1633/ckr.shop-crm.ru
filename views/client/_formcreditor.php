<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Creditor */
/* @var $form yii\widgets\ActiveForm */

if(isset($client_id) == false)
	$client_id = $model->client_id;

?>

<div class="creditor-form">
	<div class="box box-default">
		<div class="box-body">
			<?php $form = ActiveForm::begin(); ?>

		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'contract_id')->hiddenInput(['readonly' => true, 'value' => $contract->id])->label(false) ?>
			</div>
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'client_id')->hiddenInput(['readonly' => true, 'value' => $client_id])->label(false) ?>
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'number')->textInput(['required' => 'true']) ?>
			</div>
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'name')->textInput() ?>				
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'requisites')->textInput() ?>				
			</div>
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'date_confirmed')->widget(\yii\widgets\MaskedInput::className(), [
    					'mask' => '99.99.9999',
					]) ?>				
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>				
			</div>
		</div>
   		<div class="row">
		</div>
   	
<div style="display:none">
</div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?php if($model->isNewRecord == false): ?>
			<a href="<?=Url::toRoute(['creditor/update', 'id' => $model->id])?>" class="btn btn-success">Подробнее</a>
        <?php endif; ?>
    </div>

    <?php ActiveForm::end(); ?>

		</div>
	</div>
</div>
