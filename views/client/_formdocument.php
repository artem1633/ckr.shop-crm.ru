<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
//use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Documents */
/* @var $form yii\widgets\ActiveForm */

if(isset($client_id) == false)
	$client_id = $model->client_id;

?>

<div class="documents-form">
	<div class="box box-default">
		<div class="box-body">
			<?php $form = ActiveForm::begin(); ?>

		<div class="row">

			<div class="col-md-4 vcenter">
				<?= $form->field($model, 'contract_id')->hiddenInput(['readonly' => true, 'value' => $contract->id])->label(false) ?>
			</div>
			<div class="col-md-4 vcenter">
				<?= $form->field($model, 'client_id')->hiddenInput(['readonly' => true, 'value' => $client_id])->label(false) ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'client_id')->hiddenInput(['value' => $client_id])->label(false) ?>
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'name')->textInput() ?>
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'date_created')->widget(DatePicker::classname(), [
						'language' => 'ru',

					]) ?>
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'file')->fileInput() ?>
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>
			</div>
		</div>
   	
<div style="display:none">
</div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

		</div>
	</div>
</div>
