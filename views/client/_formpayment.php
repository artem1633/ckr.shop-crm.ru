<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Payment */
/* @var $form yii\widgets\ActiveForm */


if(isset($client_id) == false)
	$client_id = $model->client_id;

$readonlyDateEdit = $model->isNewRecord == false && Yii::$app->user->identity->isAdmin() == false;

?>

<div class="payment-form">
	<div class="box box-default">
		<div class="box-body">
			<?php $form = ActiveForm::begin(); ?>

		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'client_id')->hiddenInput(['value' => $client_id])->label(false) ?>				
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 vcenter">
				<?php $model->nameList['Прочее'] = 'Прочее' ?>
				    <?= $form->field($model, 'name')->dropDownList($model->nameList) ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'payment_type_id')->dropDownList($paymentTypes)->label('способ оплаты') ?>
			</div>
		</div>

   		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'price')->textInput(['required' => 'true', 'type' => "number"]) ?>				
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'receipt')->textarea(['rows' => 6]) ?>				
			</div>
		</div>
   	
<div style="display:none">
</div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

		</div>
	</div>
</div>
