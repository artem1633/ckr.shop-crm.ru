<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ClientSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="client-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'date_birth') ?>

    <?= $form->field($model, 'place_birth') ?>

    <?= $form->field($model, 'passport') ?>

    <?php  echo $form->field($model, 'registration_address') ?>

    <?php  echo $form->field($model, 'current_address') ?>

    <?php  echo $form->field($model, 'phone') ?>

    <?php  echo $form->field($model, 'alt_phone') ?>

    <?php  echo $form->field($model, 'WorkPlace') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
