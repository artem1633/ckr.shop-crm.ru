<?php
/**
 * Created by PhpStorm.
 * User: Qist
 * Date: 17.04.2017
 * Time: 19:43
 */

$months = [
    '01' => 'Января',
    '02' => 'Февараля',
    '03' => 'Марта',
    '04' => 'Апреля',
    '05' => 'Мая',
    '06' => 'Июня',
    '07' => 'Июля',
    '08' => 'Августа',
    '09' => 'Сентября',
    '10' => 'Октября',
    '11' => 'Ноября',
    '12' => 'Декабря',
];


$current_date = "«".date('d')."» ".$months[date('m')]." ".date('Y')."г.";

?>

<html>

<head>
    <meta http-equiv=Content-Type content="text/html; charset=windows-1251">
    <meta name=Generator content="Microsoft Word 12 (filtered)">
    <style>
        <!--
        /* Font Definitions */
        @font-face
        {font-family:"Cambria Math";
            panose-1:2 4 5 3 5 4 6 3 2 4;}
        @font-face
        {font-family:Calibri;
            panose-1:2 15 5 2 2 2 4 3 2 4;}
        @font-face
        {font-family:Tahoma;
            panose-1:2 11 6 4 3 5 4 4 2 4;}
        /* Style Definitions */
        p.MsoNormal, li.MsoNormal, div.MsoNormal
        {margin-top:0cm;
            margin-right:0cm;
            margin-bottom:10.0pt;
            margin-left:0cm;
            line-height:115%;
            font-size:11.0pt;
            font-family:"Calibri","sans-serif";}
        p.MsoHeader, li.MsoHeader, div.MsoHeader
        {mso-style-link:"Верхний колонтитул Знак";
            margin:0cm;
            margin-bottom:.0001pt;
            font-size:11.0pt;
            font-family:"Calibri","sans-serif";}
        p.MsoFooter, li.MsoFooter, div.MsoFooter
        {mso-style-link:"Нижний колонтитул Знак";
            margin:0cm;
            margin-bottom:.0001pt;
            font-size:11.0pt;
            font-family:"Calibri","sans-serif";}
        p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
        {mso-style-link:"Текст выноски Знак";
            margin:0cm;
            margin-bottom:.0001pt;
            font-size:6.0pt;
            font-family:"Tahoma","sans-serif";}
        p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
        {margin-top:0cm;
            margin-right:0cm;
            margin-bottom:10.0pt;
            margin-left:36.0pt;
            line-height:115%;
            font-size:11.0pt;
            font-family:"Calibri","sans-serif";}
        p.MsoListParagraphCxSpFirst, li.MsoListParagraphCxSpFirst, div.MsoListParagraphCxSpFirst
        {margin-top:0cm;
            margin-right:0cm;
            margin-bottom:0cm;
            margin-left:36.0pt;
            margin-bottom:.0001pt;
            line-height:115%;
            font-size:11.0pt;
            font-family:"Calibri","sans-serif";}
        p.MsoListParagraphCxSpMiddle, li.MsoListParagraphCxSpMiddle, div.MsoListParagraphCxSpMiddle
        {margin-top:0cm;
            margin-right:0cm;
            margin-bottom:0cm;
            margin-left:36.0pt;
            margin-bottom:.0001pt;
            line-height:115%;
            font-size:11.0pt;
            font-family:"Calibri","sans-serif";}
        p.MsoListParagraphCxSpLast, li.MsoListParagraphCxSpLast, div.MsoListParagraphCxSpLast
        {margin-top:0cm;
            margin-right:0cm;
            margin-bottom:10.0pt;
            margin-left:36.0pt;
            line-height:115%;
            font-size:11.0pt;
            font-family:"Calibri","sans-serif";}
        span.a
        {mso-style-name:"Текст выноски Знак";
            mso-style-link:"Текст выноски";
            font-family:"Tahoma","sans-serif";}
        span.a0
        {mso-style-name:"Верхний колонтитул Знак";
            mso-style-link:"Верхний колонтитул";}
        span.a1
        {mso-style-name:"Нижний колонтитул Знак";
            mso-style-link:"Нижний колонтитул";}
        .MsoPapDefault
        {margin-bottom:10.0pt;
            line-height:115%;}
        /* Page Definitions */
        @page Section1
        {size:595.2pt 841.8pt;
            margin:27.6pt 28.0pt 1.0cm 28.0pt;}
        div.Section1
        {page:Section1;}
        @page Section2
        {size:595.2pt 841.8pt;
            margin:25.35pt 28.0pt 72.0pt 28.0pt;}
        div.Section2
        {page:Section2;}
        /* List Definitions */
        ol
        {margin-bottom:0cm;}
        ul
        {margin-bottom:0cm;}
        -->
    </style>

</head>

<body lang=RU>

<div class=Section1>

    <p class=MsoNormal style='margin-top:0cm;margin-right:153.0pt;margin-bottom:
0cm;margin-left:153.0pt;margin-bottom:.0001pt;text-indent:26.05pt;line-height:
87%;punctuation-wrap:simple;text-autospace:none'><span style='position:relative;
z-index:2'><span style='left:0px;position:absolute;left:-16px;top:-17px;
width:715px;height:152px'>

<table cellpadding=0 cellspacing=0 align=left>
 <tr>
  <td width=0 height=0></td>
  <td width=360></td>
  <td width=46></td>
  <td width=309></td>
 </tr>
 <tr>
  <td height=10></td>
  <td rowspan=2 align=left valign=top><img width=360 height=108
                                           src="/img/agreement-logo.png"></td>
 </tr>
 <tr>
  <td height=98></td>
  <td></td>
  <!-- <td rowspan=2 align=left valign=top><img width=309 height=142
                                           src="/img/border.png"></td> -->
 </tr>
 <tr>
  <td height=44></td>
 </tr>
</table>


    <br clear=ALL>

    <p class=MsoNormal style='margin-top:0cm;margin-right:153.0pt;margin-bottom:
0cm;margin-left:153.0pt;margin-bottom:.0001pt;text-indent:26.05pt;line-height:
87%;punctuation-wrap:simple;text-autospace:none'><b>Договор на
оказание юридических услуг с физическим лицом № <?=$contract->number?></span></b></p>

    <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:25.0pt;margin-bottom:.0001pt;line-height:99%;text-autospace:none'><span
            style='font-size:6.0pt;line-height:99%;font-family:"Times New Roman","serif"'>г.Борисоглебск</span><span
            style='font-size:12.0pt;line-height:99%;font-family:"Times New Roman","serif"'>                                                                                                                           
</span><b><?=$current_date?></b></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
14.0pt;text-autospace:none'><span style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:35.4pt;line-height:89%;
punctuation-wrap:simple;text-autospace:none'><span style='font-size:6.0pt;
line-height:89%;font-family:"Times New Roman","serif"'>Индивидуальный
предприниматель Прокопенко Анастасия Николаевна, ИНН 360408101210, ОГРН
315366800050012, именуемое далее &quot;Исполнитель&quot;, с одной стороны и <b><?= /** @var \app\models\client $client */
                    $client->name?>, <?=$client->passport?>, адрес регистрации: <?=$client->registration_address?>,</span></b><span style='color:red'>  </span><span style="font-size: 5.0pt;">действующий
(ая) от своего имени, именуемый (ая) далее &quot;Заказчик&quot;, с другой
стороны, вместе именуемые «Стороны», а по отдельности «Сторона», заключили
настоящий договор о следующем:</span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:35.4pt;line-height:89%;
punctuation-wrap:simple;text-autospace:none'><span style='font-size:12.0pt;
line-height:89%;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

    <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:234.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-10.45pt;line-height:99%;punctuation-wrap:simple;text-autospace:
none'><b><span lang=EN-US style='font-size:6.0pt;line-height:99%;font-family:
"Times New Roman","serif"'>1.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></b><b><span
                lang=EN-US style='font-size:6.0pt;line-height:99%;font-family:"Times New Roman","serif"'>Предмет
договора </span></b></p>

    <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:234.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
line-height:99%;punctuation-wrap:simple;text-autospace:none'><b><span
                lang=EN-US style='font-size:6.0pt;line-height:99%;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
2.25pt;text-autospace:none'><b><span lang=EN-US style='font-size:6.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>

    <p class=MsoNormal style='margin:0cm;margin-bottom:.0001pt;text-align:justify;
text-justify:inter-ideograph;text-indent:.3pt;line-height:94%;punctuation-wrap:
simple;text-autospace:none'><span style='font-size:6.0pt;line-height:94%;
font-family:"Times New Roman","serif"'>1.1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span><span style='font-size:6.0pt;line-height:94%;font-family:"Times New Roman","serif"'>Исполнитель
обязуется по поручению Заказчика оказать ему юридические услуги в объеме и
порядке, оговоренном настоящим Договором на оказание юридических услуг с
физическим лицом, далее по тексту «Договор», а Заказчик обязуется обеспечить
Исполнителя документами, сведениями и средствами, необходимыми для оказания
юридических услуг и выплатить ему вознаграждение на условиях и в порядке,
предусмотренных настоящим Договором. </span></p>

    <p class=MsoNormal style='margin:0cm;margin-bottom:.0001pt;text-align:justify;
text-justify:inter-ideograph;text-indent:.3pt;line-height:94%;punctuation-wrap:
simple;text-autospace:none'><span style='font-size:6.0pt;line-height:94%;
font-family:"Times New Roman","serif"'>1.2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span><span style='font-size:6.0pt;line-height:94%;font-family:"Times New Roman","serif"'>Заказчик
обязуется оформить и предоставить нотариально заверенную доверенность выданную
на компанию </span><span lang=EN-US style='font-size:6.0pt;line-height:94%;
font-family:"Times New Roman","serif"'>Исполнителя, на право представлять
интересы Заказчика</span><span style='font-size:6.0pt;line-height:94%;
font-family:"Times New Roman","serif"'>, с правом передоверия</span><span
            lang=EN-US style='font-size:6.0pt;line-height:94%;font-family:"Times New Roman","serif"'>.
</span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
2.5pt;text-autospace:none'><span lang=EN-US style='font-size:6.0pt;font-family:
"Times New Roman","serif"'>&nbsp;</span></p>

    <p class=MsoNormal style='margin:0cm;margin-bottom:.0001pt;text-align:justify;
text-justify:inter-ideograph;text-indent:.3pt;line-height:89%;punctuation-wrap:
simple;text-autospace:none'><span lang=EN-US style='font-size:6.0pt;
line-height:89%;font-family:"Times New Roman","serif"'>1.3.<span
                style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp; </span></span><span
            style='font-size:6.0pt;line-height:89%;font-family:"Times New Roman","serif"'>Поручение
к Исполнителю Заказчик оформляет «Заявлением» (Приложение № 2), которое
является неотъемлемой частью </span><span lang=EN-US style='font-size:6.0pt;
line-height:89%;font-family:"Times New Roman","serif"'>Договора, а также
определяет сущность услуги и поручения. </span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:2.4pt;text-autospace:none'><span
            lang=EN-US style='font-size:6.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

    <p class=MsoNormal style='margin:0cm;margin-bottom:.0001pt;text-align:justify;
text-justify:inter-ideograph;text-indent:.3pt;line-height:89%;punctuation-wrap:
simple;text-autospace:none'><span style='font-size:6.0pt;line-height:89%;
font-family:"Times New Roman","serif"'>1.4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:6.0pt;line-height:89%;font-family:"Times New Roman","serif"'>Объем
юридических услуг оказываемых Исполнителем в рамках настоящего Договора
определяется тарифами и услугами изложенными в Приложении № 1 Договора и
являющейся его неотъемлемой частью, а также включает в себя: </span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:2.5pt;text-autospace:none'><span
            style='font-size:6.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:normal;punctuation-wrap:simple;
text-autospace:none'><span style='font-size:6.0pt;font-family:"Times New Roman","serif"'>-
юридические консультации по вопросам действующего российского законодательства,
в том числе подбор и предоставление необходимых для исполнения поручения
Заказчика нормативных актов;</span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:normal;punctuation-wrap:simple;
text-autospace:none'><span style='font-size:6.0pt;font-family:"Times New Roman","serif"'>-
выбор наиболее оптимального плана по урегулированию спорных правовых ситуаций
между Заказчиком и третьими лицами указанными в «Заявлении»; </span></p>

    <p class=MsoNormal style='margin-top:0cm;margin-right:1.0pt;margin-bottom:0cm;
margin-left:0cm;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
line-height:normal;punctuation-wrap:simple;text-autospace:none'><span
            style='font-size:6.0pt;font-family:"Times New Roman","serif"'>- разработка
различных правовых позиций, связанной с защитой прав Заказчика в
рассматриваемой ситуации и оказание юридических услуг по реализации выбранного
направления защиты; </span></p>

    <p class=MsoNormal style='margin-top:0cm;margin-right:1.0pt;margin-bottom:0cm;
margin-left:0cm;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
line-height:normal;punctuation-wrap:simple;text-autospace:none'><span
            style='font-size:6.0pt;font-family:"Times New Roman","serif"'>- обращение в
соответствующие государственные учреждения и организации с необходимыми
заявлениями, документами и т.п.; </span></p>

    <p class=MsoNormal style='margin-top:0cm;margin-right:1.0pt;margin-bottom:0cm;
margin-left:0cm;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
line-height:92%;punctuation-wrap:simple;text-autospace:none'><span
            style='font-size:6.0pt;line-height:92%;font-family:"Times New Roman","serif"'>-
подготовка и направление необходимых документов в соответствующий суд и
оказание в связи с этим юридических услуг по представительству Заказчика при
ведении дел в суде;</span></p>

    <p class=MsoNormal style='margin-top:0cm;margin-right:1.0pt;margin-bottom:0cm;
margin-left:0cm;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
line-height:92%;punctuation-wrap:simple;text-autospace:none'><span
            style='font-size:6.0pt;line-height:92%;font-family:"Times New Roman","serif"'> -
иные юридические услуги, необходимые для выполнения настоящего Договора. </span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
11.85pt;text-autospace:none'><span style='font-size:6.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

    <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:210.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-9.7pt;line-height:99%;punctuation-wrap:simple;text-autospace:none'><b><span
                lang=EN-US style='font-size:6.0pt;line-height:99%;font-family:"Times New Roman","serif"'>2.<span
                    style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></b><b><span
                lang=EN-US style='font-size:6.0pt;line-height:99%;font-family:"Times New Roman","serif"'>Права
и обязанности сторон </span></b></p>

    <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:210.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
line-height:99%;punctuation-wrap:simple;text-autospace:none'><b><span
                lang=EN-US style='font-size:6.0pt;line-height:99%;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:98%;text-autospace:none'><span
            style='font-size:6.0pt;line-height:98%;font-family:"Times New Roman","serif"'>2.1.
В рамках настоящего договора Исполнитель обязуется:</span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:2.5pt;text-autospace:none'><span
            style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:normal;punctuation-wrap:simple;
text-autospace:none'><span style='font-size:6.0pt;font-family:"Times New Roman","serif"'>-
изучить имеющиеся у Заказчика документы, относящиеся к предмету услуги или
поручения, дать письменное или устное предварительное заключение о судебной
перспективе дела, в том числе о юридической обоснованности обжалования
состоявшихся судебных решений; </span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:normal;punctuation-wrap:simple;
text-autospace:none'><span style='font-size:6.0pt;font-family:"Times New Roman","serif"'>-
при содействии Заказчика провести работу по подбору документов и других
материалов, обосновывающих заявленные требования;</span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:99%;text-autospace:none'><span
            style='font-size:6.0pt;line-height:99%;font-family:"Times New Roman","serif"'>-
консультировать Заказчика по всем возникающим в связи с данным Договором
правовыми вопросами.</span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:2.5pt;text-autospace:none'><span
            style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:92%;punctuation-wrap:simple;
text-autospace:none'><span style='font-size:6.0pt;line-height:92%;font-family:
"Times New Roman","serif"'>2.2. При оказании Заказчику юридических услуг,
обусловленных настоящим Договором, Исполнитель руководствуется принципами
максимального учета интересов Заказчика и осуществляет действия в соответствии
с конкретными, осуществимыми и правомерными поручениями Заказчика.</span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:2.55pt;text-autospace:none'><span
            style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:95%;punctuation-wrap:simple;
text-autospace:none'><span style='font-size:6.0pt;line-height:95%;font-family:
"Times New Roman","serif"'>2.3. Исполнитель не отвечает перед Заказчиком за
обычный риск убытков, связанных с судебными процессами. Любые заключения
специалистов Исполнителя либо привлеченных Исполнителем лиц по поводу
возможного исхода той или иной стадии судебного процесса в силу объективных причин
являются лишь обоснованными предположениями и не могут быть использованы для
каких-либо претензий к Исполнителю со стороны Заказчика. Упущенная выгода
возмещению не подлежит.</span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:2.65pt;text-autospace:none'><span
            style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:96%;punctuation-wrap:simple;
text-autospace:none'><span style='font-size:6.0pt;line-height:96%;font-family:
"Times New Roman","serif"'>2.4. Заказчик обязуется предоставлять Исполнителю
любую запрошенную им информацию и/или документы, которые относятся к
конкретному поручению Заказчика и требуются Исполнителю для наиболее полного и
быстрого его выполнения. Исполнитель не несет ответственности за последствия,
связанные с представлением Заказчиком документов, не соответствующих
действительности. При этом Исполнитель как в период действия настоящего
Договора об оказании юридических услуг, так и после его прекращения,
гарантирует Заказчику неразглашение сведений третьи лицам, ставших известными
ему в результате оказания Заказчику юридических услуг в рамках настоящего
Договора на оказание юридических услуг.</span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:96%;punctuation-wrap:simple;
text-autospace:none'><span style='font-size:6.0pt;line-height:96%;font-family:
"Times New Roman","serif"'>2.5 В течении действия данного договора стороны
имеют право расторгнуть договор досрочно направив письменное заявление без
объяснения причин расторжения данного договора. В случае расторжения договора
по инициативе Заказчика оплаченные ранее денежные средства Исполнителем не
возвращаются.</span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
12.05pt;text-autospace:none'><span style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

    <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:198.0pt;margin-bottom:.0001pt;line-height:99%;text-autospace:none'><b><span
                style='font-size:6.0pt;line-height:99%;font-family:"Times New Roman","serif"'>3.
Порядок сдачи приема услуг.</span></b></p>

    <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:198.0pt;margin-bottom:.0001pt;line-height:99%;text-autospace:none'><span
            style='font-size:12.0pt;line-height:99%;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
98%;text-autospace:none'><span style='font-size:6.0pt;line-height:98%;
font-family:"Times New Roman","serif"'>3.1. Исполнитель обязуется выполнять
принятые на себя обязательства в надлежащие сроки.</span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
2.35pt;text-autospace:none'><span style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:89%;punctuation-wrap:simple;
text-autospace:none'><span style='font-size:6.0pt;line-height:89%;font-family:
"Times New Roman","serif"'>3.2. Сдача-приемка оказанных услуг производится путем
подписания обеими Сторонами Акта сдачи-приемки услуг, далее по тексту «Акт».</span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
2.55pt;text-autospace:none'><span style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:89%;punctuation-wrap:simple;
text-autospace:none'><span style='font-size:6.0pt;line-height:89%;font-family:
"Times New Roman","serif"'>3.3. В случае не подписания Заказчиком Акта услуг в
течение трех рабочих дней, с момента его получения, и не предоставления его Исполнителю,
обязательства по Договору считаются выполненными полностью.</span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
2.55pt;text-autospace:none'><span style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:92%;punctuation-wrap:simple;
text-autospace:none'><span style='font-size:6.0pt;line-height:92%;font-family:
"Times New Roman","serif"'>3.4. В случае отказа Заказчика от подписания Акта,
Заказчик в течении трех рабочих дней с момента получения Акта, передает
Исполнителю перечень необходимых доработок и согласовывает с Исполнителем сроки
их устранения. Претензии Заказчика должны быть обоснованными и содержать
конкретные ссылки на несоответствие оказанных услуг результатам.</span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
2.55pt;text-autospace:none'><span style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:89%;punctuation-wrap:simple;
text-autospace:none'><span style='font-size:6.0pt;line-height:89%;font-family:
"Times New Roman","serif"'>3.5. После устранения замечаний указанных в перечне
Исполнитель вновь передает Акт, в порядке предусмотренном п. 3.2. настоящего
Договора.</span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:92%;punctuation-wrap:simple;
text-autospace:none'><a name=page3></a><span style='font-size:6.0pt;
line-height:92%;font-family:"Times New Roman","serif"'>3.6. Стороны пришли к
соглашению, что Акты и другие документы Исполнитель может направлять в бумажном
виде на юридический адрес или электронным письмом с приложением копий Актов и
других документов на электронную почту (</span><span lang=EN-US
                                                     style='font-size:6.0pt;line-height:92%;font-family:"Times New Roman","serif"'>eskalat</span><span
            style='font-size:6.0pt;line-height:92%;font-family:"Times New Roman","serif"'>36@</span><span
            lang=EN-US style='font-size:6.0pt;line-height:92%;font-family:"Times New Roman","serif"'>mail</span><span
            style='font-size:6.0pt;line-height:92%;font-family:"Times New Roman","serif"'>.</span><span
            lang=EN-US style='font-size:6.0pt;line-height:92%;font-family:"Times New Roman","serif"'>ru</span><span
            style='font-size:6.0pt;line-height:92%;font-family:"Times New Roman","serif"'>)
Заказчика указанные им в Договоре.</span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
11.85pt;text-autospace:none'><span style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

    <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:233.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-9.65pt;line-height:99%;punctuation-wrap:simple;text-autospace:
none'><b><span lang=EN-US style='font-size:6.0pt;line-height:99%;font-family:
"Times New Roman","serif"'>4.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></b><b><span
                lang=EN-US style='font-size:6.0pt;line-height:99%;font-family:"Times New Roman","serif"'>Порядок
расчетов </span></b></p>

    <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:233.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
line-height:99%;punctuation-wrap:simple;text-autospace:none'><b><span
                lang=EN-US style='font-size:6.0pt;line-height:99%;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>

    <p class=MsoNormal style='margin:0cm;margin-bottom:.0001pt;text-align:justify;
text-justify:inter-ideograph;text-indent:0cm;line-height:89%;punctuation-wrap:
simple;text-autospace:none'><span style='font-size:6.0pt;line-height:89%;
font-family:"Times New Roman","serif"'>4.1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:6.0pt;line-height:89%;font-family:"Times New Roman","serif"'>Стоимость 
услуг  по  Договору  определяется  согласно  тарифам  указанным  в  Приложении 
№  1,  со дня подписания Договора в течении 12 месяцев. Дата подписания
Договора является отчетной датой для оплаты в следующем месяце. </span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
2.5pt;text-autospace:none'><span style='font-size:6.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

    <p class=MsoNormal style='margin-top:0cm;margin-right:1.0pt;margin-bottom:0cm;
margin-left:0cm;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
line-height:89%;punctuation-wrap:simple;text-autospace:none'><span
            style='font-size:6.0pt;line-height:89%;font-family:"Times New Roman","serif"'>Стоимость
по представлению интересов Заказчика в последующих судебных заседаниях и
инстанциях определяется дополнительным соглашением. </span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
2.5pt;text-autospace:none'><span style='font-size:6.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

    <p class=MsoNormal style='margin:0cm;margin-bottom:.0001pt;text-align:justify;
text-justify:inter-ideograph;text-indent:.3pt;line-height:88%;punctuation-wrap:
simple;text-autospace:none'><span style='font-size:6.0pt;line-height:88%;
font-family:"Times New Roman","serif"'>4.2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:6.0pt;line-height:88%;font-family:"Times New Roman","serif"'>Оплата
осуществляется ежемесячно в виде предоплаты в течении трех рабочих дней с
наступления отчетной даты в размере 100% от стоимости услуг, определенных
настоящим Договором. </span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
2.5pt;text-autospace:none'><span style='font-size:6.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

    <p class=MsoNormal style='margin-top:0cm;margin-right:1.0pt;margin-bottom:0cm;
margin-left:0cm;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:.3pt;line-height:89%;punctuation-wrap:simple;text-autospace:none'><span
            style='font-size:6.0pt;line-height:89%;font-family:"Times New Roman","serif"'>4.3.<span
                style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span
            style='font-size:6.0pt;line-height:89%;font-family:"Times New Roman","serif"'>Расходы
по оплате почтовых расходов, государственных пошлин и сборов в стоимость услуг
Исполнителя не входят и оплачиваются Заказчиком отдельно. </span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
2.5pt;text-autospace:none'><span style='font-size:6.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

    <p class=MsoNormal style='margin:0cm;margin-bottom:.0001pt;text-align:justify;
text-justify:inter-ideograph;text-indent:.3pt;line-height:97%;punctuation-wrap:
simple;text-autospace:none'><span style='font-size:5.0pt;line-height:97%;
font-family:"Times New Roman","serif"'>4.4.</span><span style='font-size:9.5pt;
line-height:97%;font-family:"Times New Roman","serif"; font-size: 4.4pt;'>Заказчик обязуется
оплатить услуги Исполнителя в размере и порядке, предусмотренных настоящим
Договором, в случае просрочки полной суммы предоплаты Заказчиком по Договору
более чем за 1(один) месяц, Исполнитель имеет право в одностороннем порядке
расторгнуть Договор. </span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
11.65pt;text-autospace:none'><span style='font-size:9.5pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

    <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:237.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:-10.3pt;line-height:99%;punctuation-wrap:simple;text-autospace:
none'><b><span lang=EN-US style='font-size:6.0pt;line-height:99%;font-family:
"Times New Roman","serif"'>5.<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span></b><b><span
                lang=EN-US style='font-size:6.0pt;line-height:99%;font-family:"Times New Roman","serif"'>Прочие
условия. </span></b></p>

    <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:237.0pt;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
line-height:99%;punctuation-wrap:simple;text-autospace:none'><b><span
                lang=EN-US style='font-size:6.0pt;line-height:99%;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
2.25pt;text-autospace:none'><span lang=EN-US style='font-size:12.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

    <p class=MsoNormal style='margin:0cm;margin-bottom:.0001pt;text-align:justify;
text-justify:inter-ideograph;text-indent:.3pt;line-height:94%;punctuation-wrap:
simple;text-autospace:none'><span style='font-size:6.0pt;line-height:94%;
font-family:"Times New Roman","serif"'>5.1.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span style='font-size:6.0pt;line-height:94%;font-family:"Times New Roman","serif"'>Стороны
освобождаются от ответственности за неисполнение или ненадлежащее исполнение
обязательств по договору при возникновении непреодолимых препятствий, под
которыми понимаются: стихийные бедствия, массовые беспорядки, запретительные
действия властей и иные форс-мажорные обстоятельства. Сумма, уплаченная за
выполненную на этот момент работу, возврату не подлежит. </span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
2.6pt;text-autospace:none'><span style='font-size:6.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

    <p class=MsoNormal style='margin:0cm;margin-bottom:.0001pt;text-align:justify;
text-justify:inter-ideograph;text-indent:.3pt;line-height:89%;punctuation-wrap:
simple;text-autospace:none'><span style='font-size:6.0pt;line-height:89%;
font-family:"Times New Roman","serif"'>5.2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:6.0pt;line-height:89%;font-family:"Times New Roman","serif"'>Стороны
обязуются все возникающие разногласия решать путем переговоров, при не
урегулировании сторонами возникших разногласий, спор разрешается в судебном
порядке. </span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
2.5pt;text-autospace:none'><span style='font-size:6.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

    <p class=MsoNormal style='margin:0cm;margin-bottom:.0001pt;text-align:justify;
text-justify:inter-ideograph;text-indent:.3pt;line-height:92%;punctuation-wrap:
simple;text-autospace:none'><span style='font-size:6.0pt;line-height:92%;
font-family:"Times New Roman","serif"'>5.3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span><span style='font-size:6.0pt;line-height:92%;font-family:"Times New Roman","serif"'>Исполнитель
вправе по своему усмотрению, в рамках дополнительного соглашения к Договору и по
согласованию с Заказчиком, для выполнения своих обязательств перед Заказчиком
привлекать к оказанию услуг по настоящему договору третьих лиц, в том числе
адвокатов, аудиторов, консультантов и т.д. </span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
2.35pt;text-autospace:none'><span style='font-size:6.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

    <p class=MsoNormal style='margin-top:0cm;margin-right:1.0pt;margin-bottom:0cm;
margin-left:0cm;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:.3pt;line-height:89%;punctuation-wrap:simple;text-autospace:none'><span
            style='font-size:6.0pt;line-height:89%;font-family:"Times New Roman","serif"'>5.4.<span
                style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><span
            style='font-size:6.0pt;line-height:89%;font-family:"Times New Roman","serif"'>В
случае изменения адреса, контактных телефонов или банковских реквизитов Стороны
обязаны уведомить об этом друг друга в течение 3-х рабочих дней с момента
изменений. </span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
2.5pt;text-autospace:none'><span style='font-size:6.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

    <p class=MsoNormal style='margin:0cm;margin-bottom:.0001pt;text-align:justify;
text-justify:inter-ideograph;text-indent:.3pt;line-height:89%;punctuation-wrap:
simple;text-autospace:none'><span style='font-size:6.0pt;line-height:89%;
font-family:"Times New Roman","serif"'>5.5.<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><span style='font-size:6.0pt;line-height:89%;font-family:"Times New Roman","serif"'>Настоящий
договор составлен в двух экземплярах, для каждой из Сторон, вступает в силу с
момента его подписания и действует до полного выполнения Исполнителем
согласованного объема услуг и полного завершения расчетов Заказчиком. </span></p>

    <p class=MsoNormal style='margin:0cm;margin-bottom:.0001pt;text-align:justify;
text-justify:inter-ideograph;text-indent:.3pt;line-height:92%;punctuation-wrap:
simple;text-autospace:none'><span style='font-size:6.0pt;line-height:92%;
font-family:"Times New Roman","serif"'>5.6.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;
</span></span><span style='font-size:6.0pt;line-height:92%;font-family:"Times New Roman","serif"'>Срок
действия данного Договора – 12 месяцев с момента подписания Сторонами, если
Стороны, за один месяц до окончания срока, не направили друг другу уведомления
о его расторжении, то действие Договора считается пролонгированным еще на шесть
месяцев. </span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:92%;punctuation-wrap:simple;
text-autospace:none'><span style='font-size:6.0pt;line-height:92%;font-family:
"Times New Roman","serif"'>&nbsp;</span></p>

    <p class=MsoListParagraphCxSpFirst align=center style='margin-top:0cm;
margin-right:0cm;margin-bottom:0cm;margin-left:108.0pt;margin-bottom:.0001pt;
text-align:center;text-indent:-18.0pt;line-height:99%;text-autospace:none'><b><span
                style='font-size:6.0pt;line-height:99%;font-family:"Times New Roman","serif"'>6.<span
                    style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></b><b><span
                style='font-size:6.0pt;line-height:99%;font-family:"Times New Roman","serif"'>Адреса
и подписи сторон.</span></b></p>

    <p class=MsoListParagraphCxSpLast style='margin-top:0cm;margin-right:0cm;
margin-bottom:0cm;margin-left:108.0pt;margin-bottom:.0001pt;line-height:99%;
text-autospace:none'><span style='font-size:12.0pt;line-height:99%;font-family:
"Times New Roman","serif"'>&nbsp;</span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal;text-autospace:none'><span style='font-size:6.0pt;font-family:"Times New Roman","serif"'>Исполнитель:</span><span
            style='font-size:12.0pt;font-family:"Times New Roman","serif"'>                                                                        </span><span
            style='font-size:9.5pt;font-family:"Times New Roman","serif"'>Заказчик:</span></p>

    <table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0
           style='border-collapse:collapse;border:none'>
        <tr>
            <td width=367 valign=top >
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:6.0pt;font-family:"Times New Roman","serif"'>ИП
  Прокопенко Анастасия Николаевна</span></p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:6.0pt;font-family:"Times New Roman","serif"'>ИНН
  360408101210</span></p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:6.0pt;font-family:"Times New Roman","serif"'>ОГРН
  315366800050012</span></p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:6.0pt;font-family:"Times New Roman","serif"'>Юр.адрес:
  Воронежская обл., г.Борисоглебск, ул.Устиновская д.36/1</span></p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:6.0pt;font-family:"Times New Roman","serif"'>Фактический
  адрес:</span></p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:6.0pt;font-family:"Times New Roman","serif"'>397160,
  Воронежская обл., г.Борисоглебск,</span></p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:6.0pt;font-family:"Times New Roman","serif"'>Ул.Карла
  Маркса, д. 104, оф. 7</span></p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span lang=EN-US style='font-size:6.0pt;font-family:"Times New Roman","serif"'>Тел.:
  8-952-106-36-37</span></p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span lang=EN-US style='font-size:6.0pt;font-family:"Times New Roman","serif"'>e-mail:
  finuslugi36@mail.ru</span></p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;text-autospace:none'><span lang=EN-US style='font-size:12.0pt;
  font-family:"Times New Roman","serif"'>&nbsp;</span></p>
            </td>
            <td width=367 valign=top >
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;text-autospace:none'><b><span style='font-size:6.0pt;font-family:
  "Times New Roman","serif";'><?=$client->name?>, </span></b></p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;text-autospace:none'><b><span style='font-size:6.0pt;font-family:
  "Times New Roman","serif";'><?=$client->passport?>,</span></b></p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;text-autospace:none'><b><span style='font-size:6.0pt;font-family:
  "Times New Roman","serif";'>адрес регистрации: <?=$client->registration_address?></span></b><span
                        style='font-size:6.0pt;font-family:"Times New Roman","serif";color:#00B050'> 
  </span></p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;text-autospace:none'><b>адрес фактического проживания: <?=$client->current_address?>
 </b></p>
                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;text-autospace:none'><b>тел. <?=$client->phone?></b></p>
            </td>
        </tr>
    </table>

    

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
99%;text-autospace:none'><span style='font-size:8.1pt;line-height:99%;
font-family:"Times New Roman","serif"'>ИП  
_________________________/Прокопенко А.Н/    
__________________/_______________________/</span></p>

</div>

<span style='font-size:12.0pt;font-family:"Times New Roman","serif"'><br
        clear=all style='page-break-before:always'>
</span>

<div class=Section2>

    <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:11.0cm;margin-bottom:.0001pt;line-height:normal'><span
                style='position:absolute;z-index:8;left:0px;margin-left:373px;margin-top:10px;
width:7px;height:126px'>
<!--            <img width=7 height=126-->
<!--                                       src="/img/border.png">-->
        </span><span
                style='position:absolute;z-index:1;left:0px;margin-left:2px;margin-top:19px;
width:354px;height:106px'>
<!--            <img width=354 height=106-->
<!--                 src="/img/agreement-logo.png ">-->
        </span>
        <a
                name=page9></a><span style='font-size:9.0pt;font-family:"Times New Roman","serif"'>Приложение
№ 1 к Договору на оказание юридических услуг с физическим лицом № </span><b><span
                    style='font-size:6.0pt;font-family:"Times New Roman","serif";'><?=$contract->number?>
</span></b></p>

    <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:11.0cm;margin-bottom:.0001pt;line-height:normal'><b><span
                style='font-size:6.0pt;font-family:"Times New Roman","serif";'>от
<?=date('d.m.Yг.')?></span></b></p>

    <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:25.0pt;margin-bottom:.0001pt;line-height:99%;text-autospace:none'><span
            style='font-size:9.0pt;line-height:99%;font-family:"Times New Roman","serif"'>от
</span><b><span style='font-size:9.5pt;line-height:99%;font-family:"Times New Roman","serif";'><?=$current_date?></span></b></p>

    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal;text-autospace:none'><b><span
                style='font-family:"Times New Roman","serif"'>Тарифы на оказание юридических
услуг</span></b></p>
    <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=733
           style='width:550.0pt;margin-left:.5pt;border-collapse:collapse'>
        <tr style='height:12.95pt'>
            <td width=47 valign=bottom>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;text-autospace:none'><span lang=EN-US
                                                                  style='font-family:"Times New Roman","serif"'>№</span><span style='font-family:
  "Times New Roman","serif"'> п/п</span></p>
            </td>
            <td width=442 valign=bottom >
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;text-autospace:none'><span
                        style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Тарифы</span></p>
            </td>
            <td width=244 valign=bottom >
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;text-autospace:none'><span lang=EN-US
                                                                  style='font-family:"Times New Roman","serif"'>Стоимость</span></p>
            </td>
        </tr>
        <tr style='height:12.95pt'>
            <td width=47 valign=bottom>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.95pt;text-autospace:none'><span lang=EN-US
                                                                   style='font-family:"Times New Roman","serif"'>1</span></p>
            </td>
            <td width=442 valign=bottom >
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;text-autospace:none'><span
                        style='font-family:"Times New Roman","serif"'>Тариф «Эконом» при сумме долга
  до 150&nbsp;000 руб.</span></p>
            </td>
            <td width=244 valign=bottom >
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;text-autospace:none'><span
                        style='font-family:"Times New Roman","serif"'>4000 руб.</span></p>
            </td>
        </tr>
        <tr style='height:12.15pt'>
            <td width=47 valign=bottom >
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:12.0pt;text-autospace:none'><span lang=EN-US
                                                                  style='font-family:"Times New Roman","serif"'>2</span></p>
            </td>
            <td width=442 valign=bottom >
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.95pt;text-autospace:none'><span
                        style='font-family:"Times New Roman","serif"'>Тариф «Базовый» при сумме долга
  от 150&nbsp;000 руб. </span></p>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.95pt;text-autospace:none'><span
                        style='font-family:"Times New Roman","serif"'>до 300&nbsp;000 руб.</span></p>
            </td>
            <td width=244 valign=bottom >
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.95pt;text-autospace:none'><span
                        style='font-family:"Times New Roman","serif"'>6700 руб.</span></p>
            </td>
        </tr>
        <tr style='height:12.2pt'>
            <td width=47 valign=bottom >
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.85pt;text-autospace:none'><span
                        style='font-family:"Times New Roman","serif"'>3</span></p>
            </td>
            <td width=442 valign=bottom >
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.95pt;text-autospace:none'><span
                        style='font-family:"Times New Roman","serif"'>Тариф «Премиум» при сумме долга
  от 300&nbsp;000 руб. </span></p>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:12.0pt;text-autospace:none'><span
                        style='font-family:"Times New Roman","serif"'>до 800&nbsp;000 руб.</span></p>
            </td>
            <td width=244 valign=bottom >
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:12.0pt;text-autospace:none'><span
                        style='font-family:"Times New Roman","serif"'>7700 руб.</span></p>
            </td>
        </tr>
        <tr style='height:16.35pt'>
            <td width=47 valign=bottom >
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.95pt;text-autospace:none'><span
                        style='font-family:"Times New Roman","serif"'>4</span></p>
            </td>
            <td width=442 valign=bottom>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.95pt;text-autospace:none'><span
                        style='font-family:"Times New Roman","serif"'>Тариф «Максимальный» при сумме
  долга от 800&nbsp;000 руб. </span></p>
            </td>
            <td width=244 valign=bottom >
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.85pt;text-autospace:none'><span
                        style='font-family:"Times New Roman","serif"'>10500 руб.</span></p>
            </td>
        </tr>
        <tr style='height:16.35pt'>
            <td width=47 valign=bottom >
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.95pt;text-autospace:none'><span
                        style='font-family:"Times New Roman","serif"'>5</span></p>
            </td>
            <td width=442 valign=bottom >
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.95pt;text-autospace:none'><span
                        style='font-family:"Times New Roman","serif"'>Тариф «Индивидуальный»</span></p>
            </td>
            <td width=244 valign=bottom >
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.85pt;text-autospace:none'><span
                        style='font-family:"Times New Roman","serif"'>20% от суммы иска</span></p>
            </td>
        </tr>
    </table>

    <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:96.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
none'><b><span style='font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>

    <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:96.0pt;margin-bottom:.0001pt;line-height:normal;text-autospace:
none'><b><span style='font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>

    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal;text-autospace:none'><b><span
                style='font-family:"Times New Roman","serif"'>Тарифы на оказание дополнительных
юридических услуг</span></b></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
11.0pt;text-autospace:none'><span style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

    <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=733
           style='width:550.0pt;margin-left:.5pt;border-collapse:collapse'>
        <tr style='height:12.95pt'>
            <td width=47 valign=bottom >
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;text-autospace:none'><span lang=EN-US
                                                                  style='font-family:"Times New Roman","serif"'>№</span><span style='font-family:
  "Times New Roman","serif"'> п/п</span></p>
            </td>
            <td width=442 valign=bottom >
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;text-autospace:none'><span lang=EN-US
                                                                  style='font-family:"Times New Roman","serif"'>Перечень письменных работ</span></p>
            </td>
            <td width=244 valign=bottom>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;text-autospace:none'><span lang=EN-US
                                                                  style='font-family:"Times New Roman","serif"'>Стоимость</span></p>
            </td>
        </tr>
        <tr style='height:12.95pt'>
            <td width=47 valign=bottom>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.95pt;text-autospace:none'><span lang=EN-US
                                                                   style='font-family:"Times New Roman","serif"'>1</span></p>
            </td>
            <td width=442 valign=bottom>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;text-autospace:none'><span lang=EN-US
                                                                  style='font-family:"Times New Roman","serif"'>Юридическая консультация</span></p>
            </td>
            <td width=244 valign=bottom>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;text-autospace:none'><span lang=EN-US
                                                                  style='font-family:"Times New Roman","serif"'>350 руб.</span></p>
            </td>
        </tr>
        <tr style='height:12.15pt'>
            <td width=47 valign=bottom>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:12.0pt;text-autospace:none'><span lang=EN-US
                                                                  style='font-family:"Times New Roman","serif"'>2</span></p>
            </td>
            <td width=442 valign=bottom>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.95pt;text-autospace:none'><span lang=EN-US
                                                                   style='font-family:"Times New Roman","serif"'>Письменный комментарий
  документов</span></p>
            </td>
            <td width=244 valign=bottom style='width:183.0pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 0cm 0cm 0cm;height:12.15pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.95pt;text-autospace:none'><span lang=EN-US
                                                                   style='font-family:"Times New Roman","serif"'>1500 руб.</span></p>
            </td>
        </tr>
        <tr style='height:12.2pt'>
            <td width=47 valign=bottom>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.85pt;text-autospace:none'><span lang=EN-US
                                                                   style='font-family:"Times New Roman","serif"'>3</span></p>
            </td>
            <td width=442 valign=bottom>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:12.0pt;text-autospace:none'><span
                        style='font-family:"Times New Roman","serif"'>Отзыв согласия на обработку
  персональных данных</span></p>
            </td>
            <td width=244 valign=bottom>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:12.0pt;text-autospace:none'><span lang=EN-US
                                                                  style='font-family:"Times New Roman","serif"'>500 руб.</span></p>
            </td>
        </tr>
        <tr style='height:12.15pt'>
            <td width=47 valign=bottom>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.95pt;text-autospace:none'><span lang=EN-US
                                                                   style='font-family:"Times New Roman","serif"'>4</span></p>
            </td>
            <td width=442 valign=bottom>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.85pt;text-autospace:none'><span lang=EN-US
                                                                   style='font-family:"Times New Roman","serif"'>Заявление (досудебное)</span></p>
            </td>
            <td width=244 valign=bottom>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.85pt;text-autospace:none'><span lang=EN-US
                                                                   style='font-family:"Times New Roman","serif"'>1500 руб.</span></p>
            </td>
        </tr>
        <tr style='height:12.15pt'>
            <td width=47 valign=bottom>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.85pt;text-autospace:none'><span lang=EN-US
                                                                   style='font-family:"Times New Roman","serif"'>5</span></p>
            </td>
            <td width=442 valign=bottom>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.95pt;text-autospace:none'><span lang=EN-US
                                                                   style='font-family:"Times New Roman","serif"'>Жалоба (досудебная)</span></p>
            </td>
            <td width=244 valign=bottom style='width:183.0pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 0cm 0cm 0cm;height:12.15pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.95pt;text-autospace:none'><span lang=EN-US
                                                                   style='font-family:"Times New Roman","serif"'>1500 руб.</span></p>
            </td>
        </tr>
        <tr style='height:12.15pt'>
            <td width=47 valign=bottom >
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.95pt;text-autospace:none'><span lang=EN-US
                                                                   style='font-family:"Times New Roman","serif"'>6</span></p>
            </td>
            <td width=442 valign=bottom >
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.85pt;text-autospace:none'><span lang=EN-US
                                                                   style='font-family:"Times New Roman","serif"'>Претензия (досудебная)</span></p>
            </td>
            <td width=244 valign=bottom >
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.85pt;text-autospace:none'><span lang=EN-US
                                                                   style='font-family:"Times New Roman","serif"'>1500 руб.</span></p>
            </td>
        </tr>
        <tr style='height:12.15pt'>
            <td width=47 valign=bottom>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.85pt;text-autospace:none'><span lang=EN-US
                                                                   style='font-family:"Times New Roman","serif"'>7</span></p>
            </td>
            <td width=442 valign=bottom>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.95pt;text-autospace:none'><span
                        style='font-family:"Times New Roman","serif"'>Исковое заявление в суд (в т.ч.
  встречное)</span></p>
            </td>
            <td width=244 valign=bottom>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.95pt;text-autospace:none'><span
                        style='font-family:"Times New Roman","serif"'>4500 руб.</span></p>
            </td>
        </tr>
        <tr style='height:12.15pt'>
            <td width=47 valign=bottom>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.95pt;text-autospace:none'><span
                        style='font-family:"Times New Roman","serif"'>8</span></p>
            </td>
            <td width=442 valign=bottom>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.85pt;text-autospace:none'><span
                        style='font-family:"Times New Roman","serif"'>Отзыв на исковое заявление</span></p>
            </td>
            <td width=244 valign=bottom>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.85pt;text-autospace:none'><span
                        style='font-family:"Times New Roman","serif"'>3000 руб.</span></p>
            </td>
        </tr>
        <tr style='height:12.15pt'>
            <td width=47 valign=bottom>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:12.0pt;text-autospace:none'><span
                        style='font-family:"Times New Roman","serif"'>9</span></p>
            </td>
            <td width=442 valign=bottom>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.95pt;text-autospace:none'><span
                        style='font-family:"Times New Roman","serif"'>Возражение на исковые
  требования</span></p>
            </td>
            <td width=244 valign=bottom>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.95pt;text-autospace:none'><span
                        style='font-family:"Times New Roman","serif"'>4500 руб.</span></p>
            </td>
        </tr>
        <tr style='height:12.2pt'>
            <td width=47 valign=bottom>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.85pt;text-autospace:none'><span
                        style='font-family:"Times New Roman","serif"'>10</span></p>
            </td>
            <td width=442 valign=bottom>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:12.0pt;text-autospace:none'><span
                        style='font-family:"Times New Roman","serif"'>Заявление на отмену судебного
  приказа</span></p>
            </td>
            <td width=244 valign=bottom>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:12.0pt;text-autospace:none'><span lang=EN-US
                                                                  style='font-family:"Times New Roman","serif"'>1000 руб.</span></p>
            </td>
        </tr>
        <tr style='height:12.15pt'>
            <td width=47 valign=bottom>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.95pt;text-autospace:none'><span lang=EN-US
                                                                   style='font-family:"Times New Roman","serif"'>11</span></p>
            </td>
            <td width=442 valign=bottom>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.85pt;text-autospace:none'><span
                        style='font-family:"Times New Roman","serif"'>Заявление на отмену заочного
  решения суда</span></p>
            </td>
            <td width=244 valign=bottom>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.85pt;text-autospace:none'><span lang=EN-US
                                                                   style='font-family:"Times New Roman","serif"'>2000 руб.</span></p>
            </td>
        </tr>
        <tr style='height:12.15pt'>
            <td width=47 valign=bottom>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.85pt;text-autospace:none'><span lang=EN-US
                                                                   style='font-family:"Times New Roman","serif"'>12</span></p>
            </td>
            <td width=442 valign=bottom>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.95pt;text-autospace:none'><span lang=EN-US
                                                                   style='font-family:"Times New Roman","serif"'>Жалоба на определение суда</span></p>
            </td>
            <td width=244 valign=bottom>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.95pt;text-autospace:none'><span lang=EN-US
                                                                   style='font-family:"Times New Roman","serif"'>2000 руб.</span></p>
            </td>
        </tr>
        <tr style='height:12.15pt'>
            <td width=47 valign=bottom>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.85pt;text-autospace:none'><span lang=EN-US
                                                                   style='font-family:"Times New Roman","serif"'>13</span></p>
            </td>
            <td width=442 valign=bottom>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.85pt;text-autospace:none'><span lang=EN-US
                                                                   style='font-family:"Times New Roman","serif"'>Ходатайство об истребовании
  документов</span></p>
            </td>
            <td width=244 valign=bottom >
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.85pt;text-autospace:none'><span lang=EN-US
                                                                   style='font-family:"Times New Roman","serif"'>1000 руб.</span></p>
            </td>
        </tr>
        <tr style='height:12.15pt'>
            <td width=47 valign=bottom>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.85pt;text-autospace:none'><span
                        style='font-family:"Times New Roman","serif"'>14</span></p>
            </td>
            <td width=442 valign=bottom>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.85pt;text-autospace:none'><span
                        style='font-family:"Times New Roman","serif"'>Представительство в суде (за 1
  день судебного заседания)</span></p>
            </td>
            <td width=244 valign=bottom>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.85pt;text-autospace:none'><span
                        style='font-family:"Times New Roman","serif"'>5000 руб.</span></p>
            </td>
        </tr>
        <tr style='height:12.15pt'>
            <td width=47 valign=bottom>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.85pt;text-autospace:none'><span
                        style='font-family:"Times New Roman","serif"'>15</span></p>
            </td>
            <td width=442 valign=bottom>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.85pt;text-autospace:none'><span
                        style='font-size:6.0pt;font-family:"Times New Roman","serif"'>Запрос в Банк
  на предоставление документы (договор/выписка/разъяснение)</span></p>
            </td>
            <td width=244 valign=bottom>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:11.85pt;text-autospace:none'><span
                        style='font-family:"Times New Roman","serif"'>500 руб.</span></p>
            </td>
        </tr>
    </table>

    <p class=MsoNormal>______________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________</p>


    <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'><span
                style='position:absolute;z-index:8;left:0px;margin-left:373px;margin-top:10px;
width:7px;height:126px'>
<!--         <img width=7 height=126-->
<!--                                   src="/img/border.png">-->
        </span><span
                style='position:absolute;z-index:1;left:0px;margin-left:2px;margin-top:19px;
width:354px;height:106px'>
<!--            <img width=354 height=106-->
<!--                 src="/img/agreement-logo.png">-->
        </span><a
                name=page9></a><span style='font-size:9.0pt;font-family:"Times New Roman","serif"'>Приложение
№ 2 к Договору на оказание юридических услуг с физическим лицом № </span><b><span
                    style='font-size:6.0pt;font-family:"Times New Roman","serif";'><?=$contract->number?>
</span></b></p>

    <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'><b><span
                style='font-size:6.0pt;font-family:"Times New Roman","serif";'>от
<?=date('d.m.Yг.')?></span></b></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
12.95pt;text-autospace:none'><span style='font-size:12.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

    <table>
        <tr>
            <td style="width: 400px;">
                <div class="container-info">
                    <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'><span
                                style='font-family:"Times New Roman","serif";'>ИП Прокопенко
Анастасия Николаевна</span></p>

                    <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'><span
                                style='font-family:"Times New Roman","serif";'>ИНН 360408101210</span></p>

                    <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'><span
                                style='font-family:"Times New Roman","serif";'>ОГРН
315366800050012</span></p>

                    <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'><span
                                style='font-family:"Times New Roman","serif";'>Фактический адрес:</span></p>

                    <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'><span
                                style='font-family:"Times New Roman","serif";'>397160, Воронежская
обл., г.Борисоглебск,</span></p>

                    <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'><span
                                style='font-family:"Times New Roman","serif";'>Ул.Карла Маркса, д.
104, оф. 7</span></p>

                    <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'><span lang=EN-US
                                                                                                                                                style='font-family:"Times New Roman","serif";'>Тел.:
8-952-106-36-37</span></p>

                    <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'><span
                                style='font-family:"Times New Roman","serif"'>&nbsp;</span></p>
                </div>
            </td>
            <td>
                <table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0 width=656
                       style='width:491.7pt;border-collapse:collapse;border:none; height: 170px;'>
                    <tr style='height:40.1pt'>
                        <td width=328 valign=top>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;text-autospace:none'><b><span style='font-size:6.0pt;font-family:
  "Times New Roman","serif";'><?=$client->name?>, </span></b></p>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;text-autospace:none'><b><span style='font-size:6.0pt;font-family:
  "Times New Roman","serif";'><?=$client->passport?>,
  12.03.2011г., 362-016, </span></b></p>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;text-autospace:none'><b><span style='font-size:6.0pt;font-family:
  "Times New Roman","serif";'>адрес регистрации: <?=$client->registration_address?>,</span></b><span
                                        style='font-size:6.0pt;font-family:"Times New Roman","serif";color:#00B050'> 
  </span></p>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;text-autospace:none'><b><span style='font-size:6.0pt;font-family:
  "Times New Roman","serif";'>адрес фактического проживания:
                                        <?=$client->current_address?></span></p>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;text-autospace:none'><b><span style='font-size:12.0pt;font-family:
  "Times New Roman","serif";'>тел. <?=$client->phone?></span></b></p>
                        </td>
                        <td width=328 valign=top>
                            <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;text-autospace:none'><span style='font-family:"Times New Roman","serif";
  color:red'>&nbsp;</span></p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:248.0pt;margin-bottom:.0001pt;line-height:99%;text-autospace:none'><span
            style='font-family:"Times New Roman","serif"'>ЗАЯВЛЕНИЕ</span></p>

    <p class=MsoNormal style='margin-top:0cm;margin-right:9.0pt;margin-bottom:0cm;
margin-left:0cm;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
text-indent:35.4pt;line-height:91%;punctuation-wrap:simple;text-autospace:none'><span
            style='font-family:"Times New Roman","serif"; font-size: 5.0pt;'>Прошу Вас оказать мне юридические
услуги по представлению моих интересов по кредитным договорам для снижения</span><span
            style='font-size:5.0pt;line-height:91%;font-family:"Times New Roman","serif"'>
</span><span style='font-family:"Times New Roman","serif"; font-size: 6.0pt;'>и/или  реструктуризации
задолженности, а также представления интересов в суде и ФССП.</span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:92%;punctuation-wrap:simple;
text-autospace:none'><span style='font-family:"Times New Roman","serif"; font-size: 5.0pt;'>Я, до
подписания, ознакомившись с условиями данного Договора на оказание юридических
услуг, прошу заключить со мной Договор на оказание юридических услуг.</span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:2.65pt;text-autospace:none'><span
            style='font-size:6.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;text-indent:14.15pt;line-height:92%;
punctuation-wrap:simple;text-autospace:none'><span style='font-family:"Times New Roman","serif"; font-size: 6.0pt;'>Я
ознакомлен с условиями Договора и Тарифами, осознаю их и понимаю, согласен с
ними и обязуюсь неукоснительно соблюдать, принимая на себя все права и
обязанности Клиента, указанные в них. Я заявляю, что данный Договор и Тарифы, с
которыми я предварительно ознакомился, являются неотъемлемой частью настоящего
Заявления.</span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:10.0pt;text-autospace:none'><span
            style='font-size:6.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-justify:inter-ideograph;line-height:99%;text-autospace:none'><span
            style='font-family:"Times New Roman","serif"; font-size: 6.0pt;'>Об ответственности за
достоверность представленных сведений предупрежден(а).</span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
99%;text-autospace:none'><span style='font-family:"Times New Roman","serif"; font-size: 6.0pt;'>В
соответствии с Федеральным законом от 27.07.2006г. №152-ФЗ «О персональных
данных»</span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
99%;text-autospace:none'><span style='position:absolute;z-index:5;margin-left:
214px;margin-top:2px;width:17px;height:12px'></span><span
            style='position:absolute;z-index:4;margin-left:114px;margin-top:2px;width:17px;
height:12px'></span><span
            style='font-family:"Times New Roman","serif"; font-size: 6.0pt;'>Я выражаю свое       
согласие             несогласие с обработкой (а именно: сбор, систематизация,
накопление, обновление, изменение, использование, распространение, обмен,
обезличивание, блокирование, уничтожение) моих персональных данных, указанных
выше. </span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
99%;text-autospace:none'><span style='font-family:"Times New Roman","serif"; font-size: 6.0pt;'>Отзыв
моего согласия на обработку персональных данных производится путем направления
соответствующего письменного заявления в Компанию по почте или иным способом,
позволяющим подтвердить факт направления. </span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
99%;text-autospace:none'><span style='font-family:"Times New Roman","serif"; font-size: 6.0pt;'>      
</span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
99%;text-autospace:none'><span style='font-family:"Times New Roman","serif"'>Мой
платеж, составит: _____________________(________________________________________________)руб.</span></p>

    <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right;line-height:99%;text-autospace:none'><b><span
                style='font-family:"Times New Roman","serif"'>________________/______________________________</span></b></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
99%;text-autospace:none'><b><span style='font-size:9.0pt;line-height:99%;
font-family:"Times New Roman","serif"'>                                                                                                                                 
(подпись)                        </span></b><b><span style='font-size:8.5pt;
line-height:99%;font-family:"Times New Roman","serif"'>(расшифровка)</span></b></p>

    <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:11.0cm;margin-bottom:.0001pt;line-height:normal'><span
                style='position:absolute;z-index:8;left:0px;margin-left:373px;margin-top:10px;
width:7px;height:126px'>
<!--            <img width=7 height=126-->
<!--                    src="/img/border.png">-->
        </span><span
                style='position:absolute;z-index:1;left:0px;margin-left:2px;margin-top:19px;
width:354px;height:106px'>
<!--            <img width=354 height=106-->
<!--                 src="/img/agreement-logo.png">-->
        </span><a
                name=page9></a><span style='font-size:9.0pt;font-family:"Times New Roman","serif"'>Приложение
№ 3 к Договору на оказание юридических услуг с физическим лицом № </span><b><span
                    style='font-size:6.0pt;font-family:"Times New Roman","serif";'><?=$contract->number?>
</span></b></p>

    <p class=MsoNormal style='margin-top:0cm;margin-right:0cm;margin-bottom:0cm;
margin-left:11.0cm;margin-bottom:.0001pt;line-height:normal'><b><span
                style='font-size:6.0pt;font-family:"Times New Roman","serif";'>от
<?=date('d.m.Yг.')?></span></b></p>

    <p class=MsoNormal style='margin-left:11.0cm'><span style='font-size:9.0pt;
line-height:115%;font-family:"Times New Roman","serif"'> </span></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
10.0pt;text-autospace:none'><span style='font-size:9.0pt;font-family:"Times New Roman","serif"'>от
«___» ___________ 20__ г</span></p>
    

    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal;text-autospace:none'><b><span
                style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Перечень
передаваемых в работу кредитных договоров:</span></b></p>

    <table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0
           style='border-collapse:collapse;border:none'>
        <tr>
            <td width=45 valign=top style='width:33.75pt;border:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;text-autospace:none'><span
                        style='font-size:10.1pt;font-family:"Times New Roman","serif"'>№ п/п</span></p>
            </td>
            <td width=688 valign=top style='width:516.05pt;border:solid black 1.0pt;
  border-left:none;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;text-autospace:none'><span
                        style='font-size:10.1pt;font-family:"Times New Roman","serif"'>Номер
  кредитного договора, наименование банка</span></p>
            </td>
        </tr>
        <?php $counter = 1; foreach($client->creditors as $creditor): ?>
            <tr>
                <td width=45 valign=top>
                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal;text-autospace:none'><span
                                style='font-size:9.0pt;font-family:"Times New Roman","serif"'><?=$counter?></span></p>
                </td>
                <td width=688 valign=top>
                    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal;text-autospace:none'><span style='font-size:9.0pt;font-family:"Times New Roman","serif";'><?= /** @var \app\models\Creditor $creditor */
                            $creditor->name?> от <?=$creditor->date_confirmed?></span></p>
                </td>
            </tr>
        <?php $counter++; endforeach; ?>
    </table>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal;text-autospace:none'><span style='font-size:12.0pt;font-family:"Times New Roman","serif"'>Итого:
 <?=count($client->creditors)?> кредитных договора на сумму до                                      руб.
 (с учетом скидки)                  руб. </span></p>

    <p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right;line-height:99%;text-autospace:none'><b><span
                style='font-family:"Times New Roman","serif"'>________________/______________________________</span></b></p>

    <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
99%;text-autospace:none'><b><span style='font-size:9.0pt;line-height:99%;
font-family:"Times New Roman","serif"'>                                                                                                                                 
(подпись)                        </span></b><b><span style='font-size:8.5pt;
line-height:99%;font-family:"Times New Roman","serif"'>(расшифровка)</span></b></p>

</div>

</body>

</html>

