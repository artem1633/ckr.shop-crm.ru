<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Contract */

$this->title = 'Создать новый договор';
$this->params['breadcrumbs'][] = ['label' => 'Договора', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contract-create" style="margin: auto; width: 50%;">


    <?= $this->render('_formcontract', [
        'model' => $model,
        'client_id' => $client_id,
    ]) ?>

</div>
