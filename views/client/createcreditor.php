<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Creditor */

$this->title = 'Создать кредитора';
$this->params['breadcrumbs'][] = ['label' => 'Кредиторы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="creditor-create" style="margin: auto; width: 50%;">


    <?= $this->render('_formcreditor', [
        'model' => $model,
        'client_id' => $client_id,
        'contracts' => $contracts,
        'contract' => $contract,
    ]) ?>

</div>
