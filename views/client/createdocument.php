<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Documents */

$this->title = 'Создать новый документ';
$this->params['breadcrumbs'][] = ['label' => 'Документы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="documents-create" style="margin: auto; width: 50%;">


    <?= $this->render('_formdocument', [
        'model' => $model,
        'client_id' => $client_id,
        'contracts' => $contracts,
        'contract' => $contract,
    ]) ?>

</div>
