<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Payment */

$this->title = 'Добавить платеж';
$this->params['breadcrumbs'][] = ['label' => 'Платежи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-create" style="margin: auto; width: 50%;">


    <?= $this->render('_formpayment', [
        'model' => $model,
        'client_id' => $client_id,
        'paymentTypes' => $paymentTypes,
    ]) ?>

</div>
