﻿<html>

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1251">
<meta name=Generator content="Microsoft Word 12 (filtered)">
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:Wingdings;
	panose-1:5 0 0 0 0 0 0 0 0 0;}
@font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
	{font-family:Tahoma;
	panose-1:2 11 6 4 3 5 4 4 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:10.0pt;
	margin-left:0cm;
	line-height:115%;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";}
p.MsoCommentText, li.MsoCommentText, div.MsoCommentText
	{mso-style-link:"Текст примечания Знак";
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:10.0pt;
	margin-left:0cm;
	line-height:115%;
	font-size:10.0pt;
	font-family:"Calibri","sans-serif";}
a:link, span.MsoHyperlink
	{color:blue;
	text-decoration:underline;}
a:visited, span.MsoHyperlinkFollowed
	{color:purple;
	text-decoration:underline;}
p.MsoCommentSubject, li.MsoCommentSubject, div.MsoCommentSubject
	{mso-style-link:"Тема примечания Знак";
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:10.0pt;
	margin-left:0cm;
	line-height:115%;
	font-size:10.0pt;
	font-family:"Calibri","sans-serif";
	font-weight:bold;}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
	{mso-style-link:"Текст выноски Знак";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:10.0pt;
	font-family:"Tahoma","sans-serif";}
p.ConsPlusNonformat, li.ConsPlusNonformat, div.ConsPlusNonformat
	{mso-style-name:ConsPlusNonformat;
	margin:0cm;
	margin-bottom:.0001pt;
	text-autospace:none;
	font-size:10.0pt;
	font-family:"Courier New";}
span.a
	{mso-style-name:"Текст примечания Знак";
	mso-style-link:"Текст примечания";}
span.a0
	{mso-style-name:"Тема примечания Знак";
	mso-style-link:"Тема примечания";
	font-weight:bold;}
span.a1
	{mso-style-name:"Текст выноски Знак";
	mso-style-link:"Текст выноски";
	font-family:"Tahoma","sans-serif";}
@page Section1
	{size:595.25pt 841.9pt;
	margin:21.3pt 1.0cm 14.2pt 70.9pt;}
div.Section1
	{page:Section1;}
 /* List Definitions */
 ol
	{margin-bottom:0cm;}
ul
	{margin-bottom:0cm;}
-->
</style>

</head>

<body lang=RU link=blue vlink=purple>

<div class=Section1>

<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal;text-autospace:none'><b><span
style='font-family:"Times New Roman","serif"'>ДОГОВОР СУБАРЕНДЫ</span></b></p>

<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal;text-autospace:none'><b><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>ТРАНСПОРТНОГО
СРЕДСТВА БЕЗ ЭКИПАЖА № <?=date('dmy',time()).'-'.$doc->day; ?></span></b></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><b><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal;text-autospace:none'><span style='font-size:10.0pt;
font-family:"Times New Roman","serif"'>г.&nbsp;Москва                                
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<?= date('d.m.Y',time()); ?>

<br>
<br>
</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>Общество с
ограниченной ответственностью «Мир ин Медиа», именуемое в дальнейшем
&quot;Арендатор&quot;, в лице Генерального директора Игнаткина Игоря Петровича,
действующего на основании Устава с одной стороны, и Гражданин <?=$people->name; ?>,
 именуемый в дальнейшем &quot;Субарендатор&quot;, именуемые
вместе &quot;Стороны&quot;, а по отдельности &quot;Сторона&quot;, заключили
настоящий договор (далее - Договор) о нижеследующем.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><b><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>

<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal;text-autospace:none'><b><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>1. ПРЕДМЕТ
ДОГОВОРА</span></b></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><b><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>1.1.             Арендатор
обязуется предоставить Субарендатору без экипажа за арендную плату во временное
владение и пользование без оказания услуг по управлению им и по его технической
эксплуатации следующее транспортное средство:</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-family:"Times New Roman","serif"'>наименование (тип) легковой
седан,</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>марка, модель </span><span
lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif"'>
<?=$avto->marka?>
</span><span style='font-size:10.0pt;font-family:"Times New Roman","serif"'>,</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>категория  <?=$avto->category?>,</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>государственный
регистрационный знак  <?=$avto->znak?>,</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>идентификационный
номер (VIN)  </span><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif"'><?=$avto->vin?></span><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>,</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>год выпуска <?=$avto->god?>,</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>шасси (рама)
<?=$avto->rama?></span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>цвет <?=$avto->color?>,</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>мощность двигателя
</span><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif"'><?=$avto->mosh?></span><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'> (кВт),</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>а Субарендатор
обязуется принять, оплатить владение и пользование и своевременно возвратить
Транспортное средство в исправном состоянии.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>1.2. Передаваемое
в субаренду ТС принадлежит Арендатору на основании Договора аренды транспортного
средства без экипажа № <?=$avto->dogovor_rent; ?> от <?= date('d.m.Y',$avto->dogovor_rent_data); ?></span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>1.3.Транспортное
средство находится в исправном состоянии и отвечает требованиям, предъявляемым
к эксплуатируемым транспортным средствам, используемым для следующих целей: в
личных целях. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>1.4.             
Оценочная стоимость транспортного средства устанавливается в размере 700 000
руб. 00 коп. (семьсот тысяч рублей (я) ноль копеек(йки).</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><a
name=Par28></a><span style='font-size:10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><b><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>

<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal;text-autospace:none'><b><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>2. ПРАВА И
ОБЯЗАННОСТИ СТОРОН</span></b></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><b><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>2.1. Арендатор
обязан:</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>2.1.1.  Предоставить
Транспортное средство Арендатору в порядке и на условиях настоящего Договора.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>2.1.2. Предоставить
Арендатору Транспортное средство в состоянии, соответствующем условиям Договора
и назначению Транспортного средства, со всеми его принадлежностями и относящейся
к нему документацией.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>2.1.3. Перед
подписанием Акта возврата транспортного произвести внешний и внутренний осмотр
Транспортного средства, а также его рабочее состояние.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>2.2. Арендатор
вправе:</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>2.2.1. В любой
момент обязать Субарендатора предоставить Транспортное средство на осмотр.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>2.2.2. В любой момент
обязать Субарендатора вернуть Транспортное средство.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>2.3. Субарендатор
обязан:</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>2.3.1. Перед
подписанием Акта приемки-передачи транспортного средства произвести внешний и
внутренний осмотр Транспортного средства, а также его рабочее состояние.</span></p>

<br/>
<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>Страница 1</span></p>

<br/>
<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>2.3.2. Вносить
арендную плату в размере, сроки и в порядке, предусмотренные настоящим
Договором.</span></p>



<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><a
name=Par55></a><a name=Par59></a><span style='font-size:10.0pt;font-family:
"Times New Roman","serif"'>2.3.3. Поддерживать надлежащее состояние
Транспортного средства.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>2.3.4. Обеспечить
сохранность ТС, бережное обращение и содержание ТС в исправном состоянии, в том
числе контролировать уровень моторного масла, других эксплуатационных
жидкостей, уровень давления в шинах, а также показания датчиков аварийного
состояния отдельных систем автомобиля. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>2.3.5.
Эксплуатировать ТС только на территории г. Москвы и Московской области.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>2.3.6. Соблюдать
лимит суточного и месячного ограничения по пробегу:</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>2.3.6.1.
Суточный лимит: _______________________________;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>2.3.6.2.
Месячный лимит: _______________________________.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>2.3.7. По
первому требованию Арендатора предоставить машину Транспортное средство на
осмотр, в чистом виде. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>2.3.8. По
первому требованию Арендатора вернуть Транспортное средство.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>2.3.9. Соблюдать
запрет на курение, а также запрет на употребление алкогольной и
спиртосодержащей продукции в салоне транспортного средства.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>2.3.10. Субарендатор
обязуется незамедлительно сообщить Арендатору обо всех случаях причинения
ущерба ТС или нарушения его работоспособности, а также об его угоне или его
утрате по иным обстоятельствам. В случае возникновения угрозы причинения ущерба
ТС или его поломки, Субарендатор обязан прекратить его дальнейшую эксплуатацию
и незамедлительно сообщить об этом Арендатору. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>2.3.11. В случае
ДТП Субарендатор обязуется НЕЗАМЕДЛИТЕЛЬНО ОПОВЕСТИТЬ АРЕНДАТОРА, установленным
данным договором надлежащим способом, затем:</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'> 1) выполнить
обязанности, предусмотренные ПДД РФ при ДТП, незамедлительно вызвать
сотрудников ГИБДД, о событии уведомить Арендодателя; </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'> 2) получить от
сотрудника ГИБДД:  а) извещение о ДТП по форме 154 с синим или черным штампом в
верхнем углу и подписью инспектора; б) определение; в) акт медицинского
освидетельствования; г) постановление об отказе или возбуждении дела с подписью
инспектора; проверить правильность внесенных сведений - ФИО, № в/у, данных
автомобиля, проверить наличие в справке всех полученных повреждений автомобиля
(за каждое неотмеченное в документах повреждение, Субарендатор несет
материальную ответственность);  </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>3) уточнить у
прибывших сотрудников ГИБДД порядок и сроки возможного дооформления документов,
а именно: дату, время и место получения определения, постановления, если их
наличие отмечено в справке по установленной форме;  </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>4) записать ФИО
очевидцев, их паспортные данные, телефоны, адреса проживания;  </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>5) в течение 2
дней (если иной срок не установлен для получения необходимых документов)
предоставить Арендатору подлинники документов, подтверждающих факт наступления ДТП
и его последствий с указанием обстоятельств, а именно: документы для признания
события страховым, в т.ч. с указанием иных участников ДТП (копия протокола и постановления
по делу об административном правонарушении, определения (если выносилось),
справки установленного образца) и заверенное постановление о возбуждении или
отказе в возбуждении уголовного дела, если таковое имело место. В случае
возбуждения уголовного дела представить окончательное решение (постановление об
отказе в возбуждении уголовного дела, постановление о приостановлении
уголовного дела, обвинительное заключение или решение суда). </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>2.3.12. В случае
досрочного возврата арендованного транспортного средства, уведомить об этом
Арендатора в срок не позднее, чем за 7 (Семь) календарных дней до
предполагаемой даты возврата.</span></p>

<p class=MsoNormal style='text-align:justify'><span style='font-size:10.0pt;
line-height:115%;font-family:"Times New Roman","serif"'>          2</span><span
lang=EN-US style='font-size:10.0pt;line-height:115%;font-family:"Times New Roman","serif"'>.3.13.
</span><span style='font-size:10.0pt;line-height:115%;font-family:"Times New Roman","serif"'>Cубарендатор
обязан нести возникающие в связи с коммерческой эксплуатацией арендованного
автомобиля расходы, в том числе расходы на оплату горюче-смазочных и других
расходуемых в процессе эксплуатации материалов.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><b><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>

<br/>
<br/>
<br/>
<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>Страница 2</span></p>

<br/>

<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal;text-autospace:none'><b><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>3. СРОК АРЕНДЫ</span></b></p>

<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal;text-autospace:none'><b><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal;text-autospace:none'><span style='font-size:10.0pt;font-family:"Times New Roman","serif"'>3.1.
 Срок аренды бессрочно.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal;text-autospace:none'><span style='font-size:10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal;text-autospace:none'><b><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>

<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal;text-autospace:none'><b><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>4. РАЗМЕР, СРОКИ
И ПОРЯДОК ВНЕСЕНИЯ АРЕНДНОЙ ПЛАТЫ. ЗАЛОГ</span></b></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><b><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><a
name=Par79></a><span style='font-size:10.0pt;font-family:"Times New Roman","serif"'>4.1.
Арендная плата за пользование Транспортного средства устанавливается в размере <?=$avto->arenda?> руб. за 1 (одни) сутки.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><a
name=Par80></a><span style='font-size:10.0pt;font-family:"Times New Roman","serif"'>4.2.
Настоящим стороны договорились, что арендная плата, указанная в п. 4.1.
настоящего Договора, является фиксированной и подлежит изменению только по дополнительному
соглашению Сторон.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>4.3. Арендная
плата вносится за период неделя (<i>указать период: день, неделя, месяц, иной
период</i>)<i> </i>аренды ТС, в срок не позднее 5 (Пяти) календарных дней (не
нужное зачеркнуть)</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>- до  даты
передачи ТС в аренду</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>- с момента подписания
Акта приемки-возврата ТС.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>4.4. Субарендатор
в обеспечение исполнения своих обязательств по настоящему Договору уплачивает
Арендатору залог в размере  <?=$avto->zalog?> рублей 00 копеек. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>4.5. Залог
возвращается Субарендатору после исполнения им всех обязанностей по Договору
аренды, не позднее 14 (Четырнадцати) календарных дней после подписания Акта приемки-возврата
транспортного средства, за вычетом дополнительных расходов, в том числе административных
штрафов за нарушение правил дорожного движения. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>4.6. Залог
Субарендатору не возвращается в следующих случаях:</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>4.6.1. В случае нарушения
срока указанного в п. 2.3.12. настоящего Договора об уведомлении Арендатора о
возврате транспортного средства;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>4.6.2. В случае,
если срок аренды составляет менее 30 (Тридцати) календарных дней.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>4.7. Расчеты по
настоящему Договору осуществляются по выбору Сторон в следующем порядке: путем перевода
денежных средств на расчетный счет Арендатора либо наличным платежом в кассу Арендатора.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>4.8.
Обязательство Субарендатора по внесению арендной платы считается исполненным с
момента зачисления денежных средств на расчетный счет Арендатора или с момента
внесения Субарендатором денежных средств в кассу Арендатора.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><a
name=Par88></a><b><span style='font-size:10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>

<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal;text-autospace:none'><b><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>5. ВОЗВРАТ
ИМУЩЕСТВА АРЕНДОДАТЕЛЮ</span></b></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><b><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>5.1. Субарендатор
обязан за свой счет подготовить Транспортное средство к возврату Арендатору.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>5.2. Субарендатор
обязан вернуть Арендатору Транспортное средство в том состоянии, в котором он
его получил, с учетом нормального износа. Возврат Транспортного средства
осуществляется вместе со всеми принадлежностями и документацией по Акту
приемки-возврата транспортного средства</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>- по адресу: г.
Москва, Золоторожская набережная 1с1 офис 406.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>Акт возврата
транспортного средства, подписанный Сторонами, является неотъемлемой частью
Договора.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>5.3. В случае
несвоевременного возврата Транспортного средства Арендатор вправе потребовать
от Субарендатора внесения арендной платы за все время просрочки. </span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><b><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>

<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal;text-autospace:none'><b><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>6.
ОТВЕТСТВЕННОСТЬ СТОРОН</span></b></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><b><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>6.1. За
неисполнение или ненадлежащее исполнение настоящего Договора Стороны несут
ответственность в соответствии с настоящим Договором и действующим
законодательством Российской Федерации.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>6.2. Всю
ответственность, связанную с эксплуатацией  транспортного средства, в том числе
уголовную, а также за вред причиненные третьим лицам, имуществу, арендованным
транспортным средством, несет Субарендатор в соответствии с действующим
законодательством Российской Федерации.</span></p>


<br/>
<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>Страница 3</span></p>

<br/>


<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>6.3. В случае
нарушения Субарендатором запрета, предусмотренного п. 2.3.9. настоящего
Договора, Арендатор удерживает из залога штраф в размере  5000 (пять тысяч)
рублей.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal;text-autospace:none'><span style='font-size:10.0pt;
font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal;text-autospace:none'><b><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>7. ФОРС-МАЖОР</span></b></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><b><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>7.1. Стороны
освобождаются от ответственности за неисполнение или ненадлежащее исполнение
обязательств по Договору, если надлежащее исполнение оказалось невозможным
вследствие непреодолимой силы, то есть чрезвычайных и непредотвратимых при
данных условиях обстоятельств, под которыми понимаются: запретные действия
властей, гражданские волнения, эпидемии, блокада, эмбарго, землетрясения,
наводнения, пожары или другие стихийные бедствия.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>7.2. В случае
наступления этих обстоятельств Сторона обязана в течение 10 дней уведомить об
этом другую Сторону.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>7.3. Документ,
выданный уполномоченным государственным органом и т.д., является достаточным
подтверждением наличия и продолжительности действия непреодолимой силы.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>7.4. Если
обстоятельства непреодолимой силы продолжают действовать более 60 дней, то
каждая сторона вправе расторгнуть Договор в одностороннем порядке.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><b><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>

<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal;text-autospace:none'><b><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>8. РАЗРЕШЕНИЕ
СПОРОВ</span></b></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><b><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><a
name=Par137></a><span style='font-size:10.0pt;font-family:"Times New Roman","serif"'>8.1.
Все споры, связанные с заключением, толкованием, исполнением и расторжением
Договора, будут разрешаться Сторонами путем переговоров.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>8.2. В случае не
достижения соглашения в ходе переговоров, спор передается в арбитражный суд по
месту нахождения ответчика в соответствии с действующим законодательством РФ.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><b><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>

<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal;text-autospace:none'><b><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>9. ЗАКЛЮЧИТЕЛЬНЫЕ
ПОЛОЖЕНИЯ </span></b></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><b><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>9.1. Настоящий
договор вступает в силу с <?= date('d.m.Y',time()); ?></span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>9.2. Все
изменения и дополнения к Договору считаются действительными, если они совершены
в письменной форме и подписаны уполномоченными лицами Сторон. Соответствующие
дополнительные соглашения Сторон являются неотъемлемой частью Договора.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>9.4. Настоящим
стороны договорились, акты и другие документы, отправленные по электронной
почте, а также вся электронная переписка, в том числе смс-сообщения, будут
иметь полную юридическую силу, и могут использоваться в качестве доказательств.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>9.5. Настоящий
договор, может быть расторгнут по соглашению Сторон либо по требованию одной из
Сторон в порядке и по основаниям, предусмотренным действующим законодательством
РФ.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>9.6. Настоящий
Договор составлен в двух подлинных экземплярах, имеющих одинаковую юридическую
силу, - по одному для каждой из Сторон.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><b><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>

<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;text-indent:27.0pt;line-height:normal;text-autospace:none'><b><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>10. АДРЕСА,
РЕКВИЗИТЫ И ПОДПИСИ СТОРОН</span></b></p>

<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0
 style='border-collapse:collapse;border:none'>
 <tr>
  <td width=338 valign=top style='width:253.4pt;border:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><b><span style='font-size:
  10.0pt;font-family:"Times New Roman","serif"'>Субарендатор</span></b></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><b><span style='font-size:
  10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  12.0pt'><b><span style='font-size:10.0pt;font-family:"Times New Roman","serif"'>Ф.И.О.</span></b></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  12.0pt'><span style='font-size:10.0pt;font-family:"Times New Roman","serif"'>
  <?=$people->name?>
  </span><span style='font-size:10.0pt;font-family:"Times New Roman","serif"'>
  </span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  12.0pt'><span style='font-size:10.0pt;font-family:"Times New Roman","serif"'>Дата
  и место рождения: </span><span lang=EN-US style='font-size:10.0pt;
  font-family:"Times New Roman","serif"'>
  <?//= date('d.m.Y',time()); ?>
  <?=date('d.m.Y',strtotime($people->date_birth));?>  
  <?=$people->siti_birth?>
  </span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  12.0pt'><span style='font-size:10.0pt;font-family:"Times New Roman","serif"'>Паспорт:
  серия 
  <?=$people->seria_pasport?>
  </span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  12.0pt'><span style='font-size:10.0pt;font-family:"Times New Roman","serif"'>Выдан:
  <?// if(isset($people->registr)){echo date('d.m.Y',strtotime($people->registr));}?> 
   <?//=$people->registr?>
  </span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  12.0pt'><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif"'>
  <?echo date('d.m.Y',strtotime($people->create));?> 
  
  </span></p>
  
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  12.0pt'><span style='font-size:10.0pt;font-family:"Times New Roman","serif"'>Водительское
  удостоверение:</span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  12.0pt'><span style='font-size:10.0pt;font-family:"Times New Roman","serif"'>
    <?=$people->seria_driving?>
  </span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  12.0pt'><span style='font-size:10.0pt;font-family:"Times New Roman","serif"'>Выдан:
 <?=date('d.m.Y',strtotime($people->kem_driving));?> 
    <?//=$people->kem_driving?>
	</span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  12.0pt'><b><span style='font-size:10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  </td>
  <td width=338 valign=top style='width:253.4pt;border:solid black 1.0pt;
  border-left:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><b><span style='font-size:
  10.0pt;font-family:"Times New Roman","serif"'>Арендатор</span></b></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><b><span style='font-size:
  10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><b><span style='font-size:
  10.0pt;font-family:"Times New Roman","serif"'>ООО «Мир ин Медиа»</span></b></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><b><span style='font-size:
  10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif"'>Юридический адрес:125040, г. Москва,</span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif"'>ул. Новая Башиловка,  д. 4</span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif"'>ОГРН 1137746732650 </span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif"'>ИНН 7714912968</span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif"'>КПП 77401001     </span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif"'>Р/с 40702810200200000455   </span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif"'>в ОАО &quot;СМП БАНК&quot;</span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif"'>К/с 30101810300000000503    </span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif"'>БИК 044583503</span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><b><span style='font-size:
  10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=338 valign=top style='width:253.4pt;border:solid black 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif"'>_______________________/<?=$people->name?>/</span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><b><span style='font-size:
  10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  </td>
  <td width=338 valign=top style='width:253.4pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif"'>Генеральный директор</span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif"'>_______________________/Игнаткин И.П./</span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><b><span style='font-size:
  10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  </td>
 </tr>
</table>


<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>Страница 4</span></p>

<br/>

<p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>Приложение № 1</span></p>

<p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>к Договору субаренды</span></p>

<p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>транспортного
средства без экипажа</span></p>

<p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>№ <?=$doc->num.'-'.$doc->day; ?> от <?= date('d.m.Y',time()); ?></span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal;text-autospace:none'><b><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>АКТ
ПРИЕМКИ-ПЕРЕДАЧИ</span></b></p>

<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;line-height:normal;text-autospace:none'><b><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>ТРАНСПОРТНОГО
СРЕДСТВА</span></b></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal;text-autospace:none'><span style='font-size:10.0pt;
font-family:"Times New Roman","serif"'>г.&nbsp;Москва                                
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<?= date('d.m.Y',time()); ?>

<br>
<br>
</span></p>

<p class=MsoNormal align=right style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>Общество с
ограниченной ответственностью «Мир ин Медиа», именуемое в дальнейшем
&quot;Арендатор&quot;, в лице Генерального директора Игнаткина Игоря Петровича,
действующего на основании Устава с одной стороны, и Гражданин </span><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>
<?=$people->name; ?>
,</span><span style='font-size:10.0pt;font-family:"Times New Roman","serif"'>
именуемый в дальнейшем &quot;Субарендатор</span><span style='font-size:10.0pt;
font-family:"Times New Roman","serif"'>&quot;, с другой стороны, составили
настоящий Акт приемки-передачи транспортного средства (далее - Акт) по <span
style='color:windowtext'>Договору</span></a> субаренды транспортного средства
без экипажа № <?=$doc->num.'-'.$doc->day; ?> от <?= date('d.m.Y',time()); ?> (далее - Договор) о
нижеследующем.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>1. В соответствии
с п. 1.1 Договора Арендатор предоставляет во временное владение и пользование
транспортное средство без оказания услуг по управлению им и его технической
эксплуатации:</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-family:"Times New Roman","serif"'>наименование (тип) легковой
седан,</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>марка, модель </span><span
lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif"'>
<?=$avto->marka?>
</span><span style='font-size:10.0pt;font-family:"Times New Roman","serif"'>,</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>категория  <?=$avto->category?>,</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>государственный
регистрационный знак  <?=$avto->znak?>,</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>идентификационный
номер (VIN)  </span><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif"'><?=$avto->vin?></span><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>,</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>год выпуска <?=$avto->god?>,</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>шасси (рама)
<?=$avto->rama?></span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>цвет <?=$avto->color?>,</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>мощность двигателя
78</span><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif"'><?=$avto->mosh?></span><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'> (кВт),</span></p>


<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>Согласованная
стоимость передаваемого Транспортного средства составляет 700 000 руб. <i>(указывается
цифрами и прописью)</i></span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>2. На основании
п. 1.2. Договора Арендатор передает, а Субарендатор принимает:</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>2.1. Документы:</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>- свидетельство о
регистрации <a name=Par29></a>транспортного средства № 
<?=$avto->ctc?>
, дата
выдачи 
 <?=date('d.m.Y',strtotime($avto->ctc_date));?>
<?//=$avto->ctc_date?>
, выдано г. Москва;- страховой полис
обязательного страхования гражданской ответственности владельцев транспортных
средств серия 
<?=$avto->osago_seria?>
 № 
<?=$avto->osago?>
, дата выдачи 

 <?=date('d.m.Y',strtotime($avto->osago_date));?>
<?//=$avto->osago_date?>
;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><a
name=Par32></a><span style='font-size:10.0pt;font-family:"Times New Roman","serif"'>-
иные документы:</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>_________________________________________________________________________________________________</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>2.2.
Дополнительно оборудование:</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>_____________________________________________________________________________________________________________________</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>3. Указанное
Транспортное средство осмотрено Арендатором. При осмотре выявлены следующие
повреждения: Без повреждений<i>.</i></span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>4. Настоящий Акт
составлен в 2 (Двух) экземплярах, по одному для Арендодателя и Арендатора.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>- иные документы:</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>________________________________________________________________________________________________________</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>2.2.
Дополнительно оборудование:</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal;text-autospace:none'><span style='font-size:10.0pt;
font-family:"Times New Roman","serif"'>            </span><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>3. Указанное Транспортное
средство осмотрено Субарендатором. При осмотре установлено:</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>3.1.
Характеристика технического состояния Транспортного средства: <?=$avto->teh_status?><i>(например, &quot;технически исправное, пригодное для
эксплуатации в целях, предусмотренных п. 1.6 Договора&quot;, при выявлении в
ходе осмотра Транспортного средства недостатков указывается их характеристика)</i>.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>3.2.
Характеристика внешнего состояния Транспортного средства: <?=$avto->view_status?> <i>(например,
&quot;без видимых повреждений&quot;, при выявлении в ходе осмотра Транспортного
средства недостатков внешнего вида (сколов, царапин и т.д.) указывается их
характеристика)</i>.</span></p>

<br/>
<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>Страница 5</span></p>

<br/>


<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>3.3. Комплектация
Транспортного средства: <?=$avto->complect?> <i>(указываются
наименования принадлежностей и дополнительного оборудования, а также их
идентификационные признаки: марка, модель, серийный номер и т.д. При отсутствии
дополнительного оборудования указывается, например, &quot;соответствует
заводской&quot;)</i>.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>3.4. Показания
одометра при передаче Транспортного средства Субарендатору: <?=$avto->km?> км.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>4. Настоящий Акт
составлен в 2 (Двух) экземплярах, по одному для Арендатора и Субарендатора.<br>
<br>
</span></p>

<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0
 style='border-collapse:collapse;border:none'>
 <tr>
  <td width=338 valign=top style='width:253.4pt;border:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><b><span style='font-size:
  10.0pt;font-family:"Times New Roman","serif"'>Субарендатор</span></b></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><b><span style='font-size:
  10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  12.0pt'><b><span style='font-size:10.0pt;font-family:"Times New Roman","serif"'>Ф.И.О.</span></b></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  12.0pt'><span style='font-size:10.0pt;font-family:"Times New Roman","serif"'>
  <?=$people->name?>
  </span><span style='font-size:10.0pt;font-family:"Times New Roman","serif"'>
  </span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  12.0pt'><span style='font-size:10.0pt;font-family:"Times New Roman","serif"'>Дата
  и место рождения: </span><span lang=EN-US style='font-size:10.0pt;
  font-family:"Times New Roman","serif"'>
  <?//= date('d.m.Y',time()); ?>
  <?=date('d.m.Y',strtotime($people->date_birth));?>  
  <?=$people->siti_birth?>
  </span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  12.0pt'><span style='font-size:10.0pt;font-family:"Times New Roman","serif"'>Паспорт:
  серия 
  <?=$people->seria_pasport?>
  </span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  12.0pt'><span style='font-size:10.0pt;font-family:"Times New Roman","serif"'>Выдан:
  <?// if(isset($people->registr)){echo date('d.m.Y',strtotime($people->registr));}?> 
   <?//=$people->registr?>
  </span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  12.0pt'><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif"'>
  
  <?echo date('d.m.Y',strtotime($people->create));?> 
  </span></p>
  
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  12.0pt'><span style='font-size:10.0pt;font-family:"Times New Roman","serif"'>Водительское
  удостоверение:</span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  12.0pt'><span style='font-size:10.0pt;font-family:"Times New Roman","serif"'>
    <?=$people->seria_driving?>
  </span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  12.0pt'><span style='font-size:10.0pt;font-family:"Times New Roman","serif"'>Выдан:
 <?=date('d.m.Y',strtotime($people->kem_driving));?> 
    <?//=$people->kem_driving?>
	</span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  12.0pt'><b><span style='font-size:10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  </td>
  <td width=338 valign=top style='width:253.4pt;border:solid black 1.0pt;
  border-left:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><b><span style='font-size:
  10.0pt;font-family:"Times New Roman","serif"'>Арендатор</span></b></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><b><span style='font-size:
  10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><b><span style='font-size:
  10.0pt;font-family:"Times New Roman","serif"'>ООО «Мир ин Медиа»</span></b></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><b><span style='font-size:
  10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif"'>Юридический адрес:125040, г. Москва,</span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif"'>ул. Новая Башиловка,  д. 4</span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif"'>ОГРН 1137746732650 </span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif"'>ИНН 7714912968</span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif"'>КПП 77401001     </span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif"'>Р/с 40702810200200000455   </span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif"'>в ОАО &quot;СМП БАНК&quot;</span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif"'>К/с 30101810300000000503    </span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif"'>БИК 044583503</span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><b><span style='font-size:
  10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=338 valign=top style='width:253.4pt;border:solid black 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif"'>_______________________/<?=$people->name?>/</span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><b><span style='font-size:
  10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  </td>
  <td width=338 valign=top style='width:253.4pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif"'>Генеральный директор</span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif"'>_______________________/Игнаткин И.П./</span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><b><span style='font-size:
  10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  </td>
 </tr>
</table>
<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;text-indent:27.0pt;line-height:normal;text-autospace:none'><b><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>

<br/>
<br/>
<br/>
<br/><br/>
<br/>
<br/>
<br/><br/>
<br/>
<br/>
<br/><br/>
<br/>
<br/>
<br/><br/>
<br/>
<br/>
<br/><br/>
<br/>
<br/>
<br/><br/>
<br/>
<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>Страница 6</span></p>

<br/>


<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;text-indent:27.0pt;line-height:normal;text-autospace:none'><b><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>АКТ ВОЗВРАТА</span></b></p>

<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
text-align:center;text-indent:27.0pt;line-height:normal;text-autospace:none'><b><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>ТРАНСПОРТНОГО
СРЕДСТВА</span></b></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal;text-autospace:none'><span style='font-size:10.0pt;
font-family:"Times New Roman","serif"'>г.&nbsp;Москва                                
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<?= date('d.m.Y',time()); ?>

<br>
<br>
</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>Общество с
ограниченной ответственностью «Мир ин Медиа», именуемое в дальнейшем
&quot;Арендатор&quot;, в лице Генерального директора Игнаткина Игоря Петровича,
действующего на основании Устава с одной стороны, и Гражданин <?=$people->name; ?>,
именуемый в дальнейшем &quot;Субарендатор&quot;, с другой стороны, составили
настоящий Акт возврата транспортного средства по <span
style='color:windowtext'>Договору</span></a> аренды транспортного средства без
экипажа № <?=$doc->num.'-'.$doc->day; ?> от <?= date('d.m.Y',time()); ?> (далее - Договор) о
нижеследующем.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>1. В
соответствии с п. 5.2 Договора Субарендатор возвращает транспортное средство:</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-family:"Times New Roman","serif"'>наименование (тип) легковой
седан,</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>марка, модель </span><span
lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif"'>
<?=$avto->marka?>
</span><span style='font-size:10.0pt;font-family:"Times New Roman","serif"'>,</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>категория  <?=$avto->category?>,</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>государственный
регистрационный знак  <?=$avto->znak?>,</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>идентификационный
номер (VIN)  </span><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif"'><?=$avto->vin?></span><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>,</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>год выпуска <?=$avto->god?>,</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>шасси (рама)
<?=$avto->rama?></span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>цвет <?=$avto->color?>,</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>мощность двигателя
78</span><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif"'><?=$avto->mosh?></span><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'> (кВт),</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>далее именуемое
&quot;Транспортное средство&quot;, полученное по Акту приемки-передачи
транспортного средства № <?=$doc->num.'-'.$doc->day; ?> от <?= date('d.m.Y',time()); ?> во временное
владение и пользование без оказания услуг по управлению им и его технической
эксплуатации, а Арендатор принимает Транспортное средство.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>2. Субарендатор
возвращает, а Арендатор принимает:</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>2.1. Документы:</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>- свидетельство о
регистрации <a name=Par29></a>транспортного средства № 
<?=$avto->ctc?>
, дата
выдачи 
 <?=date('d.m.Y',strtotime($avto->ctc_date));?>
<?//=$avto->ctc_date?>
, выдано г. Москва;- страховой полис
обязательного страхования гражданской ответственности владельцев транспортных
средств серия 
<?=$avto->osago_seria?>
 № 
<?=$avto->osago?>
, дата выдачи 

 <?=date('d.m.Y',strtotime($avto->osago_date));?>
<?//=$avto->osago_date?>
;</span></p>



<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>- иные
документы:</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>______________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>2.2.
Дополнительное оборудование:</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>______________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><a
name=Par35></a><span style='font-size:10.0pt;font-family:"Times New Roman","serif"'>3.
Указанное Транспортное средство возвращено Субарендатором в том состоянии и
комплектации, в котором оно было им получено, с учетом нормального износа.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><a
name=Par38></a><span style='font-size:10.0pt;font-family:"Times New Roman","serif"'>4.
Показания одометра при возврате Субарендатором Транспортного средства: <?=$avto->km?> км.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>5. Претензии к
фактическому состоянию возвращенного Транспортного средства: не имеются.</span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>6. Настоящий Акт
составлен в 2 (Двух) экземплярах, по одному для Арендатора и Субарендатора.</span></p>

<br/>
<br/>
<br/>
<br/><br/>
<br/><br/>
<br/><br/>
<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>Страница 7</span></p>

<br/>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><b><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>

<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0
 style='border-collapse:collapse;border:none'>
 <tr>
  <td width=338 valign=top style='width:253.4pt;border:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><b><span style='font-size:
  10.0pt;font-family:"Times New Roman","serif"'>Субарендатор</span></b></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><b><span style='font-size:
  10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  12.0pt'><b><span style='font-size:10.0pt;font-family:"Times New Roman","serif"'>Ф.И.О.</span></b></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  12.0pt'><span style='font-size:10.0pt;font-family:"Times New Roman","serif"'>
  <?=$people->name?>
  </span><span style='font-size:10.0pt;font-family:"Times New Roman","serif"'>
  </span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  12.0pt'><span style='font-size:10.0pt;font-family:"Times New Roman","serif"'>Дата
  и место рождения: </span><span lang=EN-US style='font-size:10.0pt;
  font-family:"Times New Roman","serif"'>
  <?//= date('d.m.Y',time()); ?>
  <?=date('d.m.Y',strtotime($people->date_birth));?>  
  <?=$people->siti_birth?>
  </span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  12.0pt'><span style='font-size:10.0pt;font-family:"Times New Roman","serif"'>Паспорт:
  серия 
  <?=$people->seria_pasport?>
  </span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  12.0pt'><span style='font-size:10.0pt;font-family:"Times New Roman","serif"'>Выдан:
  <?// if(isset($people->registr)){echo date('d.m.Y',strtotime($people->registr));}?> 
   <?=$people->registr?>
  </span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  12.0pt'><span lang=EN-US style='font-size:10.0pt;font-family:"Times New Roman","serif"'>
  
  <?echo date('d.m.Y',strtotime($people->create));?> 
  </span></p>
  
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  12.0pt'><span style='font-size:10.0pt;font-family:"Times New Roman","serif"'>Водительское
  удостоверение:</span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  12.0pt'><span style='font-size:10.0pt;font-family:"Times New Roman","serif"'>
    <?=$people->seria_driving?>
  </span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  12.0pt'><span style='font-size:10.0pt;font-family:"Times New Roman","serif"'>Выдан:
 <?=date('d.m.Y',strtotime($people->kem_driving));?> 
    <?//=$people->kem_driving?>
	</span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  12.0pt'><b><span style='font-size:10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  </td>
  <td width=338 valign=top style='width:253.4pt;border:solid black 1.0pt;
  border-left:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><b><span style='font-size:
  10.0pt;font-family:"Times New Roman","serif"'>Арендатор</span></b></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><b><span style='font-size:
  10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><b><span style='font-size:
  10.0pt;font-family:"Times New Roman","serif"'>ООО «Мир ин Медиа»</span></b></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><b><span style='font-size:
  10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif"'>Юридический адрес:125040, г. Москва,</span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif"'>ул. Новая Башиловка,  д. 4</span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif"'>ОГРН 1137746732650 </span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif"'>ИНН 7714912968</span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif"'>КПП 77401001     </span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif"'>Р/с 40702810200200000455   </span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif"'>в ОАО &quot;СМП БАНК&quot;</span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif"'>К/с 30101810300000000503    </span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif"'>БИК 044583503</span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><b><span style='font-size:
  10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=338 valign=top style='width:253.4pt;border:solid black 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif"'>&nbsp;</span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif"'>_______________________/<?=$people->name?>/</span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><b><span style='font-size:
  10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  </td>
  <td width=338 valign=top style='width:253.4pt;border-top:none;border-left:
  none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif"'>Генеральный директор</span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><span style='font-size:10.0pt;
  font-family:"Times New Roman","serif"'>_______________________/Игнаткин И.П./</span></p>
  <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
  justify;line-height:normal;text-autospace:none'><b><span style='font-size:
  10.0pt;font-family:"Times New Roman","serif"'>&nbsp;</span></b></p>
  </td>
 </tr>
</table>
<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal;text-autospace:none'>&nbsp;</p>

<br/>
<br/>
<br/>
<br/><br/>
<br/><br/>
<br/><br/>
<br/><br/>
<br/><br/>
<br/><br/>
<br/><br/>
<br/><br/>
<br/><br/>
<br/><br/>
<br/><br/>
<br/><br/>
<br/><br/>
<br/><br/>
<p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;text-indent:27.0pt;line-height:normal;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif"'>Страница 8</span></p>

<br/>
</div>

</body>

</html>
