<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ClientSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Клиенты';
$this->params['breadcrumbs'][] = $this->title;

$gridColumns = [
    ['class' => 'yii\grid\SerialColumn'],
    // 'id',
    [
        'attribute' => 'contract_number',
        'label' => 'Номер договора',
        'value' => 'contract.number',
    ],
    [
        'attribute' => 'contract.date_created',
        'format' => 'date',
        'label' => 'Дата заключения договора',
        'value' => 'contract.date_created',
    ],
    [
        'attribute' => 'name',
        'content' => function($data){
            return Html::a($data->name, ['view','id' =>$data->id]);
        },
    ],

    [
        'attribute' => 'office.name',
        'header' => 'Офис',
    ],
    [
        'attribute' => 'contract.status',
        'header' => 'Статус',
    ],
    // 'date_birth',
    // 'place_birth:ntext',
    // 'passport:ntext',
    // 'registration_address:ntext',
    // 'current_address:ntext',
    // 'phone:ntext',
    // 'alt_phone:ntext',
    // 'WorkPlace:ntext',
    ['class' => 'yii\grid\ActionColumn',
        'template' => '{delete} ', 'visible' => Yii::$app->user->identity->isAdmin() ? true : false],
    // ['class' => 'yii\grid\ActionColumn'],
];
?>
<div class="client-index">
	<div class="box box-default">	
		<div class="box-body">
		<!-- <img src="http://cbscao.ru/sites/default/files/pictures/13249/develop900.jpg" alt="Наш логотип">-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
   </p>
	
		</div>
	</div>
	</div>
	
	<div class="box box-default">	
		<div class="box-body" style="overflow-x: auto;">
    <?php Pjax::begin() ?>
    <?php echo ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'filename' => 'Клиенты '.date('d.m.Y'),
    ]); ?>
	<?= GridView::widget([
        'tableOptions' => ['id' => 'table-clients', 'class' => 'table table-clients table-striped table-bordered nowrap dataTable dtr-inline', 'style' => 'border-bottom: transparent;'],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns,
    ]); ?>
    <?php Pjax::end() ?>
	</div>
</div>
</div>
<?php   

$script = <<< JS

jQuery('#table-clients').DataTable({
    "paging":   false,
    "searching": false,
    "info":     false
});

JS;

$this->registerJs($script, yii\web\View::POS_READY);

 ?>
