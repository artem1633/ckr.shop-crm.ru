<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

use app\services\YandexDisk;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Client */

$this->title = 'Изменить клиента: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Клиенты', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<style>
    table tr th {
        font-size: 12px;
    }
</style>
<script>
	function addCreditor(id)
	{
		var modal = $('.modal');
		$.get('/client/createcreditor',{'client_id':id}, function(data) {
			modal.html(data).modal('show');
		});
		return false;
	}
	function addDocument(id)
	{
		var modal = $('.modal');
		$.get('/client/createdocument',{'client_id':id}, function(data) {
			modal.html(data).modal('show');
		});
		return false;
	}
	function addCreditorDocument(id)
	{
		var modal = $('.modal');
		$.get('/creditor/createdocument',{'creditor_id':id}, function(data) {
			modal.html(data).modal('show');
		});
		return false;
	}
	function addPayment(id)
	{
		var modal = $('.modal');
		$.get('/client/createpayment',{'client_id':id}, function(data) {
			modal.html(data).modal('show');
		});
		return false;
	}
	function addContract(id)
	{
		var modal = $('.modal');
		$.get('/client/createcontract',{'client_id':id}, function(data) {
			modal.html(data).modal('show');
		});
		return false;
	}
</script>
<div class="client-update">


    <?= $this->render('_form', [
        'model' => $model,
        'contract' => $contract,
    ]) ?>

	<div class="box box-default" style="overflow: scroll;">
		<div class="box-body">
		  <ul class="nav nav-tabs">
		    <li class="active"><a data-toggle="tab" href="#home">Кредиторы</a></li>
		    <li><a data-toggle="tab" href="#menu1">Документы</a></li>
		    <li><a data-toggle="tab" href="#menu2">График платежей</a></li>
		    <!-- <li><a data-toggle="tab" href="#menu3">Договоры</a></li> -->
		  </ul>

		  <div class="tab-content">
		    <div id="home" class="tab-pane fade in active">
		    	<br>
		    	<button href="<?=Url::toRoute(['client/createcreditor', 'client_id' => $model->id])?>" class="btn btn-success" onClick="addCreditor(<?=$model->id?>)">Добавить кредитора</button>
                <a href="<?=Url::toRoute(['client/create-act', 'client_id' => $model->id])?>" class="btn btn-info" style="margin-left: 5px" target="_blank">Сформировать акт</a>
		    	<hr>
                <?= GridView::widget([
                    'dataProvider' => $creditorDataProvider,
                    'filterModel' => $creditorSearchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        // 'id',
                        [
                            'attribute' => 'name',
                            'header' => 'Кредитор',
                            'headerOptions' => ['style' => 'font-size: 13px'],
                            'contentOptions'=>['style'=>'max-width: 100px; width: 100px; word-wrap:break-word;'],
                            'content' => function($data) {
                                return Html::a($data->name, ['updatecreditor','id' =>$data->id], ['onClick' => "
                                        var modal = $('.modal');
                                        $.get('/client/updatecreditor',{'id':".$data->id."}, function(data) {
                                            modal.html(data).modal('show');
                                        });
                                        return false;
                                    "]);
                            },
                        ],
                        [
                            'attribute' => 'number',
                            'contentOptions'=>['style'=>'width: 100px;'],
                        ],

						[
							'attribute' => 'date_confirmed',
							'format' => 'date',
							'contentOptions'=>['style'=>'max-width: 100px; width: 100px; word-wrap:break-word; font-size: 12px;'],
							'filter' => DatePicker::widget([
								'model' => $creditorSearchModel,
								'attribute' => 'date_confirmed',
								'template' => '{addon}{input}',
								'clientOptions' => [
									'autoclose' => true,
									'format' => 'dd.mm.yyyy'
								]
							])
						],

                        [
                            'header' => 'Документы кредиторов',
                            'content' => function($data) use ($creditorsDocumentsData) {
                                return '        <button class="btn btn-success" onclick="addCreditorDocument('.$data->id.')">Добавить документ</button>
                                    <br>'.GridView::widget([
                                        'dataProvider' => $creditorsDocumentsData[$data->id]['dataProvider'],
                                        // 'filterModel' => $creditorsDocumentsData[$data->id]['searchModel'],
                                        'columns' => [
                                            //['class' => 'yii\grid\SerialColumn'],
                                            // [
                                            // 'attribute'=>'id',
                                            // 'content'=>function ($data){
                                            //  return '<span class="label label-warning" style="font-size:13px;">'.Html::a($data->id.'   ', ['documents-creditors/update','id' =>$data->id], ['style'=>'color:#ffffff;', 'onClick' => "
                                            //          var modal = $('.modal');
                                            //          $.get('/creditor/updatedocument',{'id':".$data->id."}, function(data) {
                                            //              modal.html(data).modal('show');
                                            //          });
                                            //          return false;
                                            //      "]).'</span>';

                                            //  },
                                            // ],

                                            // 'id',
                                            [
                                                'attribute' => 'name',
                                                'content' => function($data) {
                                                    return Html::a($data->name, ['documents-creditors/update','id' =>$data->id], ['onClick' => "
                                                    var modal = $('.modal');
                                                    $.get('/creditor/updatedocument',{'id':".$data->id."}, function(data) {
                                                        modal.html(data).modal('show');
                                                    });
                                                    return false;
                                                "]);
                                                },
                                            ],
                                            // 'contract.number',
                                            // 'creditor.name',
                                            [
                                                'header' => 'Скан копия',
                                                'content' => function($data) {
                                                    if($data->scan != '')
                                                    {
                                                        $out = YandexDisk::download($data->scan);
                                                        if(isset($out->href))
                                                        {
                                                            return "<a href='".$out->href."' style='text-align: center;'><i style='color: #23a825' class='fa fa-arrow-circle-down fa-lg'></i></a>";
                                                        } else {
                                                            return "<a style='text-align: center;'><i style='color: #b6b6b6' class='fa fa-arrow-circle-down fa-lg'></i></a>";
                                                        }
                                                    } else {
                                                        return "<a style='text-align: center;'><i style='color: #b6b6b6' class='fa fa-arrow-circle-down fa-lg'></i></a>";
                                                    }
                                                }
                                            ],
                                            'date_created',
                                            'comment:ntext',
                                            ['class' => 'yii\grid\ActionColumn',
                                                'template' => '{delete}',
                                                'buttons' => [
                                                    'delete' => function ($url, $model, $key){
                                                        return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                                                            ['creditor/deletedocument','id'=>$key],
                                                            [
                                                                'title'=>'Удалить',
                                                                'aria-label'=>'Удалить',
                                                                'data-confirm'=>'Вы уверены, что хотите удалить этот элемент?',
                                                                'data-method'=>'post',
                                                            ]);

                                                    },

                                                ],
                                            ],
                                            // ['class' => 'yii\grid\ActionColumn'],
                                        ],
                                    ]);
                            },
                        ],
                        [
                            'attribute' => 'comment',
                            'contentOptions'=>['style'=>'max-width: 198px; width: 169px; word-wrap:break-word'],
                        ],
                        // 'registration_address:ntext',
                        // 'current_address:ntext',
                        // 'phone:ntext',
                        // 'alt_phone:ntext',
                        // 'WorkPlace:ntext',
                        ['class' => 'yii\grid\ActionColumn',
                            'template' => '{delete}',
                            'buttons' => [
                                'delete' => function ($url, $model, $key){
                                    return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                                        ['deletecreditor','id'=>$key],
                                        [
                                            'title'=>'Удалить',
                                            'aria-label'=>'Удалить',
                                            'data-confirm'=>'Вы уверены, что хотите удалить этот элемент?',
                                            'data-method'=>'post',
                                        ]);

                                },

                            ],
                        ],
                        // ['class' => 'yii\grid\ActionColumn'],
                    ],
                ]); ?>
		    </div>
		    <div id="menu1" class="tab-pane fade">
		    	<br>
		    	<button class="btn btn-success" onclick="addDocument(<?=$model->id?>)">Добавить документ</button>
		    	<hr>
				<?= GridView::widget([
			        'dataProvider' => $documentsDataProvider,
			        'filterModel' => $documentsSearchModel,
			        'columns' => [
			            ['class' => 'yii\grid\SerialColumn'],

			            // 'id',
			            [
			            	'attribute' => 'name',
			            	'content' => function($data) {
								return Html::a($data->name, ['documents/update','id' =>$data->id], ['onClick' => "
										var modal = $('.modal');
										$.get('/client/updatedocument',{'id':".$data->id."}, function(data) {
											modal.html(data).modal('show');
										});
										return false;
									"]);
			            	}
			            ],
			            [
			            	'header' => 'Файл',
			            	'content' => function($data) {
			            		if($data->scan != '')
			            		{
			            			$out = YandexDisk::download($data->scan);
			            			if(isset($out->href))
			            			{
			            				return "<a href='".$out->href."' style='text-align: center;'><i style='color: #23a825' class='fa fa-arrow-circle-down fa-lg'></i></a>";
			            			} else {
			            				return "<a style='text-align: center;'><i style='color: #b6b6b6' class='fa fa-arrow-circle-down fa-lg'></i></a>";
			            			}
			            		} else {
			            			return "<a style='text-align: center;'><i style='color: #b6b6b6' class='fa fa-arrow-circle-down fa-lg'></i></a>";
			            		}
			            	}
			            ],
			            'date_created',
			            'comment',
			            // 'registration_address:ntext',
			            // 'current_address:ntext',
			            // 'phone:ntext',
			            // 'alt_phone:ntext',
			            // 'WorkPlace:ntext',
						['class' => 'yii\grid\ActionColumn',
								'template' => '{delete}',
								'buttons' => [
								'delete' => function ($url, $model, $key){
										return Html::a('<span class="glyphicon glyphicon-trash"></span>',
											['deletedocument','id'=>$key],
											[
											'title'=>'Удалить',
											'aria-label'=>'Удалить',
											'data-confirm'=>'Вы уверены, что хотите удалить этот элемент?',
											'data-method'=>'post',
											]);

									},

								],
							],
			           // ['class' => 'yii\grid\ActionColumn'],
			        ],
			    ]); ?>
		    </div>
		    <div id="menu2" class="tab-pane fade">
		    	<br>
		    	<button class="btn btn-success" onclick="addPayment(<?=$model->id?>)">Добавить платеж</button>
		    	<hr>
				<?= GridView::widget([
			        'dataProvider' => $paymentsDataProvider,
			        'filterModel' => $paymentsSearchModel,
			        'columns' => [
			            ['class' => 'yii\grid\SerialColumn'],
						[
						'attribute'=>'id',
						'content'=>function ($data){                 
							return Html::a($data->id.'   ', ['payment/update','id' =>$data->id], ['onClick' => "
									var modal = $('.modal');
									$.get('/client/updatepayment',{'id':".$data->id."}, function(data) {
										modal.html(data).modal('show');
									});
									return false;
								"]);
									 
						 },
						],
			            'date',

			            // 'id',
			            'name',
			            [
			                'attribute' => 'price',
			                'contentOptions'=>['style'=>'width: 103px;'],
			            ],
			            [
			            	'attribute' => 'receipt_number',
			            	'contentOptions'=>['style'=>'width: 103px;'],
			            ],
			            // 'registration_address:ntext',
			            // 'current_address:ntext',
			            // 'phone:ntext',
			            // 'alt_phone:ntext',
			            // 'WorkPlace:ntext',
                        [
                            'content' => function($data){
				                return "<a href='".Url::toRoute(['client/create-bso', 'paymentId' => $data->id])."' target='_blank'><i class='fa fa-file'></i></a>";
                            }
                        ],
						['class' => 'yii\grid\ActionColumn',
								'template' => '{delete}',
								'visible' => Yii::$app->user->identity->isAdmin() ? true : false,
								'buttons' => [
								'delete' => function ($url, $model, $key){
										return Html::a('<span class="glyphicon glyphicon-trash"></span>', 
											['deletepayment','id'=>$key],
											[
											'title'=>'Удалить',
											'aria-label'=>'Удалить',
											'data-confirm'=>'Вы уверены, что хотите удалить этот элемент?',
											'data-method'=>'post',
                                            ]);
										
									},
								],
							],
			           // ['class' => 'yii\grid\ActionColumn'],
			        ],
			    ]); ?>
		    </div>
<!-- 		    <div id="menu3" class="tab-pane fade">
		    	<br>
		    	<button class="btn btn-success" onclick="addContract(<?=$model->id?>)">Добавить контракт</button>
		    	<hr>
				<?= GridView::widget([
			        'dataProvider' => $contractsDataProvider,
			        'filterModel' => $contractsSearchModel,
			        'columns' => [
			            ['class' => 'yii\grid\SerialColumn'],

			            // 'id',
			            [
			            	'attribute' => 'number',
			            	'content' => function($data){
								return Html::a($data->number.'   ', ['payment/update','id' =>$data->id], ['onClick' => "
										var modal = $('.modal');
										$.get('/client/updatecontract',{'id':".$data->id."}, function(data) {
											modal.html(data).modal('show');
										});
										return false;
									"]);
			            	},
			            ],
			            [
			            	'attribute' => 'client.name',
			            	'header' => 'ФИО клиента',
			            ],
			            'price',
			            // 'registration_address:ntext',
			            // 'current_address:ntext',
			            // 'phone:ntext',
			            // 'alt_phone:ntext',
			            // 'WorkPlace:ntext',
						['class' => 'yii\grid\ActionColumn',
								'template' => '{delete}',
								'buttons' => [
								'delete' => function ($url, $model, $key){
										return Html::a('<span class="glyphicon glyphicon-trash"></span>', 
											['deletecontract','id'=>$key],
											[
											'title'=>'Удалить',
											'aria-label'=>'Удалить',
											'data-confirm'=>'Вы уверены, что хотите удалить этот элемент?',
											'data-method'=>'post',
											]);
										
									},
									
								],
							],
			           // ['class' => 'yii\grid\ActionColumn'],
			        ],
			    ]); ?>
		    </div> -->
		  </div>
		</div>
	</div>

</div>

<div class="row">
	<div class="modal comment">
		
	</div>
</div>