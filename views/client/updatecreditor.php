<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Creditor */

$this->title = 'Изменить кредитора: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Кредиторы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';

?>
<div class="creditor-update" style="margin: auto; width: 50%;">


    <?= $this->render('_formcreditor', [
        'model' => $model,
        'contracts' => $contracts,
        'contract' => $contract,
    ]) ?>



</div>

<!-- <div class="box box-default">
	<div class="box-body">
		<h3>Документы по кредиторам</h3>
		<a href="<?=Url::toRoute(['documents-creditors/create', 'creditor_id' => $model->id])?>" class="btn btn-success">Добавить документ</a>
		<br>
		<?= GridView::widget([
	        'dataProvider' => $documentsDataProvider,
	        'filterModel' => $documentsSearchModel,
	        'columns' => [
	            //['class' => 'yii\grid\SerialColumn'],
				[
				'attribute'=>'id',
				'content'=>function ($data){                 
					return '<span class="label label-warning" style="font-size:13px;">'.Html::a($data->id.'   ', ['documents-creditors/update','id' =>$data->id], ['style'=>'color:#ffffff;']).'</span>';
							 
				 },
				],

	            // 'id',
	            'creditor.name',
	            'contract.number',
	            'name',
	            'scan',
	            // 'comment:ntext',
				['class' => 'yii\grid\ActionColumn',
				'template' => '{delete} '],
	           // ['class' => 'yii\grid\ActionColumn'],
	        ],
	    ]); ?>
	</div>
</div> -->

