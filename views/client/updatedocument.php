<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Documents */

$this->title = 'Изменить ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Документы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="documents-update" style="margin: auto; width: 50%;">


    <?= $this->render('_formdocument', [
        'model' => $model,
        'contracts' => $contracts,
        'contract' => $contract,
    ]) ?>

</div>
