<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Contract */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contract-form">
	<div class="box box-default">
		<div class="box-body">
			<?php $form = ActiveForm::begin(); ?>

   		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'number')->textInput() ?>
			</div>
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'date_created')->textInput() ?>				
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'price')->textInput() ?>				
			</div>
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'rate')->textInput() ?>				
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'status')->dropDownList([
				    	'Активный' => 'Активный',
				    	'Завершенный' => 'Завершенный',
				    	'Архив' => 'Архив',
				    ]) ?>				
			</div>
			<div class="col-md-4 vcenter">
				    <?= $form->field($client, 'name')->textInput() ?>				
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($client, 'date_birth')->widget(\yii\widgets\MaskedInput::className(), [
    					'mask' => '99.99.9999',
					]) ?>				
			</div>
			<div class="col-md-4 vcenter">
				    <?= $form->field($client, 'place_birth')->textarea(['rows' => 6]) ?>				
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($client, 'passport')->textarea(['rows' => 6]) ?>				
			</div>
			<div class="col-md-4 vcenter">
				    <?= $form->field($client, 'registration_address')->textarea(['rows' => 6]) ?>				
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($client, 'alt_phone')->widget(\yii\widgets\MaskedInput::className(), [
    					'mask' => '+7(999) 999-99-99',
					]) ?>				
			</div>
			<div class="col-md-4 vcenter">
				    <?= $form->field($client, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
    					'mask' => '+7(999) 999-99-99',
					]) ?>				
			</div>
		</div>
   		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($client, 'current_address')->textarea(['rows' => 6]) ?>				
			</div>
			<div class="col-md-4 vcenter">
				    <?= $form->field($client, 'WorkPlace')->textarea(['rows' => 6]) ?>				
			</div>
		</div>
   		<div class="row">
			<div class="col-md-8 vcenter">
				    <?= $form->field($client, 'comments')->textarea(['rows' => 6]) ?>				
			</div>
		</div>
   	
<div style="display:none">
</div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Сформировать договор' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

		</div>
	</div>
</div>
