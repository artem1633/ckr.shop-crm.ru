<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Contract */

$this->title = 'Создать новый договор';
$this->params['breadcrumbs'][] = ['label' => 'Договора', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contract-create">


    <?= $this->render('_form', [
        'model' => $model,
        'client' => $client,
    ]) ?>

</div>
