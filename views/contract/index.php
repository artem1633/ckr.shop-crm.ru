<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ContractSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Договора';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contract-index">
	<div class="box box-default">	
		<div class="box-body">
		<!-- <img src="http://cbscao.ru/sites/default/files/pictures/13249/develop900.jpg" alt="Наш логотип">-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
   </p>
	
		</div>
	</div>
	</div>
	
	<div class="box box-default">	
		<div class="box-body" style="overflow-x: auto;">    
	<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
			[
			'attribute'=>'id',
			'content'=>function ($data){                 
				return '<span class="label label-warning" style="font-size:13px;">'.Html::a($data->id.'   ', ['update','id' =>$data->id], ['style'=>'color:#ffffff;']).'</span>';
						 
			 },
			],

            // 'id',
            'number',
            'date_created',
            'price',
            'status',
            // 'rate',
			['class' => 'yii\grid\ActionColumn',
			'template' => '{delete} '],
           // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
	</div>
</div>
</div>
