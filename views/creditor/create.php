<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Creditor */

$this->title = 'Создать кредитора';
$this->params['breadcrumbs'][] = ['label' => 'Кредиторы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="creditor-create">


    <?= $this->render('_form', [
        'model' => $model,
        'client_id' => $client_id,
        'contracts' => $contracts,
    ]) ?>

</div>
