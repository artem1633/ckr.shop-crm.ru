<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DocumentsCreditors */

$this->title = 'Добавить ';
$this->params['breadcrumbs'][] = ['label' => 'Documents Creditors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="documents-creditors-create" style="margin: auto; width: 50%;">


    <?= $this->render('_formdocument', [
        'model' => $model,
        'creditor_id' => $creditor_id,
        'contracts' => $contracts,
        'contract' => $contract,
    ]) ?>

</div>
