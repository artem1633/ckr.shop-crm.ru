<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

use app\services\YandexDisk;

/* @var $this yii\web\View */
/* @var $model app\models\Creditor */

$this->title = 'Изменить кредитора: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Кредиторы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<script>
	function addCreditorDocument(id)
	{
		var modal = $('.modal');
		$.get('/creditor/createdocument',{'creditor_id':id}, function(data) {
			modal.html(data).modal('show');
		});
		return false;
	}
</script>
<div class="creditor-update">


    <?= $this->render('_form', [
        'model' => $model,
        'contracts' => $contracts,
    ]) ?>

</div>

<div class="box box-default">
	<div class="box-body">
		<h3>Документы по кредиторам</h3>
		<button class="btn btn-success" onclick="addCreditorDocument(<?=$model->id?>)">Добавить документ</button>
		<br>
		<?= GridView::widget([
	        'dataProvider' => $documentsDataProvider,
	        'filterModel' => $documentsSearchModel,
	        'columns' => [
	            //['class' => 'yii\grid\SerialColumn'],
				// [
				// 'attribute'=>'id',
				// 'content'=>function ($data){                 
				// 	return '<span class="label label-warning" style="font-size:13px;">'.Html::a($data->id.'   ', ['documents-creditors/update','id' =>$data->id], ['style'=>'color:#ffffff;', 'onClick' => "
				// 			var modal = $('.modal');
				// 			$.get('/creditor/updatedocument',{'id':".$data->id."}, function(data) {
				// 				modal.html(data).modal('show');
				// 			});
				// 			return false;
				// 		"]).'</span>';
							 
				//  },
				// ],

	            // 'id',
	            [
	            	'attribute' => 'name',
	            	'content' => function($data) {
						return Html::a($data->name, ['documents-creditors/update','id' =>$data->id], ['onClick' => "
								var modal = $('.modal');
								$.get('/creditor/updatedocument',{'id':".$data->id."}, function(data) {
									modal.html(data).modal('show');
								});
								return false;
							"]);
	            	},
	            ],
	            'contract.number',
	            'creditor.name',
			    [
			        'header' => 'Скан копия',
			        'content' => function($data) {
			        	if($data->scan != '')
			        	{
			            	$out = YandexDisk::download($data->scan);
			            	if(isset($out->href))
			            	{
			            		return "<a href='".$out->href."' style='text-align: center;'><i style='color: #23a825' class='fa fa-arrow-circle-down fa-lg'></i></a>";
			            	} else {
			            		return "<a style='text-align: center;'><i style='color: #b6b6b6' class='fa fa-arrow-circle-down fa-lg'></i></a>";
			            	}
			            } else {
			            	return "<a style='text-align: center;'><i style='color: #b6b6b6' class='fa fa-arrow-circle-down fa-lg'></i></a>";
			            }
			        }
			            ],
	            // 'comment:ntext',
				['class' => 'yii\grid\ActionColumn',
						'template' => '{delete}',
						'buttons' => [
						'delete' => function ($url, $model, $key){
								return Html::a('<span class="glyphicon glyphicon-trash"></span>', 
									['deletedocument','id'=>$key],
									[
									'title'=>'Удалить',
									'aria-label'=>'Удалить',
									'data-confirm'=>'Вы уверены, что хотите удалить этот элемент?',
									'data-method'=>'post',
									]);
								
							},
							
						],
					],
	           // ['class' => 'yii\grid\ActionColumn'],
	        ],
	    ]); ?>
	</div>
</div>

<div class="row">
	<div class="modal comment">
		
	</div>
</div>