<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">
	<div class="box box-default">
		<div class="box-body">
			<?php $form = ActiveForm::begin(); ?>

            <div class="row">
                <div class="col-md-4 vcenter">
                    <?= $form->field($model, 'title')->textInput() ?>
                </div>
            </div>

		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'description')->textInput() ?>
			</div>
		</div>

            <div class="row">
                <div class="col-md-4 vcenter">
                    <?= $form->field($model, 'created_at')->widget(\yii\widgets\MaskedInput::className(), [
                        'mask' => '99.99.9999',
                    ]) ?>
                </div>
            </div>



   	
<div style="display:none">
</div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

		</div>
	</div>
</div>
