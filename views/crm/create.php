<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'Добавить новую запись срм';
$this->params['breadcrumbs'][] = ['label' => 'Срм', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crm-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
