<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Срм';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">
	<div class="box box-default">	
		<div class="box-body">
		<!-- <img src="http://cbscao.ru/sites/default/files/pictures/13249/develop900.jpg" alt="Наш логотип">-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
   </p>
	
		</div>
	</div>
	</div>
	
	<div class="box box-default">	
		<div class="box-body" style="overflow-x: auto;">    
	<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'title',
                'value' => 'title',
                'label' => 'Заголовок'
            ],
            // 'id',
            [
                'attribute' => 'description',
                'content' => function($data) {
                    return Html::a($data->description, ['update','id' =>$data->id]);
                },
            ],

            [
                'attribute' => 'created_at',
				'format' => 'date',
                'value' => 'created_at',
                'label' => 'Дата создания'
            ],

//            'first_name:ntext',
//            'last_name:ntext',
//            'middle_name:ntext',

            // 'auth_key',
            // 'password_hash',
            // 'password',
            // 'status',
            // 'category_id',
            // 'office_id',
            // 'phone:ntext',
            // 'date',
			['class' => 'yii\grid\ActionColumn',
			    'template' => '{delete}',
                'contentOptions'=> ['style'=>'max-width: 100px; width: 100px;'],
            ],
           // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
	</div>
</div>
</div>
