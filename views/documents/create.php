<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Documents */

$this->title = 'Создать новый документ';
$this->params['breadcrumbs'][] = ['label' => 'Документы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="documents-create">


    <?= $this->render('_form', [
        'model' => $model,
        'client_id' => $client_id,
        'contracts' => $contracts,
    ]) ?>

</div>
