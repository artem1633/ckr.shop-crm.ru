<?php

use kartik\time\TimePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Events */
/* @var $form yii\widgets\ActiveForm */
?>


			<?php $form = ActiveForm::begin(); ?>

   		<div class="row">
			<div class="col-md-12 vcenter">
				    <?= $form->field($model, 'time')->widget(TimePicker::className(), [
                        'name' => 't1',
                        'pluginOptions' => [
                            'showSeconds' => false,
                            'showMeridian' => false,
                            'minuteStep' => 1,
                            'secondStep' => 5,
                        ]
                    ]) ?>
			</div>
		</div>
   		<div class="row">
			<div class="col-md-12 vcenter">
				    <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>				
			</div>
		</div>
   		<div class="row">
			<div class="col-md-12 vcenter">
				    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>				
			</div>
		</div>
   	
<div style="display:none">
</div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>


