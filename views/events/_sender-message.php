<?php
/**
 * Created by PhpStorm.
 * User: Qist
 * Date: 15.04.2017
 * Time: 12:23
 */

?>

<?php if($result) {?>
    <div class="alert alert-success alert-dismissible" style="width: 75%;">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Успех!</h4>
        Все пользователи были успешно оповещены о событии
    </div>
<?php } else { ?>
    <div class="alert alert-danger alert-dismissible" style="width: 75%;">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Ошибка!</h4>
        Во время отправки, какому-то из пользователей произошла критическая ошибка. Свяжитесь с разработчиками CRM!
    </div>
<?php } ?>

