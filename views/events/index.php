<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EventsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$dateParam = Yii::$app->request->get('date');

if($dateParam)
{
    $date = new \DateTime($dateParam);
} else {
    $date = new \DateTime();
}

$yesterday = $date->sub(new \DateInterval('P1D'))->format('d.m.Y');
$today = $date->add(new \DateInterval('P1D'))->format('d.m.Y');
$tomorrow = $date->add(new \DateInterval('P1D'))->format('d.m.Y');

$this->title = $today;
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="events-index">
	<div class="box box-default">	
		<div class="box-body">
		<!-- <img src="http://cbscao.ru/sites/default/files/pictures/13249/develop900.jpg" alt="Наш логотип">-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <?php Pjax::begin() ?>
    <p>
        <?= Html::button('Добавить', ['value' => Url::toRoute(['events/create', 'calendarId' => $calendarId, 'calendarDate' => $today]), 'class' => 'btn btn-success', 'id' => 'button-modal_create', 'onClick' => "
           $('#modal-create').modal('show')
            .find('#modal-create_content')
            .load($(this).attr('value'));
        "]) ?>
   </p>

		</div>
	</div>
	</div>

    <?php
        Modal::begin([
            'header' => '<h4>Добавить/изменить запись</h4>',
            'id' => 'modal-create',
            'size' => 'modal-lg'
        ]);

        echo "<div id=\"modal-create_content\"></div>";

        Modal::end();
    ?>

	<div class="box box-default">
		<div class="box-body" style="overflow-x: auto;">
            <div class="btn-group">
                <a href="<?=Url::toRoute(['events/index', 'date' => $yesterday])?>" type="button" class="btn btn-default btn-md" title="<?=$yesterday?>"><i class="fa fa-chevron-left"></i></a>
                <a href="<?=Url::toRoute(['events/index', 'date' => $tomorrow])?>" type="button" class="btn btn-default btn-md" title="<?=$tomorrow?>"><i class="fa fa-chevron-right"></i></a>
            </div>
            <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
			[
			'attribute'=>'time',
			'content'=>function ($data) use ($today){
				return Html::button($data->time.'   ', ['value' => Url::toRoute(['events/update', 'id' => $data->id, 'calendarDate' => $today]), 'class' => 'button-modal_update btn btn-warning btn-xs', 'style'=>'color:#ffffff; font-size: 13px', 'onClick' => "
				   $('#modal-create').modal('show')
                        .find('#modal-create_content')
                        .load($(this).attr('value'));
				"]).'<a href="'.Url::toRoute(['events/notify', 'id' => $data->id]).'" class="btn btn-info btn-xs btn-calendar-notify" title="оповестить всех о событии"><i class="fa fa-bell"></i></a>';
						 
			 },
                'contentOptions' => ['style' => 'width: 100px'],
			],

            // 'id',
            'content:ntext',
            'comment:ntext',
			['class' => 'yii\grid\ActionColumn',
			'template' => '{delete} ',
            'contentOptions' => ['style' => 'width: 10px']],
           // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
	</div>
        <?php Pjax::end() ?>
</div>
</div>
