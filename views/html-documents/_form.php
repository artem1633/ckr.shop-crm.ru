<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use skeeks\yii2\ckeditor\CKEditorWidget;
use skeeks\yii2\ckeditor\CKEditorPresets;

/* @var $this yii\web\View */
/* @var $model app\models\HtmlDocuments */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="html-documents-form">
	<div class="box box-default">
		<div class="box-body">
			<?php $form = ActiveForm::begin(); ?>

		<div class="row">
			<div class="col-md-4 vcenter">
				    <?= $form->field($model, 'name_ru')->textInput(['maxlength' => true]) ?>
			</div>
		</div>
   		<div class="row">
			<div class="col-md-12 vcenter">
				    <?= $form->field($model, 'content')->widget(CKEditorWidget::className(), [
                        'options' => ['rows' => 6],
                        'preset' => CKEditorPresets::FULL
                    ]) ?>
			</div>
		</div>
   	
<div style="display:none">
</div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

		</div>
	</div>
</div>
