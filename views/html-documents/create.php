<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\HtmlDocuments */

$this->title = 'Добавить ';
$this->params['breadcrumbs'][] = ['label' => 'Html Documents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="html-documents-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
