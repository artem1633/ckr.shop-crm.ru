<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\HtmlDocuments */

$this->title = 'Изменить документ «' . $model->name_ru.'»';
$this->params['breadcrumbs'][] = ['label' => 'Документы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="html-documents-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
