<?php
use app\models\Tasks;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Office;
use dmstr\widgets\Alert;
use app\models\Notifications;

/* @var $this \yii\web\View */
/* @var $content string */

$userId = Yii::$app->user->identity->id;

$tasksCount = Tasks::find()->where(['performer_id' => $userId, 'date_close' => null])->count();

$offices = Office::find()->all();

$date = new DateTime();
$interval = new DateInterval('P1M');
$lastMonth = $date->sub($interval);

?>

<header class="main-header">


    <a class="logo" href="/" style="background-image: url(/img/logo.jpg);background-size: 50%;background-position: center;background-repeat: no-repeat;"><span class="logo-mini"></span><span class="logo-lg"></span></a>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">

                    <?= $this->render('offices', [
                        'offices' => $offices,
                        'date' => $date,
                        'interval' => $interval,
//                        'lastMonth' => $lastMonth,
                    ]) ?>

                    <li class="dropdown notifications-menu">
                        <a href="#" title="Количество задач" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                            <i class="fa fa-list-ul"></i>
                            <?php if($tasksCount != 0): ?>
                                <span class="label label-warning" style="font-size: 14px"><?=$tasksCount?></span>
                            <?php endif; ?>
                        </a>
                    </li>

                    <!-- User Account: style can be found in dropdown.less -->
                    <li>
                        <a href="<?=Url::toRoute(['site/logout'])?>">Выход</a>
                    </li>

                </ul>
            </div>
    </nav>
</header>
