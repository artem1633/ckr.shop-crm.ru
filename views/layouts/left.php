<aside class="main-sidebar">

    <section class="sidebar">

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Главное меню', 'options' => ['class' => 'header']],
                    ['label' => 'Календарь', 'icon' => 'calendar', 'url' => ['/events']],
                    ['label' => 'Задачи', 'icon' => 'list-ul', 'url' => ['/tasks']],
                    ['label' => 'Клиенты', 'icon' => 'users', 'url' => ['/client']],
                    ['label' => 'Блокнот', 'icon' => 'pencil', 'url' => ['/crm']],
                    ['label' => 'Касса', 'icon' => 'money', 'url' => ['/report-money-operations/index'],],
                    ['label' => 'Справочники', 'url' => '#', 'icon' => 'book', 'items' => [
                        ['label' => 'Типы платежей', 'icon' => 'pencil', 'url' => ['/payment-type'],],
                        ['label' => 'Офисы', 'icon' => 'pencil', 'url' => ['/office'],],
                    ],],
                    ['label' => 'Отчеты', 'url' => '#', 'icon' => 'file-text', 'items' => [
                        // ['label' => 'Книга доходов и расходов', 'icon' => 'book', 'url' => ['/report-money-operations/index'],],
                        ['label' => 'Отчет по доходам', 'icon' => 'file', 'url' => ['/report-money-operations/debits'],],
                        ['label' => 'Отчет по расходам', 'icon' => 'file', 'url' => ['/report-money-operations/credits'],],
                    ], 'visible' => Yii::$app->user->identity->category_id === 1],
                    ['label' => 'Настройки', 'url' => '#', 'icon' => 'cog', 'items' => [
                        ['label' => 'Пользователи', 'icon' => 'user', 'url' => ['/user'],],
                        ['label' => 'Документы', 'icon' => 'file-text-o', 'url' => ['/html-documents'],],
                    ], 'visible' => Yii::$app->user->identity->category_id === 1],
                    // [
                    //     'label' => 'Same tools',
                    //     'icon' => 'fa fa-share',
                    //     'url' => '#',
                    //     'items' => [
                    //         ['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii'],],
                    //         ['label' => 'Debug', 'icon' => 'fa fa-dashboard', 'url' => ['/debug'],],
                    //         [
                    //             'label' => 'Level One',
                    //             'icon' => 'fa fa-circle-o',
                    //             'url' => '#',
                    //             'items' => [
                    //                 ['label' => 'Level Two', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                    //                 [
                    //                     'label' => 'Level Two',
                    //                     'icon' => 'fa fa-circle-o',
                    //                     'url' => '#',
                    //                     'items' => [
                    //                         ['label' => 'Level Three', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                    //                         ['label' => 'Level Three', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                    //                     ],
                    //                 ],
                    //             ],
                    //         ],
                    //     ],
                    // ],
                ],
            ]
        ) ?>

    </section>

</aside>
