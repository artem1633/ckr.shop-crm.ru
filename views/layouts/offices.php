<?php
/**
 * Created by PhpStorm.
 * User: Ilusha
 * Date: 12.04.2017
 * Time: 19:17
 */



/** @var \app\models\Office $offices */

$globalBalance = 0;

?>

<li class="dropdown notifications-menu">
    <a href="#" title="Остатки по офисам" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
        <i class="fa fa-building"></i>
<!--        <span class="label label-warning">10</span>-->
    </a>
    <ul class="dropdown-menu">
        <li class="header">Остатки по офисам</li>
        <li>
            <!-- inner menu: contains the actual data -->
        <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 163px;"><ul class="menu" style="overflow: hidden; width: 100%; height: 200px;">
                    <?php foreach($offices as $office): ?>
                        <li>
                            <a href="#">
                                <i class="fa fa-building-o text-aqua"></i>
                                <?= $office->name?>
                                <?php
                                    $balance = $office->getBalance();
                                    $globalBalance = $globalBalance + $balance;
                                    if($balance > 0)
                                    {
                                        echo "<span class=\"text-success\" style=\"font-size: 14px\">+$balance</span>";
                                    } elseif($balance < 0)
                                    {
                                        echo "<span class=\"text-danger\" style=\"font-size: 14px\">$balance</span>";
                                    } else {
                                        echo "0";
                                    }
                                ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul><div class="slimScrollBar" style="background: rgb(0, 0, 0) none repeat scroll 0% 0%; width: 3px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 195.122px;"></div><div class="slimScrollRail" style="width: 3px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;"></div></div>
        </li>

    </ul>
</li>


<li class="dropdown notifications-menu">
    <a href="#" title="Прибль" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
        <i class="fa fa-rub"></i>
        <?php if($globalBalance >= 0) { ?>
            <span class="label label-success" style="font-size: 14px">+<?=$globalBalance?></span>
        <?php } else { ?>
            <span class="label label-danger" style="font-size: 14px"><?=$globalBalance?></span>
        <?php } ?>
    </a>
</li>