<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PaymentType */

$this->title = 'Добавить новый тип платежа';
$this->params['breadcrumbs'][] = ['label' => 'Типы платежей', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-type-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
