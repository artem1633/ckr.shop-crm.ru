<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PaymentType */

$this->title = 'Изменить: "' . $model->name.'"';
$this->params['breadcrumbs'][] = ['label' => 'Типы платежей', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="payment-type-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
