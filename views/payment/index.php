<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PaymentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Платежи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-index">
	
	<div class="box box-default">	
		<div class="box-body" style="overflow-x: auto;">    
	<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
			[
			'attribute'=>'id',
			'content'=>function ($data){                 
				return '<span class="label label-warning" style="font-size:13px;">'.Html::a($data->id.'   ', ['update','id' =>$data->id], ['style'=>'color:#ffffff;']).'</span>';
						 
			 },
			],

            // 'id',
            'payment_type_id',
            'date',
            'price',
            // 'receipt_number',
            // 'receipt:ntext',
			['class' => 'yii\grid\ActionColumn',
			'template' => '{delete} '],
           // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
	</div>
</div>
</div>
