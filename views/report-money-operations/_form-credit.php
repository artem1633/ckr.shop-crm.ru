<?php
/**
 * Created by PhpStorm.
 * User: Ilusha
 * Date: 12.04.2017
 * Time: 12:33
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ReportMoneyOperations */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="report-money-operations-form">
    <div class="box box-default">
        <div class="box-body">
            <?php $form = ActiveForm::begin(); ?>

            <div class="row">
                <div class="col-md-4 vcenter">
                    <?= $form->field($model, 'date') ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4 vcenter">
                    <?= $form->field($model, 'event')->dropDownList($model->creditArticles)->label('Статья расхода') ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 vcenter">
                    <?= $form->field($model, 'summ')->textInput() ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 vcenter">
                    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 vcenter">
                    <?= $form->field($model, 'office_id')->dropDownList($model->officeList) ?>
                </div>
            </div>

            <div style="display:none">
            </div>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
