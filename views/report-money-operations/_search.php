<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ReportMoneyOperationsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="report-money-operations-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'date') ?>

    <?= $form->field($model, 'event') ?>

    <?= $form->field($model, 'summ') ?>

    <?= $form->field($model, 'person_id') ?>

    <?php // echo $form->field($model, 'person_snp') ?>

    <?php // echo $form->field($model, 'comment') ?>

    <?php // echo $form->field($model, 'office_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
