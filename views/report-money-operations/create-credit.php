<?php
/**
 * Created by PhpStorm.
 * User: Ilusha
 * Date: 12.04.2017
 * Time: 12:30
 */

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ReportMoneyOperations */

$this->title = 'Добавить расход';
$this->params['breadcrumbs'][] = ['label' => 'Отчеты', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Расходы', 'url' => ['credits']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="report-money-operations-create">


    <?= $this->render('_form-credit', [
        'model' => $model,
    ]) ?>

</div>
