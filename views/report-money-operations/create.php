<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ReportMoneyOperations */

$this->title = 'Добавить ';
$this->params['breadcrumbs'][] = ['label' => 'Report Money Operations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="report-money-operations-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
