<?php
/**
 * Created by PhpStorm.
 * User: Ilusha
 * Date: 12.04.2017
 * Time: 12:26
 */

use yii\helpers\Html;
use yii\grid\GridView;
use dosamigos\datepicker\DateRangePicker;
use app\models\Office;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ReportMoneyOperationsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Расходы';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="report-money-operations-index">
    <div class="box box-default">
        <div class="box-body">
            <!-- <img src="http://cbscao.ru/sites/default/files/pictures/13249/develop900.jpg" alt="Наш логотип">-->
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <p>
                <?= Html::a('Добавить', ['createcredit'], ['class' => 'btn btn-success']) ?>
            </p>

        </div>
    </div>
</div>

<div class="box box-default">
    <div class="box-body" style="overflow-x: auto;">

        <div class="box-body" style="overflow-x: auto;">
            <a class="btn btn-info" href="/report-money-operations/credits">Очистить фильтры</a>
        <div id="w0" class="grid-view">

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                //['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute'=>'id',
                    'content'=>function ($data){
                        return '<span class="label label-warning" style="font-size:13px;">'.Html::a($data->id.'   ', ['updatecredit','id' =>$data->id], ['style'=>'color:#ffffff;']).'</span>';

                    },
                    'contentOptions' => ['style' => 'width: 100px'],
                ],

                // 'id',
                [
                    'attribute' => 'date',
                    'format' => 'date',
//                    'filter' => DatePicker::widget([
//                        'model' => $searchModel,
//                        'attribute' => 'date',
//                        'template' => '{addon}{input}',
//                        'clientOptions' => [
//                            'autoclose' => true,
//                            'format' => 'dd-mm-yyyy'
//                        ]
//                    ])

                    'filter' => DateRangePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'date',
//                    'attributeFrom' => 'date_from',
                        'attributeTo' => 'date_to',
                        'language' => 'ru',
//                    'options' => ['placeholder' => 'Start date'],
                        'labelTo' => 'до',
                        'clientOptions' => [
                            'autoclose' => true,
                            'format' => 'dd-mm-yyyy'
                        ]
                    ])
                ],
                [
                    'attribute' => 'event',
                    'label' => 'Статья расхода',
                ],
                'summ',
                'comment',
                [
                    'attribute' => 'office_id',
                    'value' => 'office.name',
//                    'header' => 'Офис',
                    'label' => 'Офис',
                    'filter' => ArrayHelper::map(Office::find()->asArray()->all(), 'id', 'name'),
//                    'filter'=>ArrayHelper::map(Office::find()->asArray()->all(), 'ID', 'Name'),
//                    'filter' => Html::activeDropDownList(new Office(), 'name', ArrayHelper::map(Office::find()->asArray()->all(), 'id', 'name'),['class'=>'form-control','prompt' => 'Все']),
                ],
                // ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
</div>
</div>