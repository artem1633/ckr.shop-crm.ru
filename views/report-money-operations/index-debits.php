<?php
/**
 * Created by PhpStorm.
 * User: Qist
 * Date: 12.04.2017
 * Time: 12:10
 */

use yii\helpers\Html;
use yii\grid\GridView;
use dosamigos\datepicker\DatePicker;
use dosamigos\datepicker\DateRangePicker;
use yii\bootstrap\Dropdown;
use app\models\Office;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ReportMoneyOperationsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Доходы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-default">

    <div class="box-body" style="overflow-x: auto;">

        <div class="box-body" style="overflow-x: auto;">
            <a class="btn btn-info" href="/report-money-operations/debits">Очистить фильтры</a>
        <div id="w0" class="grid-view">

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                //['class' => 'yii\grid\SerialColumn'],

                // 'id',

                [
                    'attribute' => 'date',
                    'format' => 'date',
//                    'filter' => DatePicker::widget([
//                        'model' => $searchModel,
//                        'attribute' => 'date',
//                        'template' => '{addon}{input}',
//                        'clientOptions' => [
//                            'autoclose' => true,
//                            'format' => 'dd-mm-yyyy'
//                        ]
//                    ])

                'filter' => DateRangePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'date',
//                    'attributeFrom' => 'date_from',
                    'attributeTo' => 'date_to',
                    'language' => 'ru',
//                    'options' => ['placeholder' => 'Start date'],
                    'labelTo' => 'до',
                    'clientOptions' => [
                         'autoclose' => true,
                         'format' => 'dd-mm-yyyy'
                    ]
                ])
                ],

                [
                    'attribute' => 'event',
                    'label' => 'Вид дохода',
                ],
                'summ',
                // 'person_id',
                'person_snp',
//                'office.name',

                [
                    'attribute' => 'office_id',
                    'value' => 'office.name',
//                    'header' => 'Офис',
                    'label' => 'Офис',
                    'filter' => ArrayHelper::map(Office::find()->asArray()->all(), 'id', 'name'),
//                    'filter'=>ArrayHelper::map(Office::find()->asArray()->all(), 'ID', 'Name'),
//                    'filter' => Html::activeDropDownList(new Office(), 'name', ArrayHelper::map(Office::find()->asArray()->all(), 'id', 'name'),['class'=>'form-control','prompt' => 'Все']),
                ],
                // ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
</div>
</div>
