<?php

use yii\helpers\Html;
use yii\grid\GridView;
use dosamigos\datepicker\DateRangePicker;
use yii\helpers\ArrayHelper;
use app\models\Office;
use kartik\export\ExportMenu;

//use kartik\select2\Select2;
//use yii\helpers\Url;
//use yii\helpers\Html;
//use yii\bootstrap\Modal;
//use kartik\grid\GridView;
//use johnitvn\ajaxcrud\CrudAsset;
//use johnitvn\ajaxcrud\BulkButtonWidget;
//use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ReportMoneyOperationsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Касса';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="report-money-operations-index">
    <div class="box box-default">
        <div class="box-body">
            <!-- <img src="http://cbscao.ru/sites/default/files/pictures/13249/develop900.jpg" alt="Наш логотип">-->
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <p>
                <?= Html::a('Добавить расход', ['createcredit'], ['class' => 'btn btn-success']) ?>
            </p>

        </div>
    </div>
</div>


<?php

$columnsOnly = [
    'date',
    'event',
    'summ',
    'person_snp',
    'comment:ntext',
    'type',
    'office.name',
];

$columns = [
    [
        'attribute' => 'date',
        'format' => 'date',

        'filter' => DateRangePicker::widget([
            'model' => $searchModel,
            'attribute' => 'date',
            'attributeTo' => 'date_to',
            'language' => 'ru',
            'labelTo' => 'до',
            'clientOptions' => [
                'autoclose' => true,
                'format' => 'dd-mm-yyyy'
            ]
        ])
    ],

    'event',
    'summ',
    'person_snp',
    'comment:ntext',

    [
        'attribute' => 'type',
        'content' => function($data) {
            if ($data->type == 1) {
                return "<span class='text-success' title='Доход'><i class='fa fa-arrow-left'></i></span>";
            } else {
                return "<span class='text-danger' title='Расход'><i class='fa fa-arrow-right'></i></span>";
            }
        },
        'contentOptions' => ['style' => 'width: 50px'],
    ],

    [
        'attribute' => 'office_id',
        'value' => 'office.name',
        'label' => 'Офис',
        'filter' => ArrayHelper::map(Office::find()->asArray()->all(), 'id', 'name'),
//                    'filter'=>ArrayHelper::map(Office::find()->asArray()->all(), 'ID', 'Name'),
//                    'filter' => Html::activeDropDownList(new Office(), 'name', ArrayHelper::map(Office::find()->asArray()->all(), 'id', 'name'),['class'=>'form-control','prompt' => 'Все']),
    ],
];
?>
<?php
echo ExportMenu::widget([
    'dataProvider' => $dataProvider,
    'columns' => $columnsOnly,
]);
?>
	<div class="box box-default">	
		<div class="box-body" style="overflow-x: auto;">

            <div class="box-body" style="overflow-x: auto;">
                <a class="btn btn-info" href="/report-money-operations/index">Очистить фильтры</a>
            <div id="w0" class="grid-view">



	<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,

        'columns' => $columns,

//        'toolbar'=> [
//            ['content'=>
//                Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],
//                    ['role'=>'modal-remote','title'=> 'Добавить исполнителя','class'=>'btn btn-default']).
//                Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
//                    ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Обновить']).
//                '{toggleData}'.
//                '{export}'
//            ],
//        ],
    ]); ?>
	</div>
</div>
</div>
