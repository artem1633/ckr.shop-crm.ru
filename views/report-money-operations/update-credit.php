<?php
/**
 * Created by PhpStorm.
 * User: Ilusha
 * Date: 12.04.2017
 * Time: 18:34
 */

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ReportMoneyOperations */

$this->title = 'Изменить запись в отчете';
$this->params['breadcrumbs'][] = ['label' => 'Отчеты', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Расходы', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Изменить запись в отчете';
?>
<div class="report-money-operations-update">


    <?= $this->render('_form-credit', [
        'model' => $model,
    ]) ?>

</div>
