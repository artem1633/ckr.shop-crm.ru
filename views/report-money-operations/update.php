<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ReportMoneyOperations */

$this->title = 'Изменить ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Report Money Operations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="report-money-operations-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
