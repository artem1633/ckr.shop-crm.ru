<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ReportMoneyOperations */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Report Money Operations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="report-money-operations-view">


    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены что хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'date',
            'event',
            'summ',
            'person_id',
            'person_snp',
            'comment:ntext',
            'office_id',
        ],
    ]) ?>

</div>
