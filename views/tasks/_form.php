<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Tasks */
/* @var $form yii\widgets\ActiveForm */
?>

			<?php $form = ActiveForm::begin(); ?>

        <?php if($model->isNewRecord): ?>
            <div class="row">
                <div class="col-md-12 vcenter">
                        <?= $form->field($model, 'name')->textInput() ?>
                </div>
            </div>
        <?php endif; ?>
   		<div class="row">
			<div class="col-md-12 vcenter">
				    <?= $form->field($model, 'performer_id')->dropDownList($model->getPerformersList(), ['prompt' => 'Выберите исполнителя']) ?>
			</div>
		</div>
   		<div class="row">
			<div class="col-md-12 vcenter">
				    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>				
			</div>
		</div>
   		<div class="row">
			<div class="col-md-12 vcenter">
				    <?= $form->field($model, 'exec_time')->textInput(['type' => 'number']) ?>
			</div>
		</div>
   	
<div style="display:none">
</div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?php if($model->isNewRecord == false && $model->date_close == null): ?>
            <a href="<?=Url::toRoute(['tasks/close', 'id' => $model->id])?>" class="btn btn-danger">Закрыть</a>
        <?php endif; ?>
    </div>

    <?php ActiveForm::end(); ?>

