<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;

use Carbon\Carbon;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TasksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Задачи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tasks-index">
	<div class="box box-default">	
		<div class="box-body">
		<!-- <img src="http://cbscao.ru/sites/default/files/pictures/13249/develop900.jpg" alt="Наш логотип">-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::button('Добавить', ['value' => Url::toRoute(['tasks/create']), 'class' => 'btn btn-success', 'id' => 'button-modal_create', 'onClick' => "
           $('#modal-create')
            .find('#modal-create_content')
            .load($(this).attr('value'));
            $('#modal-create').modal('show');
        "]) ?>
   </p>




            <?php
            Modal::begin([
                'header' => '<h4>Добавить/изменить задачу</h4>',
                'id' => 'modal-create',
                'size' => 'modal-lg'
            ]);

            echo "<div id=\"modal-create_content\"></div>";

            Modal::end();
            ?>

		</div>
	</div>
	</div>
	
	<div class="box box-default">
		<div class="box-body" style="overflow-x: auto;">
	<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => function($model){
	        if($model->date_close != null)
	            return ['style' => 'background: #e4e4e4'];

            $now = Carbon::now();
            $date_created = Carbon::createFromFormat('Y-m-d', $model->date_created);
            $plan_close_date = $date_created->addDays($model->exec_time);
//            Carbon::createFromFormat('d-m-Y', $data->date_created)
            $diff = $now->diffInDays($plan_close_date, false);

            if($diff < 0)
                return ['class' => 'danger'];
//				return['style' => 'background: deeppink; color: white'];

	        return ['class' => 'default'];
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			[
			'attribute'=>'date_created',

			'content'=>function ($data){
                return Html::button(date('d.m.Y', strtotime($data->date_created)).'   ', ['value' => Url::toRoute(['tasks/update', 'id' => $data->id]), 'class' => 'button-modal_update btn btn-warning btn-xs', 'style'=>'color:#ffffff; font-size: 13px', 'onClick' => "
				   $('#modal-create').modal('show')
                        .find('#modal-create_content')
                        .load($(this).attr('value'));
				"]);

			 },
                'contentOptions' => ['style' => 'width: 10px'],
			],

            // 'id',
            'name:ntext',
            [
                'attribute' => 'performer.name',
                'filter' => $searchModel->getPerformersList(),
                'header' => 'Исполнитель',
                'content' => function($data){
	                return $data->performer->last_name.' '.$data->performer->first_name;
                },
            ],
            'comment:ntext',
             'exec_time',
             'date_close:date',
            // 'owner_id',
			['class' => 'yii\grid\ActionColumn',
			'template' => '{delete} '],
           // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
	</div>
</div>
</div>
