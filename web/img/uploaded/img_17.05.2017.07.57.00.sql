-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Май 01 2017 г., 18:18
-- Версия сервера: 5.5.52-38.3
-- Версия PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `kursovay_list`
--

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'hair', NULL, NULL),
(2, 'nails', NULL, NULL),
(3, 'brows', NULL, NULL),
(4, 'eyelashes', NULL, NULL),
(5, 'makeup', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `masters`
--

CREATE TABLE `masters` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo_path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `masters`
--

INSERT INTO `masters` (`id`, `name`, `photo_path`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 'Алексей Евдокимов', '/images/masters/Alex.jpg', 1, NULL, NULL),
(2, 'Светлана Подольская', '/images/masters/Svetlana.jpg', 1, NULL, NULL),
(3, 'Анна Семенова', '/images/masters/anna.jpg\r\n', 1, NULL, NULL),
(4, 'Эдуард Богданов', '/images/masters/eduard.jpg', 5, NULL, NULL),
(5, 'Марина Сидорова', '/images/masters/marina.jpg', 1, NULL, NULL),
(6, 'Елизавета Василевская', '/images/masters/elizabeth.jpg', 2, NULL, NULL),
(7, 'Мария Камалова', '/images/masters/maria.jpg', 3, NULL, NULL),
(8, 'Алена Хмельницкая', '/images/masters/Alena.jpg', 4, NULL, NULL),
(9, 'Валерия Петрова', '/images/masters/Valeriya.jpg', 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_01_22_154621_create_list_table', 2),
(4, '2017_01_22_170947_create_tags_table', 3),
(5, '2017_01_23_091554_add_columnisadmin_to_list', 4),
(6, '2017_01_23_093240_add_columnisadmin_to_users', 4),
(7, '2017_04_03_085922_create_table_masters', 5),
(8, '2017_04_03_090054_create_table_timeline', 5),
(9, '2017_04_03_090204_create_table_orders', 5),
(10, '2017_04_09_111253_create_table_categories', 6);

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `day` date NOT NULL,
  `timeline_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `master_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`id`, `day`, `timeline_id`, `user_id`, `master_id`, `created_at`, `updated_at`) VALUES
(1, '2017-04-04', 1, 18, 1, NULL, NULL),
(2, '2017-04-04', 1, 7, 1, NULL, NULL),
(3, '2017-04-04', 3, 7, 1, NULL, NULL),
(4, '2017-04-04', 5, 7, 1, NULL, NULL),
(5, '2017-04-05', 1, 8, 1, NULL, NULL),
(6, '2017-04-05', 8, 8, 1, NULL, NULL),
(7, '2017-04-04', 1, 7, 1, NULL, NULL),
(8, '2017-04-04', 3, 7, 1, NULL, NULL),
(9, '2017-04-04', 2, 7, 1, NULL, NULL),
(10, '2017-04-06', 8, 7, 1, NULL, NULL),
(11, '2017-04-07', 1, 10, 1, NULL, NULL),
(12, '2017-04-08', 1, 10, 1, NULL, NULL),
(13, '2017-04-09', 1, 10, 1, NULL, NULL),
(14, '2017-04-10', 1, 10, 1, NULL, NULL),
(15, '2017-04-07', 2, 11, 1, NULL, NULL),
(16, '2017-04-07', 3, 11, 1, NULL, NULL),
(17, '2017-04-07', 8, 11, 1, NULL, NULL),
(18, '2017-04-08', 8, 11, 1, NULL, NULL),
(19, '2017-04-07', 7, 12, 1, NULL, NULL),
(20, '2017-04-08', 2, 11, 1, NULL, NULL),
(21, '2017-04-07', 4, 11, 1, NULL, NULL),
(22, '2017-04-07', 5, 11, 1, NULL, NULL),
(23, '2017-04-07', 6, 11, 1, NULL, NULL),
(24, '2017-04-10', 8, 12, 1, NULL, NULL),
(25, '2017-04-08', 3, 11, 1, NULL, NULL),
(26, '2017-04-13', 2, 1, 1, NULL, NULL),
(27, '2017-04-14', 44, 25, 6, NULL, NULL),
(29, '2017-04-13', 1, 11, 1, NULL, NULL),
(32, '2017-04-19', 2, 4, 1, NULL, NULL),
(33, '2017-04-18', 6, 4, 1, NULL, NULL),
(34, '2017-04-18', 5, 4, 1, NULL, NULL),
(36, '2017-04-18', 2, 11, 1, NULL, NULL),
(37, '2017-04-19', 1, 11, 1, NULL, NULL),
(38, '2017-04-20', 1, 21, 1, NULL, NULL),
(39, '2017-04-26', 1, 11, 1, NULL, NULL),
(40, '2017-04-26', 2, 11, 1, NULL, NULL),
(41, '2017-04-26', 3, 11, 1, NULL, NULL),
(42, '2017-04-29', 56, 11, 8, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `timeline`
--

CREATE TABLE `timeline` (
  `id` int(10) UNSIGNED NOT NULL,
  `time` time NOT NULL,
  `price` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `master_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `timeline`
--

INSERT INTO `timeline` (`id`, `time`, `price`, `master_id`, `created_at`, `updated_at`) VALUES
(1, '10:00:00', '250000', 1, NULL, NULL),
(2, '12:00:00', '250000', 1, NULL, NULL),
(3, '13:00:00', '250000', 1, NULL, NULL),
(4, '14:00:00', '250000', 1, NULL, NULL),
(5, '16:00:00', '250000', 1, NULL, NULL),
(6, '17:00:00', '250000', 1, NULL, NULL),
(7, '19:00:00', '250000', 1, NULL, NULL),
(8, '20:30:00', '250000', 1, NULL, NULL),
(9, '10:00:00', '300000', 2, NULL, NULL),
(10, '12:00:00', '350000', 2, NULL, NULL),
(11, '13:00:00', '150000', 2, NULL, NULL),
(12, '14:00:00', '200000', 2, NULL, NULL),
(13, '16:00:00', '350000', 2, NULL, NULL),
(14, '17:00:00', '350000', 2, NULL, NULL),
(15, '19:00:00', '350000', 2, NULL, NULL),
(16, '20:30:00', '350000', 2, NULL, NULL),
(17, '11:30:00', '400000', 3, NULL, NULL),
(18, '13:00:00', '550000', 3, NULL, NULL),
(19, '15:00:00', '450000', 3, NULL, NULL),
(20, '17:30:00', '500000', 3, NULL, NULL),
(21, '18:00:00', '500000', 3, NULL, NULL),
(22, '20:30:00', '650000', 3, NULL, NULL),
(23, '10:30:00', '400000', 4, NULL, NULL),
(24, '12:00:00', '550000', 4, NULL, NULL),
(25, '14:00:00', '450000', 4, NULL, NULL),
(26, '15:30:00', '500000', 4, NULL, NULL),
(27, '17:00:00', '500000', 4, NULL, NULL),
(28, '19:30:00', '650000', 4, NULL, NULL),
(29, '10:30:00', '400000', 5, NULL, NULL),
(30, '12:00:00', '550000', 5, NULL, NULL),
(31, '14:00:00', '450000', 5, NULL, NULL),
(32, '15:30:00', '500000', 5, NULL, NULL),
(33, '17:00:00', '500000', 5, NULL, NULL),
(34, '19:30:00', '650000', 5, NULL, NULL),
(35, '10:30:00', '400000', 5, NULL, NULL),
(36, '12:00:00', '550000', 5, NULL, NULL),
(37, '14:00:00', '450000', 5, NULL, NULL),
(38, '15:30:00', '500000', 5, NULL, NULL),
(39, '17:00:00', '500000', 5, NULL, NULL),
(40, '19:30:00', '650000', 5, NULL, NULL),
(41, '20:30:00', '650000', 5, NULL, NULL),
(42, '10:30:00', '400000', 6, NULL, NULL),
(43, '12:00:00', '550000', 6, NULL, NULL),
(44, '14:00:00', '450000', 6, NULL, NULL),
(45, '15:30:00', '500000', 6, NULL, NULL),
(46, '17:00:00', '500000', 6, NULL, NULL),
(47, '19:30:00', '650000', 6, NULL, NULL),
(48, '20:30:00', '650000', 6, NULL, NULL),
(49, '10:30:00', '400000', 7, NULL, NULL),
(50, '12:00:00', '550000', 7, NULL, NULL),
(51, '14:00:00', '450000', 7, NULL, NULL),
(52, '15:30:00', '500000', 7, NULL, NULL),
(53, '17:00:00', '500000', 7, NULL, NULL),
(54, '19:30:00', '650000', 7, NULL, NULL),
(55, '20:30:00', '650000', 7, NULL, NULL),
(56, '10:30:00', '400000', 8, NULL, NULL),
(57, '12:00:00', '550000', 8, NULL, NULL),
(58, '14:00:00', '450000', 8, NULL, NULL),
(59, '15:30:00', '500000', 8, NULL, NULL),
(60, '17:00:00', '500000', 8, NULL, NULL),
(61, '19:30:00', '650000', 8, NULL, NULL),
(62, '20:30:00', '650000', 8, NULL, NULL),
(63, '10:30:00', '400000', 9, NULL, NULL),
(64, '12:00:00', '550000', 9, NULL, NULL),
(65, '14:00:00', '450000', 9, NULL, NULL),
(66, '15:30:00', '500000', 9, NULL, NULL),
(67, '17:00:00', '500000', 9, NULL, NULL),
(68, '19:30:00', '650000', 9, NULL, NULL),
(69, '20:30:00', '650000', 9, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_image` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_admin` int(11) DEFAULT NULL,
  `master_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `phone`, `email`, `user_image`, `password`, `remember_token`, `created_at`, `updated_at`, `is_admin`, `master_id`) VALUES
(1, 'nameuser', '55555', 'user@test.test', 'uploads/user_images/58ef35919ccd9.jpg', '$2y$10$BLZ2I9JVt.708gSm.1eS6OKwPrRMjcor/5lJVruJBpG0xgNoE6Xeq', 'OTjvkiF7MzWhqdKkBZYYoWClJXdFdZtGO5r121Dop5czebSYLjKQYPE2Rnsg', '2017-01-21 12:52:50', '2017-04-13 03:23:54', 0, NULL),
(2, 'admin', NULL, 'admin@test.test', NULL, '$2y$10$DWEwr/3z2ukcaHRLbOh59e/BWKVDmtUZohDWaduFiTzFZZgrnnrh6', 'CBT66NJg1MrZ9Ed081ecxIFEc7sTcUn2Xthutp0O9yanug7pol53SSkREr8V', '2017-01-21 13:38:16', '2017-01-23 11:08:47', 1, NULL),
(3, 'user2', NULL, 'user2@test.test', NULL, '$2y$10$LTotUIcqz7VoBFd/KvXpkOg4k4z.q5X0pfqDs0XaDkTU8pOynYG0W', 'igAm9CFPyOoaDyKz7CQOw1WToBcoCrxZLrJl7oZC5gNBWKhB8om5tmV6ZIbw', '2017-01-22 01:31:11', '2017-01-22 01:31:13', 0, NULL),
(4, 'user', NULL, 'user7@test.test', NULL, '$2y$10$GOgr7YoDkhgYgVB6M9.jbeODFVMRVZg1mQFMEp5iUhKbVanoIYSKy', 'hUXj2K97qyDpCgecQ56vamiorOkc5I3mfol0ff10eA409fqrrQzOgRrnu2JI', '2017-01-23 11:04:55', '2017-01-23 11:05:37', 0, NULL),
(5, 'name', NULL, 'asdf@fdf.df', NULL, '$2y$10$jQpvq34mXCdOg4w6wBf3aeg19crco3rDmadtXoc6QS/g3TRFGWerm', 'MM3PlUKsUhw0DpGL7iGKKFbU3JAw2p69fgYQgbjCD3LuKNYw3tzWkyEr9n04', '2017-04-03 00:28:55', '2017-04-03 00:32:54', NULL, NULL),
(6, 'name', NULL, 'adsf@ma.a', NULL, '$2y$10$l1tfoYKiHgFHhzyd/JwoMu5lYpMqKgvCLr8qOBc9OkBcNMoUkIs3q', '2iQA0auITFqQdhagVgrjaO3YuosQusg9ZCpuH95gj4oHaRP7maiwOtA1sKsc', '2017-04-03 00:31:03', '2017-04-03 00:32:37', NULL, NULL),
(7, 'new_name', NULL, NULL, NULL, '$2y$10$Tz586fio6g94mU4ADI1uhOstdR7lfdOEHWEgOE4Wf2tbnV3qMui5K', 'N1WKusyMUo24kY09nxLNFbfbuGPF613DXnkHbpAJAusCA9EjjsufKM78CgNJ', '2017-04-03 00:33:41', '2017-04-03 00:34:15', NULL, NULL),
(8, 'new_new_name', NULL, NULL, NULL, '$2y$10$Aid8YmxsTl1.dhkyHV/Pp.YxjTIeN8qi7Sk4olfpoPE9DUWdyY/la', 'UqtdkYfDYdvPkmmREn2ZSVKsHyrKpyxQKK9s06VxU4l0RYERmFNewpNIPm3k', '2017-04-03 00:33:56', '2017-04-03 00:33:58', NULL, NULL),
(9, 'new-test', NULL, NULL, NULL, '$2y$10$zpCn8XOreLH9S/XH7is6HuuJxHxAoSay.xR/8by4bV0wla5hoG17W', 'SnKjRwsUWhZldIfYeThwkzIEWsWXqQouIcJ719KXhOzVLuHhpVvGiwRxlESk', '2017-04-03 20:45:35', '2017-04-03 20:45:35', NULL, NULL),
(10, 'admin_', NULL, NULL, NULL, '$2y$10$y42tVPdnUvhXyEBQqX73vuIhp6uyBvbzk9KXquHivh3ngRmoUwUdC', 'nZXCQs8JZBZDR9IHmKBD4j8nZ6CyaR1jZHRJSczMuGGHNaTjs6vdTPOEvXho', '2017-04-04 10:13:10', '2017-04-04 10:13:10', NULL, NULL),
(11, 'aaangi.mun94@gmail.com', '+998977031635', NULL, 'uploads/user_images/58f50bf919aef.jpg', '$2y$10$MYkr6aSzBPorQUisuqRXzeK5mVHcKa6R4imO2uaxfgitokGbhgjF2', 'BTxAY1EqmAGSnUlHwyZEqqG27A73p7h6hD5C4MonFyaW0JwqyDutSItdP1Qi', '2017-04-07 12:36:00', '2017-04-17 13:39:53', NULL, NULL),
(12, '1', NULL, NULL, NULL, '$2y$10$ODxATWILaTZqnd3zbhBNE.1RQZ74PbCqKY7znlmeWTj.vmpCT5pvy', NULL, '2017-04-07 12:43:23', '2017-04-07 12:43:23', NULL, NULL),
(15, 'upload_image', NULL, NULL, NULL, '$2y$10$7g7gjxRjqXlx.pxYEIb.tudVPGHEpw6uh2VBf33ByV6BDQUo33h.6', 'ZUOOVRsfulXZhFi3rGxZlhmAdh50SDpdDRVyXQ6ocksTETx0BzbjFjypAUZa', '2017-04-12 21:48:42', '2017-04-12 21:48:42', NULL, NULL),
(16, 'upload_image2', NULL, NULL, 'uploads/user_images/58ef1c9b5a5bb.jpg', '$2y$10$fCiBCGtDLjzQk47Qy8QRnOC5RGtsjtsNS7bP2alc7m69ePCSUDMHe', '7UHgmZh0GsuU83t8Gw6dVKxAuRhtv2kCGKXS76M6dJWPnOb3FeTpbf3IZkiD', '2017-04-12 21:53:17', '2017-04-12 21:53:17', NULL, NULL),
(17, 'upload_image3', NULL, NULL, 'uploads/user_images/58ef1c9b5a5bb.jpg', '$2y$10$VLnpIMUnc3fcuXehLOnNj.b4mD8tHsQu.jgckjaomr6nNd8/hAft2', 'eQPl9XpPG3KmavHb8E1piumaUSIKmt9OPMegDbinnqw6kZnL0JvL033uCBOI', '2017-04-12 21:55:31', '2017-04-12 21:55:31', NULL, NULL),
(18, 'user_name', '43435345', NULL, 'uploads/user_images/58ef1c9b5a5bb.jpg', '$2y$10$0a4kApzgput/6MnelZeyzOMAAET2BStJSnCiXvTn/IEbngCqHnuqa', 'W7DFhhvTufRoAVMQjfH8EiJxMRH1EXB1oqWbnetw7DFqPCc2g7daQxB5leoq', '2017-04-12 22:08:50', '2017-04-13 01:29:47', NULL, NULL),
(21, 'master_alexey', '+9999999999', NULL, 'uploads/user_images/58ef1c9b5a5bb.jpg', '$2y$10$tdl4UrzIY3CSX5DmNwvGL..YPUAGLy6BNAVd.E9FBgZ1qHaVuL33O', 'sN2wsbdIAKtVgWdx5HGNzJhepzvW8INJr1IAFt2inrooKVsT1WKQc0e5jkDE', '2017-04-13 01:37:15', '2017-04-13 01:38:22', NULL, 1),
(23, 'master_anna', '777775', NULL, 'uploads/user_images/58ef23fc30411.jpg', '$2y$10$3OFEB1lw4XqraWbDv1Xo/uFKU0rs2y7cXRfuiecUSjJLQFTPFWhHO', 'NitwHEWzWiZG4dFWMsvXLaJlw4dWifNnkAaeh5Mzf6BAPMNOEkeKhEJ12a9T', '2017-04-13 02:08:44', '2017-04-13 02:17:30', NULL, 2),
(24, 'client_anna', '+09879877897', NULL, 'uploads/user_images/58ef264ca3265.jpg', '$2y$10$M95hmlFN8lcTlhn45MBGPekTbYil/hu/ZY8ko6gISPxLlg/Fu9Upy', 'j0nFeZdcBiKkBjo6xivYSEFyTw3QfUql4AHNptDfu1RMEphWsDBUCFQrb0Rh', '2017-04-13 02:18:36', '2017-04-13 02:18:36', NULL, NULL),
(25, 'some_userrr', '9898989898988', NULL, 'uploads/user_images/58ef35bfdcc30.jpg', '$2y$10$Qo0SXCdWd3Ceae9RIqaJQOgykHPfK9tRqblZOAIB6guGwox0Vs1ES', 'FcBAjIEG0KRfjsLfbMiKq8TuzZ5BF3GVJqzNX3qZnBx6RNLZ88JalexmlB56', '2017-04-13 03:24:32', '2017-04-13 03:24:45', NULL, NULL);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `masters`
--
ALTER TABLE `masters`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Индексы таблицы `timeline`
--
ALTER TABLE `timeline`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `masters`
--
ALTER TABLE `masters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT для таблицы `timeline`
--
ALTER TABLE `timeline`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
